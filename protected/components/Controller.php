<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/base.twig';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    public $seo_title = null;

    public function setting() {
        $oSetting = (new Settings())->find();

        //$dataAdress = json_decode($oSetting->attributes['adress']);
        //$dataPhones = json_decode($oSetting->attributes['phones']);

        $oSetting->adress = json_decode($oSetting->attributes['adress']);
        $oSetting->phones = json_decode($oSetting->attributes['phones']);

        return $oSetting->attributes;
    }

    public function slugify($string, $allow_slashes = false, $allow_dots = false)
    {
        $slash = "";
        $dots = "\.";
        $reverse = "";
        if ($allow_slashes) $slash = "\/";
        if ($allow_dots) {
            $dots = "";
            $reverse = "\.";
        }

        $cyr = array(
            "Щ", "Ш", "Ч", "Ц", "Ю", "Я", "Ж", "А", "Б", "В", "Г", "Д", "Е", "Ё", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ь", "Ы", "Ъ", "Э", "Є", "Ї",
            "щ", "ш", "ч", "ц", "ю", "я", "ж", "а", "б", "в", "г", "д", "е", "ё", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ь", "ы", "ъ", "э", "є", "ї");
        $lat = array(
            "Shh", "Sh", "Ch", "C", "Ju", "Ja", "Zh", "A", "B", "V", "G", "D", "E", "Jo", "Z", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "Kh", "'", "Y", "`", "E", "Je", "Ji",
            "shh", "sh", "ch", "c", "ju", "ja", "zh", "a", "b", "v", "g", "d", "e", "jo", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "kh", "'", "y", "`", "e", "je", "ji"
        );


        $string = preg_replace("/[_\s" . $dots . ",?!\[\](){}]+/", "_", $string);
        $string = preg_replace("/-{2,}/", "--", $string);
        $string = preg_replace("/_-+_/", "--", $string);
        $string = preg_replace("/[_\-]+$/", "", $string);

        if (function_exists('mb_strtolower')) {
            $string = mb_strtolower($string);
        } else {
            $string = strtolower($string);
        }

        $string = preg_replace("/(ь|ъ)/", "", $string);
        $string = str_replace($cyr, $lat, $string);
        $string = preg_replace("/[^" . $slash . $reverse . "0-9a-z_\-]+/", "", $string);

        return $string;
    }

    public function format_price($value)
    {
        if ($value > 0) {
            $value = number_format($value, 2, ',', ' ');
            $value = str_replace(',00', '', $value);
        }
        return $value;
    }
    public static function applyDiscount() {
        if(Yii::app()->shoppingCart->isEmpty()) return 1;
        $session = new CHttpSession;
        $session->open();
        if($session['promo'] != null) {
            $discount = (new Discount);
            $discount->value = $session['promo'];
            $discount->type = $session['promoType'];
            $discount->apply();
        }
    }
}