<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 29.09.2018
 * Time: 15:59
 */

class CDEKController extends Controller
{
    public function actionIndex() {
        $CDEK = new ISDEKservice();
        header('Access-Control-Allow-Origin: *');
        error_reporting(0);
        ISDEKservice::setTarifPriority(
            array(233, 137, 139, 16, 18, 11, 1, 3, 61, 60, 59, 58, 57, 83),
            array(234, 136, 138, 15, 17, 62, 63, 5, 10, 12)
        );
        $action = $_REQUEST['isdek_action'];
        if (method_exists('ISDEKservice', $action)) {
            $CDEK::$action($_REQUEST);
        }
        $this->render('index');
    }
}
