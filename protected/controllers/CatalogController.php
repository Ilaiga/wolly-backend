<?php
/**
 * Created by PhpStorm.
 * User: lev2k17
 * Date: 24.07.2018
 * Time: 17:07
 */

class CatalogController extends Controller
{
    public function actionIndex() {
        $oPage = (new Pages())->findByAttributes(['url' => 'catalog']);
        if(!empty($oPage->seo_description))
            Yii::app()->clientScript->registerMetaTag($oPage->seo_description, 'description');
        if(!empty($oPage->seo_title))
            $this->seo_title = $oPage->seo_title;

        $product = new Products();
        $this->render('index', [
            'products' => $product->getGroupProducts(),
        ]);
    }
    public function actionProduct($product) {
        $oProduct = $temp = (new Products())->findByAttributes(['title' => $product]);

        if($oProduct == null) {
            $oProduct = $temp = (new Products())->findByAttributes(['url' => $product]);
        }


        $this->layout = 'main_product_detail';
        $this->seo_title = $oProduct->seo_title;

        if(!empty($oProduct->seo_description))
            Yii::app()->clientScript->registerMetaTag($oProduct->seo_description, 'description');
        if(!empty($oProduct->seo_title))
            $this->seo_title = $oProduct->seo_title;

        $productsCount = [];
        $productQuanty = [];
        if(!empty($oProduct->productsParams)) {
            foreach($oProduct->productsParams as $key => $param) {
                $temp->param_id = $param['id'];
                $i = Yii::app()->shoppingCart->itemAt($temp->getId());
                $productsCount[] = empty($i) ? 0 : $i;
            }
        }
        if(!empty($oProduct->productsParams)) {
            foreach($oProduct->productsParams as $key => $item) {
                $productQuanty[$key] = !$productsCount[$key] ? 0 : (int)$productsCount[$key]->getQuantity();
            }
        }
        if(empty($productQuanty)) {
            $productPosition = Yii::app()->shoppingCart->itemAt($temp->getId());
            $productQuanty[] = !$productPosition ? 0 : $productPosition->getQuantity();
        }

        $this->render('product/index', [
            'product' => $oProduct,
            'form' => new FormOrder(),
            'productQuanty' => $productQuanty
        ]);
    }
}