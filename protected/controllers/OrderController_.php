<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 06.08.2018
 * Time: 16:12
 */
class OrderController extends Controller
{
    public function actionIndex() {
        $product_order = new ProductOrder();
        $this->applyDiscount();

        if (Yii::app()->request->isPostRequest) {

            $data = Yii::app()->request->getPost('ProductOrder');
            if($data['delivery'] == 1) $data['adress'] = 'Самовывоз';
            $data['order_file'] = '-';

            $product_order->setAttributes($data);
            if($product_order->validate()) {
                $positions = Yii::app()->shoppingCart->getPositions();

                $product_data = array();
                foreach ($positions as $position) {
                    //$product = $position->getPropertyProduct();
                    //$del = (!empty($product->param_id) ? '_'.$product->param_id : '');
                    //$product_data[$product->id.$del][] = $product->title;
                    //$product_data[$product->id.$del][] = $position->getQuantity();
                    //$product_data[$product->id.$del][] = $position->getSumPrice();
                    //$product_data[$product->id.$del][] = !empty($product->param_name) ? $product->param_name : ('-');
                }
                $session['promo'] = 0;
                $session['promoType'] = 0;


                if($fileName = Files::upload('uploads/shop/file', 'File'))
                    $product_order->order_file = $fileName;

                if(Yii::app()->shoppingCart->getCost() < 5000)
                    $product_order->totalcost += 300;

                $product_order->totalcost += Yii::app()->shoppingCart->getCost();
                $product_order->product_data = json_encode($product_data);
                $product_order->order_type = $data['order_type'];

                //----------
                $cdek['Город'] = Yii::app()->request->cookies['CDEK_city'];
                $cdek['Адрес'] = Yii::app()->request->cookies['CDEK_address'];
                $product_order->order_cdek = json_encode($cdek);
                //----------
                $list = null;
                $product_order->save(false);
                Yii::app()->shoppingCart->clear();
                if(isset($_COOKIE['CDEK_city'])) {
                    $adress = $_COOKIE['CDEK_city'].'('.$_COOKIE['CDEK_address'].')';
                }
                else {
                    $adress = $data['adress'];
                }
                // Work !!!
                $email = Yii::app()->email;
                $email->to = [
                    'duknuken666@gmail.com',
                    $data['email']
                ];
                $email->subject = 'Ваш заказ №'.$product_order->getPrimaryKey().' успешно оформлен. Спасибо за заказ!';
                $template = 'Ваш заказ №'.$product_order->getPrimaryKey().' успешно оформлен. Спасибо за заказ!<br/><br/>
                Имя: '.$data['fio'].'<br/>
                Номер заказа: '.$product_order->getPrimaryKey().'<br/>
                Электронная почта: '.$data['email'].'<br/>
                Телефон: '.$data['phone'].'<br/>
                Адрес доставки: '. $adress .'<br/>
                Позиции заказа:<br/>
                <ul>
                '.$list.'</ul>
                Способ оплаты: '.$product_order->getTypeOrder().'
                ';
                $email->message = $template;
                $email->send();

                unset(Yii::app()->request->cookies['CDEK_city']);
                unset(Yii::app()->request->cookies['CDEK_address']);



                return $this->render('application.views.site.page.thanks', array(
                    'orderData' => $product_order,
                    'orderType' => $product_order->getTypeOrder(),
                ));
            }
            else {
                print_r($product_order->getErrors());
            }
        }
        $oPage = (new Pages())->findByAttributes(['url' => 'order']);
        if(!empty($oPage->seo_description))
            Yii::app()->clientScript->registerMetaTag($oPage->seo_description, 'description');
        if(!empty($oPage->seo_title))
            $this->seo_title = $oPage->seo_title;
        $this->render('index', [
            'form' => $product_order,
            'promoCode' => !empty($session['promo']) ? $session['promo'] : null,
            'promoType' => !empty($session['promoType']) ? $session['promoType'] : null,
            'positions' => Yii::app()->shoppingCart->getPositions()
        ]);
    }
}