<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 12.10.2018
 * Time: 13:16
 */

class OrderController extends Controller
{
    public function actionIndex() {
        $oOrder = (new ProductOrder());

        $this->applyDiscount();

        //SEO TITLE AND DESCR
        $oPage = (new Pages())->findByAttributes(['url' => 'order']);
        if(!empty($oPage->seo_description))
            Yii::app()->clientScript->registerMetaTag($oPage->seo_description, 'description');
        if(!empty($oPage->seo_title))
            $this->seo_title = $oPage->seo_title;

        $this->render('index', [
            'form' => $oOrder,
            'positions' => Yii::app()->shoppingCart->getPositions(),
        ]);
    }



    public function actionNew() {
        $oOrder = (new ProductOrder());
        $list = null;

        $session = new CHttpSession;
        $session->open();

        $this->applyDiscount();

        if(Yii::app()->request->isPostRequest) {
            $data = Yii::app()->request->getPost('ProductOrder');


            if(!empty($_POST['adressTwo']))
            {
                $data['adress'] = $_POST['adressTwo'];
                $data['comment'] = $_POST['commentTwo'];
            }
            if((isset($data['delivery']) && $data['delivery'] == 2) || (isset($data['deliveryRegions']) && $data['deliveryRegions'] == 1)) {
                if(isset($_COOKIE['CDEK_city'])) {
                    $data['adress'] = $_COOKIE['CDEK_city'] . '(' . $_COOKIE['CDEK_address'] . ')';
                }
                else $data['adress'] = 'Самовывоз';
            }


            //echo '<pre>'.print_r($data, true).'</pre>';
            //die();
            
            if(Yii::app()->shoppingCart->getCost() < 5000)
                $oOrder->totalcost += 300;

            $oOrder->totalcost += Yii::app()->shoppingCart->getCost();
            $oOrder->product_data = json_encode($this->getOrderProducts());

            $cdek['Город'] = Yii::app()->request->cookies['CDEK_city'];
            $cdek['Адрес'] = Yii::app()->request->cookies['CDEK_address'];
            $oOrder->order_cdek = json_encode($cdek);

            //PROMOCODE CLEAR

            $session['promo'] = 0;
            $session['promoType'] = 0;
            unset($session['promo']);
            unset($session['promoType']);


            //LIST FOR MAIL
            foreach($this->getOrderProducts() as $orderProductKey => $orderProduct) {
                $list .= "<li>{$orderProduct[2]} {$orderProduct[0]} шт.</li>";
            }


            $oOrder->setAttributes($data);
            if($oOrder->validate()) {

                //echo '<pre>'.print_r($data, true).'</pre>';
                //die();

                //echo 'Успешно';
                //die();

                if($fileName = Files::upload('uploads/shop/file/', 'File'))
                    $oOrder->order_file = $fileName;
                
                $oOrder->save(false);



                Yii::app()->shoppingCart->clear();
                $this->SendMail($oOrder, $data, $oOrder->getTypeOrder(), $list);

				$this->pageTitle = 'Заказ успешно оформлен';
				$this->render('thanks', [
					'order' => $oOrder->getPrimaryKey(),
				]);
            }
            else {
                $this->render('index', [
                    'form' => $oOrder,
                    'positions' => Yii::app()->shoppingCart->getPositions(),
                ]);
            }
        }
    }
    protected static function getOrderProducts() {
        $result = [];
        foreach(Yii::app()->shoppingCart->getPositions() as $position) {
            $result[$position->getId()][] = $position->getQuantity();
            $result[$position->getId()][] = $position->getSumPrice();
            $result[$position->getId()][] = $position->title . ($position->undertitle ? ' ('.$position->undertitle.') ' : '');
            $result[$position->getId()][] = isset($position->param_name) ? $position->param_name : '';
        }
        return empty($result) ? false : $result;
    }
    private function SendMail($key, $data, $type, $list) {
        //$product_order->getTypeOrder()
        $oSetting = (new Settings())->find();
        $email = Yii::app()->email;
        $email->to = [
            $oSetting->sendMail,
            $data['email']
        ];
        $email->subject = 'Заказ №'.$key->getPrimaryKey().' оформлен.';
        $template = 'Ваш заказ №'.$key->getPrimaryKey().' успешно оформлен. Спасибо за заказ!<br/><br/>
                Имя: '.$data['fio'].'<br/>
                Номер заказа: '.$key->getPrimaryKey().'<br/>
                Электронная почта: '.$data['email'].'<br/>
                Телефон: '.$data['phone'].'<br/>
                Адрес доставки: '. $data['adress'] .'<br/>
                Позиции заказа:<br/>
                <ul>
                '.$list.'</ul>
                Способ оплаты: '.$type.'
                ';
        if(!empty($data['comment']))
            $template.='<br/><br/>Комментарий: </br>'.$data['comment'];

        $email->message = $template;

        $email->send();
    }
}