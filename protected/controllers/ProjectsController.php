<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 12.08.2018
 * Time: 14:57
 */

class ProjectsController extends Controller
{
    public function actionIndex($page = null) {
        $project = new Projects();
        $criteria = new CDbCriteria();

        $criteria->condition = 'active = :active';
        $criteria->params = ['active' => 1];
        $criteria->order = 'sort DESC';

        $count = $project->count($criteria);

        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $pagination = new Pagination($count, $pages->pageSize);
        //'projectImgs',
        $news = $project->with([
            'projectImgs' => [
                'joinType'=>'INNER JOIN',
                'order' => 'image_sort DESC'
            ],
        ])->findAll($criteria);


        $oPage = (new Pages())->findByAttributes(['url' => 'projects']);
        if(!empty($oPage->seo_description))
            Yii::app()->clientScript->registerMetaTag($oPage->seo_description, 'description');
        if(!empty($oPage->seo_title))
            $this->seo_title = $oPage->seo_title;

        $this->render('index', array(
            'news' => $news,
            'pagination' => $pagination
        ));
    }
    public function actionProject($project) {
        $oProject = (new Projects())->findByAttributes(['title' => $project]);
        if($oProject == null)
            $oProject = (new Projects())->findByAttributes(['url' => $project]);


        if(!empty($oProject->seo_description))
            Yii::app()->clientScript->registerMetaTag($oProject->seo_description, 'description');
        if(!empty($oProject->seo_title))
            $this->seo_title = $oProject->seo_title;

        $this->render($oProject->type == 1 ? 'news/index' : 'project/index', [
            $oProject->type == 1 ? 'news' : 'project' => $oProject
        ]);
    }
    public function actionNews($news_id) {
        $project = (new Projects())->findByPk($news_id);

        /*if(!empty($project->seo_description))
            Yii::app()->clientScript->registerMetaTag($project->seo_description, 'description');
        if(!empty($project->seo_title))
            $this->seo_title = $project->seo_title;

        $this->render('news/index', [
            'news' => $project
        ]);*/
    }

}