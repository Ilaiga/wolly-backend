<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 28.08.2018
 * Time: 8:44
 */

class PagesController extends Controller
{
    public function actionIndex($index) {
        if($index == null) exit('param is null');
        $page = (new Pages())->findByAttributes(['url' => $index]);
        if(!$page) exit('Pages not found');
        $this->seo_title = $page->seo_title;
        $this->pageTitle = $page->title;
        if(!empty($page->seo_description))
            Yii::app()->clientScript->registerMetaTag($page->seo_description, 'description');
        $this->render('index', [
            'page' => $page
        ]);
    }
}