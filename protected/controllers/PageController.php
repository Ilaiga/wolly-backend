<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 08.09.2018
 * Time: 14:30
 */

class PageController extends Controller
{
    public function actionIndex($index) {
        if($index) {
            $page = new Pages();
            $this->render('index', [
                'page' => $page->findByAttributes(['url' => $index]),
                'pages' => $page->findAll(),
            ]);
        }
        else {
            throw new CHttpException(404, 'Страница не была найдена');
        }
    }
}