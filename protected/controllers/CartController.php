<?php
/**
 * Created by PhpStorm.
 * User: lev2k17
 * Date: 31.07.2018
 * Time: 12:05
 */

require getcwd().'/protected/vendor/html2pdf/autoload.php';
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

class CartController extends Controller
{

    public $message = 'Спасибо! Заявка принята. Менеджер свяжется с Вами в ближайшее время.';
    public function actionIndex()
    {
        $session = new CHttpSession();
        $session->open();

        if(Yii::app()->request->isAjaxRequest) {
            $result = [];
            $oPromo = (new Promo())->findByAttributes(['title' => Yii::app()->request->getPost('promocode')],
                'status=:status',
                [':status' => 1]
            );
            if($oPromo == null) {
                $result['status'] = 'error';
                self::RemoveSession();
            }
            else {
                if(Yii::app()->shoppingCart->getCost() < $oPromo->cartSum && $oPromo->cartSum != 0) {
                    $result['status'] = 'error';
                    self::RemoveSession();
                }
                else {
                    $result['status'] = 'ok';
                    $session['promo'] = $oPromo->value;
                    $session['promoType'] = $oPromo->valueType;
                    $session['promoCode'] = $oPromo->title;

                    $session['sumOld'] = Yii::app()->shoppingCart->getCost();
                    $this->applyDiscount();
                }
            }
			$result['sumOld'] = $session['sumOld'];
			$result['sum'] = Yii::app()->shoppingCart->getCost();
            echo json_encode($result);
            return true;
        }

        $oPage = (new Pages())->findByAttributes(['url' => 'cart']);
        if(!empty($oPage->seo_description))
            Yii::app()->clientScript->registerMetaTag($oPage->seo_description, 'description');
        if(!empty($oPage->seo_title))
            $this->seo_title = $oPage->seo_title;

        $this->applyDiscount();

        return $this->render('index', [
            'positions' => Yii::app()->shoppingCart->getPositions(),
            'promoCode' => isset($session['promo']) ? $session['promoCode'] : '',
            'cartProducts' => (new CartProduct())->findAll(),
            'product' => (new Products()),
            'sum' => isset($session['promo']) ? $session['sumOld'] : false
        ]);
    }

    public function actionViewPDF() {
        $this->layout = 'empty';
        $this->render('table', [
            'positions' => Yii::app()->shoppingCart->getPositions()
        ]);
    }
    public function actionPDF() {
        $html2pdf = new HTML2PDF('L','A4','en', true, 'UTF-8', 0);
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->WriteHTML($this->renderPartial('table', [
            'positions' => Yii::app()->shoppingCart->getPositions()
        ], true));
        $html2pdf->Output('WollyCart.pdf');
    }
    public function actionAddCart()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $id = Yii::app()->request->getPost('id');
            parse_str(Yii::app()->request->getPost('data'), $data);
            $oProduct = (new Products())->findByPk($id);
            if(isset($data['size'])) {
                $oParams = (new ProductsParams())->findByAttributes(['params_name' => $data['size'], 'products_id' => $id]);
                $oProduct->param_id = $oParams->id;
                $oProduct->param_name = $oParams->params_name;
            }
            Yii::app()->shoppingCart->put($oProduct, $data['count']);
            $prices = Yii::app()->shoppingCart->getCost();
            echo CHtml::encode($this->format_price($prices));
        }
    }
    public function actionAddCartSingle()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $id = Yii::app()->request->getPost('id');
            $oParams = (new ProductsParams());
            $data = json_decode(Yii::app()->request->getPost('data'));
            foreach($data as $key => $item) {
                if (empty($item->product_name)) continue;
                $oProduct = (new Products())->findByPk($id);
                $oTempParams = $oParams->findByAttributes(['params_name' => $item->product_name, 'products_id' => $id]);
                $oProduct->param_id = $oTempParams->id;
                $oProduct->param_name = $oTempParams->params_name;
                Yii::app()->shoppingCart->update($oProduct, $item->product_count);
            }
            $prices = Yii::app()->shoppingCart->getCost();
            echo CHtml::encode($this->format_price($prices));
        }
    }

    public function actionCalc() {
        $FORM = new FormOrder();
        if (Yii::app()->request->isAjaxRequest) {
            $data = Yii::app()->request->getPost('FormOrder');



            if(empty($data['dataFormName'])) exit('empty formName');
            if(!empty($data['dataName']) && (!empty($data['dataPhone']))) {
                $FORM->convertData($data);
                $FORM->form_name = $data['dataFormName'];
                $FORM->save(false);
                echo $this->message;
                return 1;
            }
            return false;
        }
    }

    public function actionDeleteProduct()
    {
        $session = new CHttpSession();
        $session->open();

        if (Yii::app()->request->isAjaxRequest) {
            $product_id = (int)Yii::app()->request->getPost('product_id');
            $param_value = Yii::app()->request->getPost('param_name');
            $oProduct = (new Products())->findByPk($product_id);
            if($param_value) {
                $oParams = (new ProductsParams())->findByAttributes(['params_name' => $param_value, 'products_id' => $product_id]);
                $oProduct->param_id = $oParams->id;
                $oProduct->param_name = $oParams->params_name;
            }
            Yii::app()->shoppingCart->remove($oProduct->getId());

			$session['sumOld'] = Yii::app()->shoppingCart->getCost();

            $this->applyDiscount();

			header('Content-type: application/json');
			$data = [
				'sum' => isset($session['promo']) ? Yii::app()->shoppingCart->getCost() : false,
				'sumOld' => $session['sumOld']
			];
			echo CJSON::encode($data);
        } else {
            echo 'Doesnt work';
        }
    }

    public function actionEditCount()
    {
        $session = new CHttpSession();
        $session->open();

        $product_id = Yii::app()->request->getPost('id');
        $count = Yii::app()->request->getPost('count');
        $param_value = Yii::app()->request->getPost('product_param');
        if (Yii::app()->request->isAjaxRequest && !empty($product_id) && !empty($count)) {
            //--------------------------------------------------
            $product = (new Products())->findByPk($product_id);
            if($param_value) {
                $oParams = (new ProductsParams())->findByAttributes(['params_name' => $param_value, 'products_id' => $product_id]);
                $product->param_id = $oParams->id;
                $product->param_name = $oParams->params_name;
            }
            Yii::app()->shoppingCart->update($product, $count);
            //--------------------------------------------------
            $this->layout = false;
            $session['sumOld'] = Yii::app()->shoppingCart->getCost();
            $this->applyDiscount();

            header('Content-type: application/json');
            $data = [
                'sum' => isset($session['promo']) ? Yii::app()->shoppingCart->getCost() : false,
                'sumOld' => $session['sumOld'],
				'promocode' => isset($session['promo']) ? true : false,
            ];
            echo CJSON::encode($data);

        } else {
            echo 'Doesnt work';
        }
    }
    private static function RemoveSession() {
		$session = new CHttpSession();
		$session->open();

    	$session->remove('promo');
		$session->remove('promoType');
		$session->remove('promoCode');
    }
}