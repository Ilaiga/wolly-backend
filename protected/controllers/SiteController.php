<?php

class SiteController extends Controller
{
	public function actionPage($alias)
    {
        $this->render('page/' . $alias);
    }
	public function actionIndex()
	{

		$FORM = new FormOrder();
        if (Yii::app()->request->isAjaxRequest) {
            $data = Yii::app()->request->getPost('FormOrder');
            if(empty($data['dataFormName'])) exit('empty formName');
            if(!empty($data['dataName']) && (!empty($data['dataPhone']) || !empty($data['dataEmail']))) {
                $FORM->convertData($data);
                $FORM->form_name = $data['dataFormName'];
                $FORM->save();
            }
            return 1;
        }


        $oPage = (new Pages())->findByAttributes(['url' => 'main']);
        if(!empty($oPage->seo_description))
            Yii::app()->clientScript->registerMetaTag($oPage->seo_description, 'description');
        if(!empty($oPage->seo_title))
            $this->seo_title = $oPage->seo_title;

        $this->render('index', [
            'FORM' => $FORM,
        ]);
	}
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}