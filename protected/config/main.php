<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Wolly Project',
    'language' => 'en',
    // preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import' => array(
		'application.models.*',
		'application.components.*',
        'application.extensions.*',
	),
	'modules' => array(
		// uncomment the following to enable the Gii tool
        'admin' => array(
            //'class' => 'application.modules.admin.*',
            //'layout' => 'admin_main'
        ),
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'dimka123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),
    'onBeginRequest'=>function($event){
        $route=Yii::app()->getRequest()->getPathInfo();
        $module=substr($route,0,strpos($route,'/'));

        if(Yii::app()->hasModule($module))
        {
            $module=Yii::app()->getModule($module);
            if(isset($module->urlRules))
            {
                $urlManager=Yii::app()->getUrlManager();
                $urlManager->addRules($module->urlRules);
            }
        }
        return true;
    },
	// application components
	'components'=>array(
        'shoppingCart' => array(
            'class' => 'application.extensions.shoppingCart.EShoppingCart',
            'discounts' =>
                array(
                    /*array(
                        'class' => 'application.extensions.shoppingCart.discount.Discount_'
                    ),*/
                    array(
                        'class' => 'application.models.Discount'
                    )
                ),
        ),
        'email'=>array(
            'class'=>'application.extensions.email.Email',
            'delivery'=>'php', //Will use the php mailing function.
            //May also be set to 'debug' to instead dump the contents of the email into the view
        ),
        'viewRenderer' => array(
            'class' => 'application.extensions.Twig.ETwigViewRenderer',
            'fileExtension' => '.twig',
            'options' => array(
                'autoescape' => true,
                'debug' => true,
            ),
            'globals' => array(
                'html' => 'CHtml',
                'app' => Yii::app(),
                'yii' => new Yii,
            ),
            'functions' => array(
                'rot13' => 'str_rot13',
            ),
            'filters' => array(
                'jencode' => 'CJSON::encode',
                'dump' => 'var_dump'
            ),

        ),
        'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
            'urlFormat' => 'path',
            'showScriptName' => false,
            'appendParams' => false,
            'urlSuffix'=>'/',
			'rules' => require (dirname(__FILE__).'/urlrewrite.php')
		),


		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages

				array(
					'class'=>'CWebLogRoute',
				),

			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
