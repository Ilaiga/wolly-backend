<?php

// This is the database connection configuration.
return array(
	//'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	// uncomment the following lines to use a MySQL database

	'connectionString' => 'mysql:host=localhost;port=1488;dbname=wolly',
	'emulatePrepare' => false,
	'username' => 'root',
	'password' => 'root',
	'charset' => 'utf8',

);