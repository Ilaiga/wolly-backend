<?php

return array(

    '<alias:(about|contact|thanks|delivery|faq)>' => '/site/page',
    '<module:(admin)>' =>'admin',
    'admin/<_c:\w+>/<_a:\w+>/<id:\d+>' =>'admin/<_c>/<_a>/<id>',
    'admin/<_c:\w+>/<_a:\w+>'          =>'admin/<_c>/<_a>',
    'admin/<_c:\w+>'                   =>'admin/<_c>',
    '<controller:(pages)>/<index>' => '<controller>/index',
    '<controller:(catalog)>/<product>' => '<controller>/product',

    //'<controller:\w+>/<action:\w+>/<page:\d+>' => '/pages/index',
    //'<controller:(pages)>' => 'pages',

    '<controller:\w+>' => '<controller>/index',
    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
    //'<controller:(cart)>/<action:(EditCount|AddCart)>' => '<controller>/<action>',
    //'<module:(admin)>/<controller:(default)>/<action:\w+>' => '/admin/<action>',
    //'module/<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
    //'module/<m:\w+>/<c:\w+>/<a:\w+>'=>'<m>/<c>/<a>',
    //----- DETAIL -----


);