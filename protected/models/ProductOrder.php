<?php

/**
 * This is the model class for table "product_order".
 *
 * The followings are the available columns in table 'product_order':
 * @property string $id
 * @property string $product_data
 * @property string $fio
 * @property string $phone
 * @property string $email
 * @property string $adress
 * @property string $comment
 * @property string $order_cdek
 * @property int $totalcost
 * @property int $order_type
 * @property string $order_file
 */
class ProductOrder extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	public $delivery = null;
	public $deliveryRegions = null;

	public function tableName()
	{
		return 'product_order';
	}
	public function getTypeOrder() {
        $result = array(
	        '1' => 'Наличными',
            '2' => 'Оплата онлайн',
            '3' => 'Оплата по счету'
        );
        return $result[$this->order_type];
	}
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		    array('delivery, deliveryRegions', 'numerical', 'integerOnly'=>true),
			array('phone', 'length', 'max' => 45),
			array('order_type', 'length', 'max' => 3),
			array('fio, comment', 'length', 'max' => 255),
			array('email, adress, order_file', 'length', 'max' => 100),
			array('fio, phone, email, adress', 'required', 'message' => ''),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			//array('id, product_id, fio, phone, email, adress, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'fio' => 'ФИО',
			'phone' => 'Телефон',
			'email' => 'Почта',
			'adress' => 'Адрес',
			'comment' => 'Комментрий',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('product_data',$this->product_data,true);
		$criteria->compare('fio',$this->fio,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('adress',$this->adress,true);
		$criteria->compare('comment',$this->comment,true);
        $criteria->compare('totalcost',$this->totalcost,true);
        $criteria->compare('order_type',$this->order_type,true);
        $criteria->compare('order_file', $this->order_file, true);
        $criteria->compare('order_cdek', $this->order_cdek, true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
