<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 08.10.2018
 * Time: 17:09
 */
class Files
{
    private static $uploadDir = '',
        $acceptMimeTypes = array(
        //images
        'image/png',
        'image/gif',
        'image/jpeg',
        'image/pjpeg',
        'image/x-png',
        // video
        'video/3gpp',
        'video/h263',
        'video/h264',
        'video/mp4',
        'video/mp4v-es',
        'video/quicktime',
        'video/x-flv',
        'video/x-m4v',
        // audio
        'audio/3gpp',
        'audio/3gpp2',
        'audio/aac',
        'audio/mp3',
        'audio/mp4',
        'audio/mp4a-latm',
        'audio/mpeg',
        'audio/mpeg3',
        'audio/mpg',
        'audio/x-aac',
        'audio/x-mp3',
        'audio/x-mpeg',
        'audio/x-mpeg3',
        'audio/x-mpegaudio',
        'audio/x-mpg',
        // flash
        'application/x-shockwave-flash',
        // application
        'application/pdf',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/msword',
        'application/vnd.ms-excel',
        'application/zip',
        'application/x-rar-compressed',
        'text/plain',
        // xml
        'text/xml',
        // undefined
        'application/octet-stream',
    ),
        $width = 800,
        $height = 600,
        $size = 0,
        $error = ''
    ;
    public static function init()
    {
        self::$uploadDir = DOC_ROOT . 'uploads/';
    }
    public static function getError()
    {
        return self::$error;
    }

    /**
     * Загружает файл на сервер
     * @param string $field имя поля
     * @return string имя загруженного файла; false в случае сбоя
     */
    public static function upload($pathToUpload = false, $field = 'Image', $single = true, $slugify = false)
    {
        if (!isset($_FILES[$field])) {
            return false;
        }

        $files = array();
        if (is_array($_FILES[$field]["name"])) {
            foreach ($_FILES[$field] as $k => $v) {
                for ($i = 0; $i < count($v); $i++) {
                    $files[$i][$k] = $v[$i];
                }
            }
        } else {
            $files[] = $_FILES[$field];
        }

        $pathToUpload = $pathToUpload ? DOC_ROOT . $pathToUpload : self::$uploadDir;

        self::$error = '';
        $fileNameArr = array();
        for ($i = 0, $fileCount = count($files); $i < $fileCount; $i++) {
            if (!is_uploaded_file($files[$i]['tmp_name'])) {
                $error_code = (!isset($files[$i]['error'])) ? 4 : $files[$i]['error'];
                switch ($error_code) {
                    case 1: // UPLOAD_ERR_INI_SIZE
                        self::$error = 'Большой размер файла';
                        break;
                    case 2: // UPLOAD_ERR_FORM_SIZE
                        self::$error = 'Нельзя загружать большие файлы';
                        break;
                    case 3: // UPLOAD_ERR_PARTIAL
                        self::$error = 'Файл загружен не полностью';
                        break;
                    case 4: // Нет файла и ладно
                        break;
                    case 6: // UPLOAD_ERR_NO_TMP_DIR
                        self::$error = 'PHP некуда загрузить файл';
                        break;
                    case 7: // UPLOAD_ERR_CANT_WRITE
                        self::$error = 'Не получилось загрузить файл';
                        break;
                    case 8: // UPLOAD_ERR_EXTENSION
                        self::$error = 'O_o';
                        break;
                    default :
                        self::$error = 'Файл не выбран';
                        break;
                }
                continue;
            }

            if (!preg_match('/\.([^.]*?)$/i', $files[$i]['name'], $extension)) {
                self::$error = 'Файл с таким именем уже на сервере';
                continue;
            }
            $extension = $extension[1];

            if (!in_array($files[$i]['type'], self::$acceptMimeTypes)) {
                self::$error = 'Не допустимый тип файла';
                continue;
            }

            if ($slugify) {
                $fileName = substr($files[$i]['name'], 0, -strlen($extension) - 1);
                $fileName = self::_setUniqueName($pathToUpload, slugify($fileName), $extension);
            } else {
                $fileName = self::_generateUniqueName($pathToUpload, $extension);
            }

            if (!@copy($files[$i]['tmp_name'], $pathToUpload . $fileName)) {
                if (!@move_uploaded_file($files[$i]['tmp_name'], $pathToUpload . $fileName)) {
                    self::$error = 'Кривые настройки сайта';
                    continue;
                }
            }

            @chmod($pathToUpload . $fileName, 0666);
            self::setFileSize($files[$i]['size']);
            $fileNameArr[$i] = $fileName;
        }

        if ($single && isset($fileNameArr[0])) {
            return $fileNameArr[0];
        } else if (count($fileNameArr)) {
            return $fileNameArr;
        }
        return false;
    }

    private static function setFileSize($size)
    {
        self::$size = $size;
    }

    public static function getFileSize()
    {
        return self::$size;
    }

    public static function copy($origName, $copyName, $pathToUpload = false)
    {
        $pathToUpload = $pathToUpload ? DOC_ROOT . $pathToUpload : self::$uploadDir;
        if (!copy($pathToUpload . $origName, $pathToUpload . $copyName)) {
            return false;
        }
        return true;
    }

    /**
     * Генерирует случайный набор символов
     * @param int $size длина строки
     * @return string сгенерированная строка
     */
    private static function _randStr($size)
    {
        $feed = "0123456789abcdefghijklmnopqrstuvwxyz";
        $randStr = "";
        for ($i = 0; $i < $size; $i++) {
            $randStr .= substr($feed, rand(0, strlen($feed) - 1), 1);
        }
        return $randStr;
    }

    /**
     * Создает уникальное имя для файла
     * @return string имя файла
     */
    private static function _generateUniqueName($pathToUpload, $extension)
    {
        $fileName = self::_randStr(10) . '.' . $extension;

        if (file_exists($pathToUpload . $fileName)) {
            return self::_generateUniqueName($pathToUpload, $extension);
        } else {
            return $fileName;
        }
    }

    private static function _setUniqueName($pathToUpload, $fileName, $extension, $count = 0)
    {
        $checkFile = $count > 0
            ? $fileName . "-" . $count . "." . $extension
            : $fileName . "." . $extension;

        if (file_exists($pathToUpload . $checkFile)) {
            return self::_setUniqueName($pathToUpload, $fileName, $extension, ++$count);
        } else {
            return $checkFile;
        }
    }

    public static function delete($fileName, $pathToUpload = false)
    {
        $pathToUpload = $pathToUpload ? DOC_ROOT . $pathToUpload : self::$uploadDir;
        if (is_file($pathToUpload . $fileName)) {
            unlink($pathToUpload . $fileName);
            return true;
        }
        return false;
    }

    /**
     * Ресайзит картинку
     * @return boolean
     */
    public static function imageResize($fileName, $pathToUpload = false, $setWidth = 0, $setHeight = 0)
    {
        $pathToUpload = $pathToUpload ? DOC_ROOT . $pathToUpload : self::$uploadDir;
        $setWidth = $setWidth ? $setWidth : self::$width;
        $setHeight = $setHeight ? $setHeight : self::$height;

        if (!is_file($pathToUpload . $fileName)) {
            return false;
        } else if (!function_exists('getimagesize')) {
            return false;
        } else if (!function_exists('imagecreatetruecolor')) {
            return false;
        } else if (!function_exists('imagecopyresampled')) {
            return false;
        }

        $vals = @getimagesize($pathToUpload . $fileName);

        $image = array();
        $image['width'] = $vals['0'];
        $image['height'] = $vals['1'];
        $image['image_type'] = $vals['2'];

        if ($setWidth > $image['width']) {
            $setWidth = $image['width'];
        }

        if ($setHeight > $image['height']) {
            $setHeight = $image['height'];
        }

        if ($setWidth / $setHeight > $image['width'] / $image['height']) {
            $k = $setHeight / $image['height'];
        } else {
            $k = $setWidth / $image['width'];
        }
        $width = round($image['width'] * $k);
        $height = round($image['height'] * $k);

        $dst_img = imagecreatetruecolor($width, $height);

        switch ($image['image_type']) {
            case 1 :
                { // GIF
                    if (!function_exists('imagecreatefromgif')) {
                        return false;
                    } else if (!function_exists('imagegif')) {
                        return false;
                    }
                    $src_img = imagecreatefromgif($pathToUpload . $fileName);
                    $transparency = imagecolortransparent($src_img);
                    if ($transparency >= 0) {
                        $transparent_color = imagecolorsforindex($src_img, $transparency);
                        $transparency = imagecolorallocate($dst_img, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
                        imagefill($dst_img, 0, 0, $transparency);
                        imagecolortransparent($dst_img, $transparency);
                    }
                    imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $width, $height, $image['width'], $image['height']);
                    imagegif($dst_img, $pathToUpload . $fileName);
                    break;
                }
            case 2 :
                { // JPEG
                    if (!function_exists('imagecreatefromjpeg')) {
                        return false;
                    } else if (!function_exists('imagejpeg')) {
                        return false;
                    }
                    $src_img = imagecreatefromjpeg($pathToUpload . $fileName);
                    imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $width, $height, $image['width'], $image['height']);
                    imagejpeg($dst_img, $pathToUpload . $fileName, 98);
                    break;
                }
            case 3 :
                { // PNG
                    if (!function_exists('imagecreatefrompng')) {
                        return false;
                    } else if (!function_exists('imagepng')) {
                        return false;
                    }
                    $src_img = imagecreatefrompng($pathToUpload . $fileName);
                    $transparency = imagecolortransparent($src_img);
                    if ($transparency >= 0) {
                        $transparent_color = imagecolorsforindex($src_img, $transparency);
                        $transparency = imagecolorallocate($dst_img, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
                        imagefill($dst_img, 0, 0, $transparency);
                        imagecolortransparent($dst_img, $transparency);
                    } else {
                        imagealphablending($dst_img, false);
                        $color = imagecolorallocatealpha($dst_img, 0, 0, 0, 127);
                        imagefill($dst_img, 0, 0, $color);
                        imagesavealpha($dst_img, true);
                    }
                    imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $width, $height, $image['width'], $image['height']);
                    imagepng($dst_img, $pathToUpload . $fileName);
                    break;
                }
            default :
                {
                    return false;
                }
        }

        imagedestroy($dst_img);
        imagedestroy($src_img);
        return true;
    }

    private static function sanitizeCoordinates($image, $width, $height, &$coordinates)
    {
        foreach ($coordinates as $key => $val) {
            if (!in_array($key, array('lx', 'rx', 'ly', 'ry'))) {
                unset($coordinates[$key]);
            }
        }

        if (!count($coordinates)) {
            return;
        }

        if (!isset($coordinates['lx'])) {
            $coordinates['lx'] = 0;
        } else {
            $lx = (int)$coordinates['lx'];
            $coordinates['lx'] = $lx < 0
                ? 0
                : $lx > $image['width']
                    ? $image['width']
                    : $lx;
        }

        if (!isset($coordinates['rx'])) {
            $coordinates['rx'] = 0;
        } else {
            $rx = (int)$coordinates['rx'];
            $coordinates['rx'] = $rx < 0
                ? 0
                : $rx > $image['width']
                    ? $image['width']
                    : $rx;
        }

        if (!isset($coordinates['ly'])) {
            $coordinates['ly'] = 0;
        } else {
            $ly = (int)$coordinates['ly'];
            $coordinates['ly'] = $ly < 0
                ? 0
                : $ly > $image['height']
                    ? $image['height']
                    : $ly;
        }

        if (!isset($coordinates['ry'])) {
            $coordinates['ry'] = 0;
        } else {
            $ry = (int)$coordinates['ry'];
            $coordinates['ry'] = $ry < 0
                ? 0
                : $ry > $image['height']
                    ? $image['height']
                    : $ry;
        }

        $cWidth = $coordinates['rx'] - $coordinates['lx'];
        $cHeight = $coordinates['ry'] - $coordinates['ly'];

        if ($cWidth != $width || $cHeight != $height) {
            if ($cWidth / $cHeight < $width / $height) {
                $k = $cHeight / $height;
            } else {
                $k = $cWidth / $width;
            }

            $width = round($cWidth / $k);
            $height = round($cHeight / $k);

            $coordinates['rx'] += $coordinates['lx'] + $width;
            $coordinates['ry'] += $coordinates['ly'] + $height;

            if ($coordinates['rx'] > $image['width']) {
                $coordinates['lx'] -= $coordinates['rx'] - $image['width'];
                $coordinates['rx'] = $image['width'];
            }

            if ($coordinates['ry'] > $image['height']) {
                $coordinates['ly'] -= $coordinates['ry'] - $image['height'];
                $coordinates['ry'] = $image['height'];
            }
        }
    }

    public static function imageCrop($fileName, $pathToUpload = false, $setWidth = 0, $setHeight = 0, $coordinates = array())
    {
        $pathToUpload = $pathToUpload ? DOC_ROOT . $pathToUpload : self::$uploadDir;
        $setWidth = $setWidth ? $setWidth : self::$width;
        $setHeight = $setHeight ? $setHeight : self::$height;

        if (!is_file($pathToUpload . $fileName)) {
            return false;
        } else if (!function_exists('getimagesize')) {
            return false;
        } else if (!function_exists('imagecreatetruecolor')) {
            return false;
        } else if (!function_exists('imagecopyresampled')) {
            return false;
        }

        $vals = @getimagesize($pathToUpload . $fileName);

        $image = array();
        $image['width'] = $vals['0'];
        $image['height'] = $vals['1'];
        $image['image_type'] = $vals['2'];

        if ($setWidth > $image['width'] && $setHeight > $image['height']) {
            return true;
        }

        if ($setWidth > $image['width']) {
            $setWidth = $image['width'];
        }
        if ($setHeight > $image['height']) {
            $setHeight = $image['height'];
        }

        self::sanitizeCoordinates($image, $setWidth, $setHeight, $coordinates);

        if (count($coordinates)) {
            $width = $coordinates['rx'] - $coordinates['lx'];
            $height = $coordinates['ry'] - $coordinates['ly'];

            $newX = $coordinates['lx'];
            $newY = $coordinates['ly'];
        } else {
            if ($setWidth / $setHeight < $image['width'] / $image['height']) {
                $k = $setHeight / $image['height'];
            } else {
                $k = $setWidth / $image['width'];
            }

            $width = round($setWidth / $k);
            $height = round($setHeight / $k);

            $newX = round(($image['width'] - $width) / 2);
            $newY = round(($image['height'] - $height) / 2);
        }

        $dst_img = imagecreatetruecolor($setWidth, $setHeight);

        switch ($image['image_type']) {
            case 1 :
                { // GIF
                    if (!function_exists('imagecreatefromgif')) {
                        return false;
                    } else if (!function_exists('imagegif')) {
                        return false;
                    }
                    $src_img = imagecreatefromgif($pathToUpload . $fileName);
                    $transparency = imagecolortransparent($src_img);
                    if ($transparency >= 0) {
                        $transparent_color = imagecolorsforindex($src_img, $transparency);
                        $transparency = imagecolorallocate($dst_img, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
                        imagefill($dst_img, 0, 0, $transparency);
                        imagecolortransparent($dst_img, $transparency);
                    }
                    imagecopyresampled($dst_img, $src_img, 0, 0, $newX, $newY, $setWidth, $setHeight, $width, $height);
                    imagegif($dst_img, $pathToUpload . $fileName);
                    break;
                }
            case 2 :
                { // JPEG
                    if (!function_exists('imagecreatefromjpeg')) {
                        return false;
                    } else if (!function_exists('imagejpeg')) {
                        return false;
                    }
                    $src_img = imagecreatefromjpeg($pathToUpload . $fileName);
                    imagecopyresampled($dst_img, $src_img, 0, 0, $newX, $newY, $setWidth, $setHeight, $width, $height);
                    imagejpeg($dst_img, $pathToUpload . $fileName);
                    break;
                }
            case 3 :
                { // PNG
                    if (!function_exists('imagecreatefrompng')) {
                        return false;
                    } else if (!function_exists('imagepng')) {
                        return false;
                    }
                    $src_img = imagecreatefrompng($pathToUpload . $fileName);
                    $transparency = imagecolortransparent($src_img);
                    if ($transparency >= 0) {
                        $transparent_color = imagecolorsforindex($src_img, $transparency);
                        $transparency = imagecolorallocate($dst_img, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
                        imagefill($dst_img, 0, 0, $transparency);
                        imagecolortransparent($dst_img, $transparency);
                    } else {
                        imagealphablending($dst_img, false);
                        $color = imagecolorallocatealpha($dst_img, 0, 0, 0, 127);
                        imagefill($dst_img, 0, 0, $color);
                        imagesavealpha($dst_img, true);
                    }
                    imagecopyresampled($dst_img, $src_img, 0, 0, $newX, $newY, $setWidth, $setHeight, $width, $height);
                    imagepng($dst_img, $pathToUpload . $fileName);
                    break;
                }
            default :
                {
                    return false;
                }
        }

        imagedestroy($dst_img);
        imagedestroy($src_img);
        return true;
    }
}
