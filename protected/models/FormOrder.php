<?php

/**
 * This is the model class for table "form_order".
 *
 * The followings are the available columns in table 'form_order':
 * @property string $id
 * @property string $form_name
 * @property string $form_data
 */
class FormOrder extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */

	public $dataName = null;
	public $dataAdress = null;
	public $dataPhone = null;
	public $dataEmail = null;
	public $dataMessage = null;
    public $dataFormName = null;
    public $dataWidth = null;
    public $dataHeight = null;


	public function tableName()
	{
		return 'form_order';
	}
	public function convertData($array = array())
    {
        if (is_array($array)) {
            return $this->form_data = json_encode($array);
        }
        return false;
    }
    public static function translateData($name) {
        $result = [
            'dataName' => 'Имя',
            'dataAdress' => 'Адрес',
            'dataPhone' => 'Телефон',
            'dataEmail' => 'Почта',
            'dataMessage' => 'Сообщение',
            'dataFormName' => 'Название формы',
            'dataWidth' => 'Ширина',
            'dataHeight' => 'Высота'
        ];
        return $result[$name];
    }
    public function getAll() {
        $forms = $this->findAll(['order' => 'id DESC']);
        $result = array();
        foreach($forms as $form) {
            $result[$form->form_name][] = $form;
        }
        return $result;
    }
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('form_name', 'length', 'max'=>100),
			array('form_data', 'safe'),
			array('dataName, dataEmail, dataPhone', 'length', 'max'=>64),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, form_name, form_data', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'form_name' => 'Form Name',
			'form_data' => 'Form Data',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('form_name',$this->form_name,true);
		$criteria->compare('form_data',$this->form_data,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    protected function afterSave()
    {
        parent::afterSave();
        $oSetting = (new Settings())->find();
        $email = Yii::app()->email;
        $email->to = [
            $oSetting->sendMail,
        ];
        $list = null;
        $array = json_decode($this->form_data);
        foreach ($array as $jsonKey => $jsonItem) {
            if ($jsonKey == 'dataFormName') continue;
            $list .= '<li>' . self::translateData($jsonKey) . ': ' . $jsonItem . '</li>';
        }
        $email->message = "Заказать звонок (Форма №{$this->id})<ul>{$list}</ul>";
        $email->subject = "Заказать звонок (Форма №{$this->id})";
        $email->send();
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FormOrder the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
