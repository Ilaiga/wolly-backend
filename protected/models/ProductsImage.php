<?php

/**
 * This is the model class for table "products_image".
 *
 * The followings are the available columns in table 'products_image':
 * @property string $id
 * @property string $products_id
 * @property string $preview
 * @property string $image_url
 * @property string $image_name
 * @property string $params_name
 *
 * The followings are the available model relations:
 * @property Products $products
 */
class ProductsImage extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */




	public function tableName()
	{
		return 'products_image';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('products_id', 'required'),
			array('products_id, preview', 'length', 'max'=>10),
			array('image_url', 'length', 'max'=>255),
			array('image_name', 'length', 'max'=>120),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, products_id, preview, image_url, image_name', 'safe', 'on'=>'search'),
		);
	}
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'products' => array(self::BELONGS_TO, 'Products', 'products_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'products_id' => 'Products',
			'preview' => 'Preview',
			'image_url' => 'Image Url',
			'image_name' => 'Image Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('products_id',$this->products_id,true);
		$criteria->compare('preview',$this->preview,true);
		$criteria->compare('image_url',$this->image_url,true);
		$criteria->compare('image_name',$this->image_name,true);
		$criteria->compare('params_name',$this->params_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductsImage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
