<?php

/**
 * This is the model class for table "products_docs".
 *
 * The followings are the available columns in table 'products_docs':
 * @property string $id
 * @property string $products_id
 * @property string $doc_url
 * @property string $doc_type
 * @property string $doc_orginalname
 *
 * The followings are the available model relations:
 * @property Products $products
 */
class ProductsDocs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products_docs';
	}


	public function getType() {
	    $result = [
	        0 => 'Сертификация',
            1 => 'Скачать инструкцию по наклейке',
            2 => 'Наши награды'
        ];
	    return $result[$this->doc_type];
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('products_id', 'required'),
			array('products_id', 'length', 'max'=>10),
			array('doc_url', 'length', 'max'=>255),
			array('doc_type', 'length', 'max'=>45),
			array('doc_orginalname', 'length', 'max'=>120),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, products_id, doc_url, doc_type, doc_orginalname', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'products' => array(self::BELONGS_TO, 'Products', 'products_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'products_id' => 'Products',
			'doc_url' => 'Doc Url',
			'doc_type' => 'Doc Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('products_id',$this->products_id,true);
		$criteria->compare('doc_url',$this->doc_url,true);
		$criteria->compare('doc_type',$this->doc_type,true);
        $criteria->compare('doc_orginalname', $this->doc_orginalname, true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductsDocs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
