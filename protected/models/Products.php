<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property string $id
 * @property integer $type
 * @property string $cost
 * @property string $title
 * @property string $description
 * @property string $label
 * @property string $undertitle
 * @property string $color_label
 * @property string $sort
 * @property string $url
 * @property integer $sale
 * @property integer $discount
 * @property integer $quantity
 * @property integer $inversion
 * @property string $seo_title
 * @property string $seo_description
 * @property integer $labels_id
 * @property string $previewText
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property ProductsDocs[] $productsDocs
 * @property ProductsForget[] $productsForgets
 * @property ProductsImage[] $productsImages
 * @property ProductsParams[] $productsParams
 */
class Products extends CActiveRecord implements IECartPosition
{
    public $param_id;
    public $param_name;

    function getId(){
        return 'Products'. $this->id . (!empty($this->param_id) ? '_' . $this->param_id : '');
    }
    function getPrice(){
        $cost = 0;
        if(!empty($this->param_id)) {
            $oParams = (new ProductsParams())->findByAttributes(['id' => $this->param_id, 'products_id' => $this->id]);
            if($oParams == null) $cost = $this->cost;
            else
                $cost = $oParams->params_discount != 0 ? $oParams->params_discount : $oParams->params_cost;
        }
        return !empty($this->param_id) ? $cost : $this->cost;
    }
    function getPropertyProduct() {
        return $this;
    }
    public function getProducts() {
        return $this->with('productsParams', 'productsImagesPreview', 'productsForgets')->sortProduct()->
            findAll(array('condition'=>"active=:active", 'params' => array(":active" => 1), 'order' => 't.sort DESC'));
    }
    public function getProductUrl() {
        return Yii::app()->createUrl('/catalog/product', [
            'product' => !empty($this->url) ? $this->url : $this->title,
        ]);
    }
    public static function getCategories() {
        return array(
            '0' => 'Пленки',
            '1' => 'Наборы',
            '2' => 'Аксессуары'
        );
    }
    public function getGroupProducts($admin = false) {
        $categories = self::getCategories();
        $products =  !$admin ? $this->getProducts() : self::model()->findAll();
        $result = array();
        foreach($products as $product) {
            $category = $categories[$product->type];
            $result[$category][] = $product;
        }
        return $result;
    }

    public function getName($Id) {
        $name = $this->findByAttributes(['id' => $Id]);
        return $name->title;
    }
    public function getForgetProduct($id) {
        return $this->with('productsParams', 'productsImagesPreview')->findByPk($id);
    }

    /*return $query = Yii::app()->db
                ->createCommand("SELECT `image_url` FROM products_image WHERE products_id = {$this->id} AND `preview` = 1 LIMIT 1")
                ->queryAll();
            */
    /*public function getProducts() {
        return Yii::app()->db
            ->createCommand("SELECT * FROM {$this->tableName()} ORDER BY `type` ASC")
            ->queryAll();
    }*/


    public function getProductParams() {
        return Yii::app()->db
            ->createCommand("SELECT * FROM products_params WHERE id = {$this->param_id} LIMIT 1")
            ->queryAll();
    }

    public function getLabels() {
        if(!$this->labels_id) return false;
        $result = Yii::app()->db->createCommand('SELECT * FROM `labels` WHERE id = '.$this->labels_id.' AND `Active` = 1 LIMIT 1')->queryRow();
        if(!$result) {
            $this->labels_id = 0;
            $this->save(false);
        }
        return $result;
    }
	/**
	 * @return string the associated database table name
	 */




	public function tableName()
	{
		return 'products';
	}
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('type, sale, quantity, discount, inversion, labels_id, active', 'numerical', 'integerOnly'=>true),
			array('cost, sort', 'length', 'max'=>10),
			array('label, undertitle, color_label', 'length', 'max'=> 45),
			array('title, url', 'length', 'max' => 100),
			array('seo_title, seo_description', 'length', 'max' => 100),
			//array('description', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, labels_id, cost, title, description, previewText, undertitle, active, inversion, seo_title, seo_description, url, sort, sale, discount, quantity', 'safe', 'on'=>'search'),
		);
	}



	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'productsDocs' => array(self::HAS_MANY, 'ProductsDocs', 'products_id'),
			'productsForgets' => array(self::HAS_MANY, 'ProductsForget', 'products_id'),
			'productsImages' => array(self::HAS_MANY, 'ProductsImage', 'products_id'),
            'productsImagesPreview' => array(self::HAS_MANY, 'ProductsImage', 'products_id', 'condition' => 'preview = 1'/*, 'order' => 't.sort DESC'*/),
			'productsParams' => array(self::HAS_MANY, 'ProductsParams', 'products_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'cost' => 'Cost',
			'title' => 'Title',
			'description' => 'Description',
			'label' => 'Label',
			'undertitle' => 'Undertitle',
			'color_label' => 'Color Label',
			'sort' => 'Sort',
            'sale' => 'Sale',
            'discount' => 'Discount',
            'quantity' => 'Quantity',
            'labels_id' => 'Labels',
            'previewText' => 'previewText',
            'active' => 'active'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('cost',$this->cost,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('undertitle',$this->undertitle,true);
		$criteria->compare('color_label',$this->color_label,true);
		$criteria->compare('sort',$this->sort,true);
        $criteria->compare('url',$this->url,true);
        $criteria->compare('sale', $this->sale, true);
        $criteria->compare('discount', $this->discount, true);
        $criteria->compare('quantity', $this->quantity, true);
        $criteria->compare('inversion', $this->inversion, true);
        $criteria->compare('seo_title', $this->seo_title, true);
        $criteria->compare('seo_description', $this->seo_description, true);
        $criteria->compare('labels_id', $this->labels_id, true);
        $criteria->compare('previewText', $this->previewText, true);
        $criteria->compare('active', $this->active, true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function scopes() {
        return array(
            'sortProduct' => array('order' => 'sort DESC'),
        );
    }
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Products the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
