<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 22.08.2018
 * Time: 16:41
 */

/*class Discount extends IEDiscount
{
    public $coupon = null;
    public function apply()
    {
        if (empty($this->coupon)) return -1;
        $promo = (new Promo())->findByAttributes(['title' => $this->coupon],
            'status=:status',
            [':status' => 1]
        );
        if($promo != null) {
            Yii::app()->shoppingCart->setDiscountPrice($promo->value);
            //Yii::app()->shoppingCart->addDiscountPrice($promo->value);
        }
        return $promo;
    }
}*/

class Discount extends IEDiscount
{
    public $value = null;
    public $type = null;
    public function apply()
    {
        $session = new CHttpSession();
        $session->open();

        if(Yii::app()->controller->getID() != 'cart' && Yii::app()->controller->getID() != 'order') return 1;

        if(!empty($session['promoCode'])) {
            $oPromo = (new Promo())->findByAttributes(['title' => $session['promoCode']]);
            if(Yii::app()->shoppingCart->getCost() < $oPromo->cartSum && $oPromo->cartSum != 0) {
                return 1;
            }
        }
        $this->type = !empty($session['promoType']) ? $session['promoType'] : $this->type;
        $this->value = !empty($session['promo']) ? $session['promo'] : $this->value;

        if ($this->type == 2)
            Yii::app()->shoppingCart->setDiscountPrice(Yii::app()->shoppingCart->getCost() / 100 * $this->value);
        else
            Yii::app()->shoppingCart->setDiscountPrice($this->value);
    }
}