<?php

/**
 * This is the model class for table "settings".
 *
 * The followings are the available columns in table 'settings':
 * @property string $id
 * @property string $adress
 * @property string $phones
 * @property string $mail
 * @property string $vk
 * @property string $inst
 * @property string $facebook
 * @property string $header_phone
 * @property string $seo_title
 * @property string $seo_description
 * @property string $sendMail
 */
class Settings extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'required'),
			array('id, vk, inst, facebook, header_phone', 'length', 'max'=>45),
			array('mail, seo_description, seo_title, sendMail', 'length', 'max'=>100),
			array('adress, phones', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, adress, phones, mail, sendMail, vk, inst, facebook, header_phone, seo_description, seo_title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'adress' => 'Adress',
			'phones' => 'Phones',
			'mail' => 'Mail',
			'vk' => 'Vk',
			'inst' => 'Inst',
			'facebook' => 'Facebook',
			'header_phone' => 'Header Phone',
            'sendMail' => 'sendMail'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('adress',$this->adress,true);
		$criteria->compare('phones',$this->phones,true);
		$criteria->compare('mail',$this->mail,true);
		$criteria->compare('vk',$this->vk,true);
		$criteria->compare('inst',$this->inst,true);
		$criteria->compare('facebook',$this->facebook,true);
		$criteria->compare('header_phone',$this->header_phone,true);
		$criteria->compare('seo_title', $this->seo_title, true);
        $criteria->compare('seo_description', $this->seo_description, true);
        $criteria->compare('sendMail', $this->sendMail, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Settings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
