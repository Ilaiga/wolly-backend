<?php

/**
 * This is the model class for table "products_params".
 *
 * The followings are the available columns in table 'products_params':
 * @property string $id
 * @property string $products_id
 * @property string $params_name
 * @property string $params_cost
 * @property integer $params_discount
 *
 * The followings are the available model relations:
 * @property Products $products
 */
class ProductsParams extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products_params';
	}



	/*public function updateParams($new_params = array()) {
	    foreach($new_params as $new_param) {
	        $this
        }
    }*/


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('products_id', 'required'),
			array('products_id, params_cost, params_discount', 'length', 'max'=>10),
			array('params_name', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, products_id, params_name, params_cost, params_discount', 'safe', 'on'=>'search'),
		);
	}
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'products' => array(self::BELONGS_TO, 'Products', 'products_id'),
		);
	}
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'products_id' => 'Products',
			'params_name' => 'Params Name',
			'params_cost' => 'Params Cost',
            'params_discount' => 'Params Discount'
		);
	}
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('products_id',$this->products_id,true);
		$criteria->compare('params_name',$this->params_name,true);
		$criteria->compare('params_cost',$this->params_cost,true);
		$criteria->compare('params_discount', $this->params_discount, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
