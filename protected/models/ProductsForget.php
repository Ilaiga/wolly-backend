<?php

/**
 * This is the model class for table "products_forget".
 *
 * The followings are the available columns in table 'products_forget':
 * @property string $id
 * @property string $products_id
 * @property string $product_id
 *
 * The followings are the available model relations:
 * @property Products $products
 */
class ProductsForget extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products_forget';
	}




	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('products_id', 'required'),
			array('products_id', 'length', 'max'=>10),
			array('product_id', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, products_id, product_id', 'safe', 'on'=>'search'),
		);
	}

    public function getProducts() {
        return $this->with('products')->findByPk($this->products_id);
    }



	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'products' => array(self::BELONGS_TO, 'Products', 'products_id'),
            //products' => array(self::HAS_ONE, 'Products', 'products_id', 'condition' => 'preview = 1'),
		    'products' => array(self::HAS_MANY, 'Products', 'products_id'),

                //'condition' => 'products_id = :product_id', array('product_id' => $this->product_id)),
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'products_id' => 'Products',
			'product_id' => 'Product',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('products_id',$this->products_id,true);
		$criteria->compare('product_id',$this->product_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductsForget the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
