<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 14.10.2018
 * Time: 11:34
 */

class PromoController extends AController
{
    public function actionIndex() {
        $oPromo = new Promo();
        if(Yii::app()->request->isPostRequest) {
            $data = Yii::app()->request->getPost('Promo');
            $data['status'] = 1;
            $oPromo->setAttributes($data);
            if($oPromo->validate()) {
                $oPromo->save();
            }
        }
        $this->render('index', [
            'promoAll' => $oPromo->findAll(),
            'promoForm' => $oPromo
        ]);
    }
    public function actionEdit($id) {
        $oPromo = (new Promo())->findByPk($id);
        if(Yii::app()->request->isPostRequest) {
            $data = Yii::app()->request->getPost('Promo');
            $oPromo->setAttributes($data);
            if($oPromo->validate()) {
                $oPromo->save(false);
            }
        }
        $this->render('edit', [
            'promo' => $oPromo
        ]);
    }
    public function actionDelete($id) {
        (new Promo())->deleteByPk($id);
    }
}