<?php

class DefaultController extends AController
{
    public $bodyClass;
    public function actionIndex()
    {
        $this->bodyClass = 'login';
        $model = new LoginForm;
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            if ($model->validate() && $model->login()) {
                return $this->render('index', []);
            }
        }
        if (!empty(Yii::app()->user->id)) {
            return $this->render('index', []);
        }
        else $this->layout = 'empty';
        return $this->render('login', [
            'form' => $model
        ]);
    }

    public function actionViewPromo($id) {
        if(Yii::app()->request->isPostRequest) {
            $data = Yii::app()->request->getPost('Promo');
            $promo = (new Promo())->findByPk($id);
            $promo->setAttributes($data);
            if($promo->validate()) {
                $promo->save(false);
            }
            else {
                print_r($promo->getErrors());
            }
        }
        $this->render('promo/view', [
            'promo' => (new Promo())->findByPk($id)
        ]);
    }
    public function actionPromo() {

    }

    public function actiondeleteProject()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $projectId = Yii::app()->request->getPost('projectId');
            if (empty($projectId)) exit('invalid projectId empty');
            $projectImg = (new ProjectImg())->deleteAllByAttributes(['project_id' => $projectId]);
            $project = (new Projects())->deleteByPk($projectId);
            return !$project ? false : true;
        }
        return 'error';
    }

    public function actionParamsDelete()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $id = Yii::app()->request->getPost('id');
            if (empty($id)) exit('empty params');
            $params = new ProductsParams();
            $params->deleteByPk($id);
        }
    }
}