<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 12.10.2018
 * Time: 16:19
 */

class PagesController extends AController
{
    public function actionIndex() {
        $oPage = new Pages();
        if (Yii::app()->request->isPostRequest) {
            $data = Yii::app()->request->getPost('Pages');
            if ($oPage->validate()) {
                $oPage->setAttributes($data);
                $oPage->content = $data['content'];
                $oPage->url = $data['url'];
                $oPage->title = $data['title'];
                $oPage->save(false);
            }
        }
        $this->render('index', [
            'pages' => $oPage->findAll(),
            'FORM' => $oPage
        ]);
    }

    public function actionSEO($page) {
        $oPage = (new Pages());
        $temp = $oPage->findByPk($page);
        $data = Yii::app()->request->getPost('Pages');
        $temp->seo_title = $data['seo_title'];
        $temp->seo_description = $data['seo_description'];
        $temp->save(false);
        $this->render('index', [
            'pages' => $oPage->findAll(),
            'FORM' => $oPage
        ]);
    }

    public function actionEdit($page) {
        $oPage = (new Pages())->findByPk($page);
        $oCart = new CartProduct();
        $content = true;
        if($oPage->url == 'cart'
            || $oPage->url == 'order'
            || $oPage->url == 'projects'
            || $oPage->url == 'catalog'
            || $oPage->url == 'main'
        ) $content = false;
        if (Yii::app()->request->isPostRequest) {
            if($oPage->url == 'cart') {
                $dataForget = Yii::app()->request->getPost('ProductForgetCart');
                $oCart->deleteAll();
                if (!empty($dataForget)) {
                    foreach ($dataForget as $forget) {
                        $oCart->id = NULL;
                        $oCart->isNewRecord = true;
                        $oCart->products_id = $forget;
                        $oCart->save();
                    }
                }
            }
            $data = Yii::app()->request->getPost('Pages');
            if ($oPage->validate()) {
                $oPage->setAttributes($data);
                if ($content) {
                    $oPage->content = $data['content'];
                    $oPage->url = $data['url'];
                    $oPage->title = $data['title'];
                }
                $oPage->save(false);
            } else {
                print_r($oPage->getErrors());
            }
        }
        return $this->render('edit', [
            'page' => $oPage,
            'content' => $content,
            'showCart' => $oPage->url == 'cart' ? true : false,
            'cartProducts' => $oCart->getIds(),
            'products' => (new Products())->getProducts()
        ]);
    }
}