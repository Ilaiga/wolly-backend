<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 12.10.2018
 * Time: 16:04
 */

class SettingController extends AController
{
    public function actionIndex() {
        $oSetting = new Settings();
        $this->render('index', [
            'settings' => $oSetting->find(),
            'products' => (new Products())->getProducts(),
        ]);
    }
    public function actionSave() {
        $oSetting = (new Settings())->findByPk(1);
        if (Yii::app()->request->isPostRequest) {

            $data = Yii::app()->request->getPost('Settings');
            //print_r($data);
            //die();
            $data['adress'] = json_encode(Yii::app()->request->getPost('adress'));
            $data['phones'] = json_encode(Yii::app()->request->getPost('phone'));
            $oSetting->setAttributes($data);
            $oSetting->save(false);
            $this->render('index', [
                'settings' => $oSetting->find(),
                'products' => (new Products())->getProducts(),
            ]);
        }
    }
}