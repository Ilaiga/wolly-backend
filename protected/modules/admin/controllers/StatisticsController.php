<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 12.10.2018
 * Time: 16:54
 */

class StatisticsController extends AController
{
    public function actionOrder() {
        $this->render('order', [
            'orders' => (new ProductOrder())->findAll(['order' => 'id DESC'])
        ]);
    }
    public function actionCallback() {
        $this->render('callback', [
            'forms' => (new FormOrder())->getAll()
        ]);
    }
    public function actionDelete($type, $StatID) {
        if(Yii::app()->request->isAjaxRequest) {
            if (empty($type) && empty($StatID))
                return false;
            switch ($type) {
                case 0:
                    {
                        $oOrder = (new ProductOrder())->deleteByPk($StatID);
                        break;
                    }
                case 1:
                    {
                        $oForm = (new FormOrder())->deleteByPk($StatID);
                        break;
                    }
            }
        }
    }
}