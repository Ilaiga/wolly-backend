<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 15.10.2018
 * Time: 12:39
 */

class LabelsController extends AController
{
    public function actionIndex() {
        $oLabels = new Labels();
        if(Yii::app()->request->isPostRequest) {
            $data = Yii::app()->request->getPost('Labels');
            $oLabels->setAttributes($data);
            $oLabels->Active = 1;
            $oLabels->LabelColor = '#'.$oLabels->LabelColor;
            if($oLabels->validate()) {
                $oLabels->save(false);
            }
            $this->redirect('/admin/labels/');
        }
        $this->render('index', ['label' => $oLabels->findAll(), 'labelForm' => $oLabels]);
    }
    public function actionEdit($id) {
        $oLabels = (new Labels())->findByPk($id);
        if(Yii::app()->request->isPostRequest) {
            $data = Yii::app()->request->getPost('Labels');
            $oLabels->setAttributes($data);
            if($oLabels->validate()) {
                $oLabels->save(false);
            }
            $this->redirect('/admin/labels/');
        }
        $this->render('edit', ['label' => $oLabels]);
    }

    public function actionDelete($id) {
        if(Yii::app()->request->isAjaxRequest) {
            (new Labels())->deleteByPk($id);
        }
    }
}