<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 11.10.2018
 * Time: 15:41
 */

class ProjectsController extends AController
{
    public function actionIndex() {
        $projects = new Projects();
        if (Yii::app()->request->isAjaxRequest) {
            $productId = Yii::app()->request->getPost('productId');
            $data = Yii::app()->request->getPost('productSort');
            if (($result = $projects->findByPk($productId))) {
                $result->sort = $data;
                $result->save();
            }
        }
        return $this->render('index', [
            'projects' => $projects->findAll(),
            'FORM' => $projects
        ]);
    }

    public function actionSEO($project) {
        $projects = new Projects();
        $project = $projects->findByPk($project);
        if (Yii::app()->request->isPostRequest) {
            $data = Yii::app()->request->getPost('Projects');
            $project->seo_title = $data['seo_title'];
            $project->seo_description = $data['seo_description'];
            $project->save(false);
        }
        return $this->render('edit', [
            'project' => $project
        ]);
    }
    public function actionProject($project) {
        $projects = new Projects();
        $project = $projects->findByPk($project);
        if (Yii::app()->request->isPostRequest) {
            $data = Yii::app()->request->getPost('Projects');
            $project->descritption = $data['descritption'];
            $project->task = $data['task'];
            $project->title = $data['title'];
            $project->content = $data['content'];
            $project->undertitle = $data['undertitle'];
            $project->save(false);
        }
        return $this->render('edit', [
            'project' => $project
        ]);
    }


    //Images functions
    public function actionSaveImageParams() {
        if(Yii::app()->request->isPostRequest) {
            $data = Yii::app()->request->getPost('image');
            $oImage = (new ProjectImg());
            $project = (new Projects())->findByPk(Yii::app()->request->getPost('ProjectID'));
            if(empty($data['imageID'])) {
                return $this->render('edit', [
                    'project' => $project
                ]);
            }
            foreach($data['imageID'] as $key => $imageID) {
                if (!$imageID) continue;
                $image = $oImage->findByPk($imageID);
                $image->image_title = $data['imageName'][$key];
                $image->image_sort = $data['imageSort'][$key];
                $image->save(false);
            }
            return $this->render('edit', [
                'project' => $project
            ]);
        }
    }


    public function actionAllSave($ProjectID) {

        if(Yii::app()->request->isPostRequest) {
            $data = Yii::app()->request->getPost('image');
            $oImage = (new ProjectImg());
            $project = (new Projects())->findByPk($ProjectID);
            if(!empty($data['imageID']))
            foreach($data['imageID'] as $key => $imageID) {
                if (!$imageID) continue;
                $image = $oImage->findByPk($imageID);
                $image->image_title = $data['imageName'][$key];
                $image->image_sort = $data['imageSort'][$key];
                $image->save(false);
            }


            $data = Yii::app()->request->getPost('Projects');
            $project->setAttributes($data);

           
            $project->descritption = $data['descritption'];

            if(isset($data['task']) && isset($data['uses'])) {
                $project->task = $data['task'];
                $project->uses = $data['uses'];
            }

            $project->url = $data['url'];
            $project->title = $data['title'];
            $project->content = $data['content'];
            $project->undertitle = $data['undertitle'];
            $project->seo_title = $data['seo_title'];

            $project->seo_description = $data['seo_description'];

            $project->save(false);


            return $this->render('edit', [
                'project' => $project
            ]);
        }
    }


    //Images functions
    public function actionDeleteImage() {
        if(Yii::app()->request->isAjaxRequest) {
            $data = Yii::app()->request->getPost('imageID');
            $oImage = (new ProjectImg())->findByPk($data);
            Files::delete('icon_' . $oImage->image_url, 'uploads/project/');
            Files::delete('main_' . $oImage->image_url, 'uploads/project/');
            Files::delete($oImage->image_url, 'uploads/project/');
            $oImage->delete();
        }
    }
    public function actionUploadImage() {
        $image = new ProjectImg();
        if (Yii::app()->request->isPostRequest) {
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                $data = json_decode(Yii::app()->request->getPost('params'));
                if ($imageName = Files::upload('uploads/project/', 'File')) {
                    //Files::imageCrop($imageName, 'uploads/project/', 1000, 1000);
                    if (Files::copy($imageName, 'icon_' . $imageName, 'uploads/project/')) {
                        Files::imageResize('icon_' . $imageName, 'uploads/project/', 100, 300);
                    }
                    if (Files::copy($imageName, 'main_' . $imageName, 'uploads/project/')) {
                        Files::imageResize('main_' . $imageName, 'uploads/project/', 200, 400);
                    }
                }
                $image->project_id = $data->project_id;
                $image->image_url = $imageName;
                $image->save();
            }
        }
    }

}