<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 11.10.2018
 * Time: 11:55
 */

class ProductsController extends AController
{
    public function actionIndex()
    {
        $product = new Products();
        if (Yii::app()->request->isAjaxRequest) {
            $productId = Yii::app()->request->getPost('productId');
            $data = Yii::app()->request->getPost('productSort');

            if (($result = $product->findByPk($productId))) {
                $result->sort = $data;
                $result->save();
            }
        }
        $this->render('index', [
            'products' => $product->getGroupProducts(true),
            'product' => $product
        ]);
    }



    //Images functions
    public function actionSaveImageParams() {
        if(Yii::app()->request->isPostRequest) {
            $data = Yii::app()->request->getPost('image');
            $oImage = (new ProductsImage());

            $product = (new Products())->findByPk(Yii::app()->request->getPost('ProductID'));
            $forgets = [];
            foreach ($product->productsForgets as $forget)
                $forgets[] = $forget->product_id;

            if(empty($data['imageID'])) {
                return $this->render('edit', [
                    'product' => $product,
                    'forget' => $forgets,
                    'products' => $product->getProducts(),
                    'labels' => (new Labels())->findAllByAttributes(['Active' => 1])
                ]);
            }
            foreach($data['imageID'] as $key => $imageID) {
                if (!$imageID) continue;
                $image = $oImage->findByPk($imageID);
                $image->params_name = $data['imageParam'][$key];
                $image->image_name = $data['imageName'][$key] ? $data['imageName'][$key] : '';
                $image->preview = 0;
                if (isset($data['imagePreview'][$key]))
                    $image->preview = 1;
                $image->save(false);
            }
            return $this->render('edit', [
                'product' => $product,
                'forget' => $forgets,
                'products' => $product->getProducts(),
                'labels' => (new Labels())->findAllByAttributes(['Active' => 1])
            ]);
        }
    }
    public function actionDeleteImage() {
        if(Yii::app()->request->isAjaxRequest) {
            $data = Yii::app()->request->getPost('imageID');
            $oImage = (new ProductsImage())->findByPk($data);
            Files::delete('icon_' . $oImage->image_url, 'uploads/shop/');
            Files::delete('main_' . $oImage->image_url, 'uploads/shop/');
            Files::delete($oImage->image_url, 'uploads/shop/');
            $oImage->delete();
        }
    }
    public function actionUploadImage() {
        $image = new ProductsImage();
        if (Yii::app()->request->isPostRequest) {
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                $data = json_decode(Yii::app()->request->getPost('params'));
                if ($imageName = Files::upload('uploads/shop/', 'File')) {
                    //Files::imageCrop($imageName, 'uploads/shop/', 1000, 1000);
                    if (Files::copy($imageName, 'icon_' . $imageName, 'uploads/shop/')) {
                        Files::imageResize('icon_' . $imageName, 'uploads/shop/', 100, 300);
                    }
                    if (Files::copy($imageName, 'main_' . $imageName, 'uploads/shop/')) {
                        Files::imageResize('main_' . $imageName, 'uploads/shop/', 200, 400);
                    }
                }
                $image->preview = 1;
                $image->products_id = $data->product_id;
                $image->image_url = $imageName;
                $image->image_name = '';
                $image->save();
            }
        }
    }

    public function actionProduct($action, $ProductID = -1)
    {

        $product = new Products();
        $paramsModel = new ProductsParams();
        $docsModel = new ProductsDocs();
        $forget_ = new ProductsForget();
        switch ($action) {

            case 'docs': {
                if (Yii::app()->request->isPostRequest) {
                    $docFiles = Yii::app()->request->getPost('productDocs');
                    if(!empty($docFiles)) {
                        $upload_dir = getcwd() . '/uploads/shop/attach/';
                        foreach ($_FILES['productDocs']['name']['File'] as $key => $error) {
                            if (empty($error)) continue;
                            //TYPE FILE
                            $filename = $_FILES['productDocs']['name']['File'][$key];
                            $type = substr(strrchr($filename, '.'), 1);
                            $tmp_name = basename($_FILES['productDocs']['tmp_name']['File'][$key]) . '.' . $type;
                            //--------
                            $result = $docsModel->findByAttributes(['doc_type' => $docFiles['Text'][$key], 'products_id' => $ProductID]);
                            if ($result != null) {
                                if (unlink($upload_dir . $result['doc_url']) != false) {
                                    $result['products_id'] = $ProductID;
                                    $result['doc_type'] = $docFiles['Text'][$key];
                                    $result['doc_url'] = $tmp_name;
                                    move_uploaded_file($_FILES['productDocs']['tmp_name']['File'][$key], $upload_dir . $tmp_name);
                                    $result->save();
                                }
                                // Нашли такой файл
                            } else {
                                $docsModel->id = NULL;
                                $docsModel->isNewRecord = true;
                                $docsModel->products_id = $ProductID;
                                $docsModel->doc_type = $docFiles['Text'][$key];
                                $docsModel->doc_url = $tmp_name;
                                $docsModel->save();
                                move_uploaded_file($_FILES['productDocs']['tmp_name']['File'][$key], $upload_dir . $tmp_name);
                                // Не нашли такой файл
                            }
                        }
                    }
                    $this->render('index', [
                        'products' => $product->getGroupProducts(true),
                        'product' => $product
                    ]);
                }
                break;
            }
            case 'add':
                {
                    if (Yii::app()->request->isPostRequest) {
                        $data = Yii::app()->request->getPost('Products');
                        if (empty($data['title'])) {
                            $product->addError('title', '[!] Заполните поле Заголовок');
                        }
                        if (empty($data['cost'])) {
                            $product->addError('cost', '[!] Заполните поле Цена');
                        }
                        if (empty($data['description'])) {
                            $product->addError('description', '[!] Заполните поле Описание');
                        }
                        if (!empty($data['title']) && !empty($data['cost']) && !empty($data['description'])) {
                            $product->type = 0;
                            $product->description = $data['description'];
                            $product->setAttributes($data);
                            $product->save(true);

                        } else {
                            return $this->render($action, [
                                'product' => $product
                            ]);
                        }
                        return $this->redirect(Yii::app()->createUrl('/admin/products/product', [
                            'action' => 'edit',
                            'ProductID' => $product->getPrimaryKey()
                        ]));
                    }
                    $this->render('index', [
                        'products' => $product->getGroupProducts(true),
                        'product' => $product
                    ]);
                    break;
                }
            case 'edit':
                {
                    $product = $product->findByPk($ProductID);
                    if (Yii::app()->request->isPostRequest) {
                        $data = Yii::app()->request->getPost('Products');
                        $dataForget = Yii::app()->request->getPost('ProductForget');
                        $forget_->deleteAllByAttributes(['products_id' => $ProductID]);
                        if (!empty($dataForget)) {
                            foreach ($dataForget as $key => $forget) {
                                $forget_->id = NULL;
                                $forget_->isNewRecord = true;
                                $forget_->product_id = $forget;
                                $forget_->products_id = $ProductID;
                                $forget_->save();
                            }
                        }
                        if ($product->validate()) {
                            $product->description = $data['description'];
                            $product->previewText = $data['previewText'];
                            $product->labels_id = $data['labels_id'];
                            $product->setAttributes($data);
                            $product->save(false);
                        }

                        //@TODO Replace This Code (Not Important)
                        $data_['params_cost'] = Yii::app()->request->getPost('params_cost');
                        $data_['params_name'] = Yii::app()->request->getPost('params');
                        $data_['params_discount'] = Yii::app()->request->getPost('params_discount');
                        $data_['params_id'] = Yii::app()->request->getPost('params_id');
                        if (!empty($data_['params_id'])) {
                            foreach ($data_['params_id'] as $data__key => $data__item) {
                                if (empty($data_['params_name'][$data__key])) continue;
                                $params_ = $paramsModel->findByPk($data__item);
                                $params_->params_name = $data_['params_name'][$data__key];
                                $params_->params_cost = empty($data_['params_cost'][$data__key]) ? 0 : $data_['params_cost'][$data__key];
                                $params_->params_discount = empty($data_['params_discount'][$data__key]) ? 0 : $data_['params_discount'][$data__key];
                                $params_->save();
                            }
                        }
                        $data_['params_cost'] = Yii::app()->request->getPost('new-params_cost');
                        $data_['params_name'] = Yii::app()->request->getPost('new-params');
                        $data_['params_discount'] = Yii::app()->request->getPost('new-params_discount');
                        if (!empty($data_['params_name'])) {
                            foreach ($data_['params_name'] as $data__key => $data__item) {
                                if (empty($data__item)) continue;
                                $paramsModel->params_name = $data__item;
                                $paramsModel->params_cost = empty($data_['params_cost'][$data__key]) ? 0 : $data_['params_cost'][$data__key];
                                $paramsModel->params_discount = empty($data_['params_discount'][$data__key]) ? 0 : $data_['params_discount'][$data__key];
                                $paramsModel->products_id = $ProductID;

                                $paramsModel->save();
                            }
                        }
                    }
                    $forgets = [];
                    foreach ($product->productsForgets as $forget)
                        $forgets[] = $forget->product_id;
                    $this->render($action, [
                        'product' => $product,
                        'forget' => $forgets,
                        'products' => $product->getProducts(),
                        'labels' => (new Labels())->findAllByAttributes(['Active' => 1])
                    ]);
                    break;
                }
            case 'remove': {
                if(Yii::app()->request->isAjaxRequest) {
                    $productId = Yii::app()->request->getPost('productId');
                    (new ProductsDocs())->deleteAllByAttributes(['products_id' => $productId]);
                    (new ProductsForget())->deleteAllByAttributes(['products_id' => $productId]);
                    (new ProductsImage())->deleteAllByAttributes(['products_id' => $productId]);
                    (new ProductsParams())->deleteAllByAttributes(['products_id' => $productId]);
                    (new CartProduct())->deleteAllByAttributes(['products_id' => $productId]);
                    (new Products())->deleteByPk($productId);
                }
                break;
            }
            case 'seo': {
                if(Yii::app()->request->isPostRequest) {
                    $product = $product->findByPk($ProductID);
                    $data = Yii::app()->request->getPost('Products');
                    $product->seo_title = $data['seo_title'];
                    $product->seo_description = $data['seo_description'];
                    $product->save(false);
                    $this->render('index', [
                        'products' => $product->getGroupProducts(true),
                        'product' => $product
                    ]);
                    break;
                }
            }
        }
    }


    public function actionSaveAll($ProductID) {
        $product = new Products();
        $paramsModel = new ProductsParams();
        $docsModel = new ProductsDocs();
        $forget_ = new ProductsForget();
        $product = $product->findByPk($ProductID);
        if (Yii::app()->request->isPostRequest) {
            $data = Yii::app()->request->getPost('Products');
            $dataForget = Yii::app()->request->getPost('ProductForget');
            $forget_->deleteAllByAttributes(['products_id' => $ProductID]);
            if (!empty($dataForget)) {
                foreach ($dataForget as $key => $forget) {
                    $forget_->id = NULL;
                    $forget_->isNewRecord = true;
                    $forget_->product_id = $forget;
                    $forget_->products_id = $ProductID;
                    $forget_->save();
                }
            }
            if ($product->validate()) {
                $product->description = $data['description'];
                $product->previewText = $data['previewText'];
                $product->seo_title = $data['seo_title'];
                $product->seo_description = $data['seo_description'];
                $product->labels_id = $data['labels_id'];
                $product->setAttributes($data);
                $product->save(false);
            }

            //@TODO Replace This Code (Not Important)
            $data_['params_cost'] = Yii::app()->request->getPost('params_cost');
            $data_['params_name'] = Yii::app()->request->getPost('params');
            $data_['params_discount'] = Yii::app()->request->getPost('params_discount');
            $data_['params_id'] = Yii::app()->request->getPost('params_id');
            if (!empty($data_['params_id'])) {
                foreach ($data_['params_id'] as $data__key => $data__item) {
                    if (empty($data_['params_name'][$data__key])) continue;
                    $params_ = $paramsModel->findByPk($data__item);
                    $params_->params_name = $data_['params_name'][$data__key];
                    $params_->params_cost = empty($data_['params_cost'][$data__key]) ? 0 : $data_['params_cost'][$data__key];
                    $params_->params_discount = empty($data_['params_discount'][$data__key]) ? 0 : $data_['params_discount'][$data__key];
                    $params_->save();
                }
            }
            $data_['params_cost'] = Yii::app()->request->getPost('new-params_cost');
            $data_['params_name'] = Yii::app()->request->getPost('new-params');
            $data_['params_discount'] = Yii::app()->request->getPost('new-params_discount');
            if (!empty($data_['params_name'])) {
                foreach ($data_['params_name'] as $data__key => $data__item) {
                    if (empty($data__item)) continue;
                    $paramsModel->isNewRecord = true;
                    $paramsModel->id = NULL;
                    $paramsModel->params_name = $data__item;
                    $paramsModel->params_cost = empty($data_['params_cost'][$data__key]) ? 0 : $data_['params_cost'][$data__key];
                    $paramsModel->params_discount = empty($data_['params_discount'][$data__key]) ? 0 : $data_['params_discount'][$data__key];
                    $paramsModel->products_id = $ProductID;
                    $paramsModel->save();
                }
            }
            $docFiles = Yii::app()->request->getPost('productDocs');
            if(!empty($docFiles)) {
                $upload_dir = getcwd() . '/uploads/shop/attach/';
                foreach ($_FILES['productDocs']['name']['File'] as $key => $error) {
                    if (empty($error)) continue;
                    //TYPE FILE
                    $filename = $_FILES['productDocs']['name']['File'][$key];
                    $type = substr(strrchr($filename, '.'), 1);
                    $tmp_name = basename($_FILES['productDocs']['tmp_name']['File'][$key]) . '.' . $type;
                    //--------
                    $result = $docsModel->findByAttributes(['doc_type' => $docFiles['Text'][$key], 'products_id' => $ProductID]);
                    if ($result != null) {
                        if (unlink($upload_dir . $result['doc_url']) != false) {
                            $result['products_id'] = $ProductID;
                            $result['doc_type'] = $docFiles['Text'][$key];
                            $result['doc_url'] = $tmp_name;
                            move_uploaded_file($_FILES['productDocs']['tmp_name']['File'][$key], $upload_dir . $tmp_name);
                            $result->save();
                        }
                        // Нашли такой файл
                    } else {
                        $docsModel->id = NULL;
                        $docsModel->isNewRecord = true;
                        $docsModel->products_id = $ProductID;
                        $docsModel->doc_type = $docFiles['Text'][$key];
                        $docsModel->doc_url = $tmp_name;
                        $docsModel->save();
                        move_uploaded_file($_FILES['productDocs']['tmp_name']['File'][$key], $upload_dir . $tmp_name);
                        // Не нашли такой файл
                    }
                }
            }
            $data = Yii::app()->request->getPost('image');
            $oImage = (new ProductsImage());
            if(!empty($data['imageID']))
            foreach($data['imageID'] as $key => $imageID) {
                if (!$imageID) continue;
                $image = $oImage->findByPk($imageID);
                $image->params_name = $data['imageParam'][$key];
                $image->image_name = $data['imageName'][$key];
                if (isset($data['imagePreview'][$key]))
                    $image->preview = 1;
                $image->save(false);
            }

            $forgets = [];
            foreach ($product->productsForgets as $forget)
                $forgets[] = $forget->product_id;
            $this->render('edit', [
                'product' => $product,
                'forget' => $forgets,
                'products' => $product->getProducts(),
                'labels' => (new Labels())->findAllByAttributes(['Active' => 1])
            ]);
        }
    }
}