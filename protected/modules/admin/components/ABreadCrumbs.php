<?php
/**
 * Created by PhpStorm.
 * User: duknuken666
 * Date: 11.10.2018
 * Time: 11:40
 */


Yii::import('zii.widgets.CBreadcrumbs');
class ABreadCrumbs extends CBreadcrumbs
{
    public $tagName = 'ul';
    public $htmlOptions = ['id' => 'breadcrumbs', 'class' => 'breadcrumb'];
    public $encodeLabel = true;
    public $homeLink = '<li><i class = "icon-home"></i><a href = "/admin/">Панель управления</a></li>';
    public $links = [];
    public $activeLinkTemplate = '<li><a href = "{url}">{label}</a></li>';
    public $inactiveLinkTemplate = '<li class = "current"><a>{label}</a></li>';
    public $separator = '';
}