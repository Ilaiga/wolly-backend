<?php
$this->pageTitle = $project->title;
$this->breadcrumbs = [
    'Проекты' => ['/admin/projects'],
    $this->pageTitle
];
?>

<? //= '<pre>' . print_r($project, true) . '</pre>'; ?>

<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4></h4>
            </div>
            <div class="widget-content">
                <?= CHtml::beginForm(Yii::app()->createUrl('/admin/projects/AllSave', ['ProjectID' => $project->id]), 'POST', []) ?>
                <?= CHtml::errorSummary($project) ?>
                <div class="tabbable box-tabs">
                    <ul class="nav nav-tabs">
                        <li><a href="#box_tab3" data-toggle="tab">SEO</a></li>
                        <li><a href="#box_tab2" data-toggle="tab">Изображения</a></li>
                        <li class="active"><a href="#box_tab1" data-toggle="tab">Описание</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="box_tab1">
                            <div class="form-horizontal row-border">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Активность:</label>
                                    <div class="col-md-10">
                                        <?= CHtml::activeCheckBox($project, 'active',
                                            array(
                                                'checked' => $project->active ? ('checked') : (''),
                                            )
                                        ) ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">URL:</label>
                                    <div class="col-md-10 input-width-xxlarge">
                                        <?= CHtml::activeTextField($project, 'url',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $project->url
                                            )
                                        ); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Тип: </label>
                                    <div class="col-md-10">
                                        <label class="radio">
                                            <?= CHtml::activeRadioButton($project, 'type',
                                                array(
                                                    'checked' => !$project->type ? ('checked') : (''),
                                                    'value' => 0,
                                                    'uncheckValue' => null
                                                )
                                            ) ?>
                                            Проект
                                        </label>
                                        <label class="radio">
                                            <?= CHtml::activeRadioButton($project, 'type',
                                                array(
                                                    'checked' => $project->type == 1 ? ('checked') : (''),
                                                    'value' => 1,
                                                    'uncheckValue' => null
                                                )
                                            ) ?>
                                            Новость
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Заголовок:</label>
                                    <div class="col-md-10 input-width-xxlarge">
                                        <?= CHtml::activeTextField($project, 'title',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $project->title
                                            )
                                        ); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Подзаголовок:</label>
                                    <div class="col-md-10 input-width-xxlarge">
                                        <?= CHtml::activeTextField($project, 'undertitle',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $project->undertitle
                                            )
                                        ); ?>
                                    </div>
                                </div>
                                <?if($project->type != 1):?>
                                <div class="panel-group" id="accordion">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                                    Настройки для проектов
                                                </a>
                                            </h3>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Задача:</label>
                                                    <div class="col-md-10">
                                                        <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                                                            'model' => $project,
                                                            'attribute' => 'task',
                                                            'language' => 'ru',
                                                        )); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Использовано:</label>
                                                    <div class="col-md-10">
                                                        <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                                                            'model' => $project,
                                                            'attribute' => 'uses',
                                                            'language' => 'ru',
                                                        )); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?endif;?>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Анонс:</label>
                                    <div class="col-md-10">
                                        <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                                            'model' => $project,
                                            'attribute' => 'descritption',
                                            'language' => 'ru',
                                        )); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Описание:</label>
                                    <div class="col-md-10">
                                        <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                                            'model' => $project,
                                            'attribute' => 'content',
                                            'language' => 'ru',
                                        )); ?>
                                    </div>
                                </div>



                            </div>
                        </div>
                        <div class="tab-pane" id="box_tab2">

                            <div class="form-horizontal row-border">

                                <div class="form-group">
                                    <div class="col-md-12" style="display: flex; flex-wrap: wrap">
                                        <? foreach ($project->projectImgs as $key => $image): ?>
                                            <div class="productPreviewImageItem">
                                                <a data-fancybox="product-<?= $key ?>"
                                                   href="/uploads/project/<?= $image['image_url'] ?>">
                                                    <img src="/uploads/project/icon_<?= $image['image_url'] ?>">
                                                </a>
                                                <input class="form-control" type="number"
                                                       name="image[imageSort][<?= $key ?>]"
                                                       placeholder="Сортировка"
                                                       value="<?= $image['image_sort'] ?>">
                                                <input type="text" name="image[imageName][<?= $key ?>]"
                                                       placeholder="Название изображения"
                                                       value="<?= $image['image_title'] ?>">

                                                <input type="hidden" name="image[imageID][<?= $key ?>]"
                                                       value="<?= $image['id'] ?>">
                                                <div class="productPreviewImage__content">
                                                    <?= CHtml::ajaxLink('Удалить',
                                                        Yii::app()->createUrl('/admin/projects/DeleteImage'),
                                                        [
                                                            'type' => 'POST',
                                                            'data' => [
                                                                'imageID' => $image['id']
                                                            ],
                                                            'success' => 'js:$(this).closest(".productPreviewImageItem").remove()'
                                                        ],
                                                        [
                                                            'class' => 'removeLink'
                                                        ]);
                                                    ?>
                                                </div>
                                            </div>
                                        <? endforeach; ?>
                                    </div>
                                    <input type="hidden" name="ProjectID" value="<?= $project->id ?>">
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8">
                                        <div id="dropzone_profile_photo"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="box_tab3">
                            <div class="form-horizontal row-border">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">(SEO) Заголовок:</label>
                                    <div class="col-md-10 input-width-xxlarge">
                                        <?= CHtml::activeTextField($project, 'seo_title',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $project->seo_title
                                            )
                                        ); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">(SEO) Описание:</label>
                                    <div class="col-md-10 input-width-xxlarge">
                                        <?= CHtml::activeTextField($project, 'seo_description',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $project->seo_description
                                            )
                                        ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-group" style="margin-top: 50px">
                    <div class="col-md-10">
                        <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                    </div>
                </div>
                <?= CHtml::endForm() ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function ($) {
        var myDropzone = new Dropzone('div#dropzone_profile_photo', {
            url: '<?=$this->createUrl('/admin/projects/UploadImage')?>',
            previewsContainer: "#dropzone_profile_photo",
            paramName: "File", // имя переменной, используемой для передачи файлов
            maxFilesize: 5, // лимит размера файла в МБ
            parallelUploads: 1, //кол-во параллельных обращений к серверу
            acceptedFiles: 'image/*',
        });
        myDropzone.on('sending', function (file, xhr, formData) {
            var data = JSON.stringify({
                project_id: <?=$project->id?>,
            });
            formData.append("params", data);
        });
        myDropzone.on("addedfile", function (file) {
            // this is my confirm function, you can insert your
            if (!confirm("Do you want to upload the file?")) {
                this.removeFile(file);
                return false;
            }
        });
    });
</script>
