<?php
$this->pageTitle = 'Настройки';
$this->breadcrumbs = [
    'Настройка'
];
?>

<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4><i class="icon-reorder"></i> Настройки сайта</h4>
                <div class="toolbar no-padding">
                    <div class="btn-group">
                        <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                    </div>
                </div>
            </div>
            <div class="widget-content">
                <?= CHtml::beginForm('/admin/setting/save/', 'POST', ['class' => 'form-horizontal row-border']); ?>
                <?= CHtml::errorSummary($settings); ?>

                <div class="form-group">
                    <label class="col-md-2 control-label">Телефон в шапке:</label>
                    <div class="col-md-10 input-width-xxlarge">
                        <?= CHtml::activeTextField($settings, 'header_phone',
                            array(
                                'class' => 'form-control',
                                'value' => $settings->header_phone,
                                'data-mask' => '+ 7 (999) 999-9999',
                                'placeholder' => '+7 (999) 999-99-99'
                            )
                        ); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Почта:</label>
                    <div class="col-md-10 input-width-xxlarge">
                        <?= CHtml::activeTextField($settings, 'mail',
                            array(
                                'class' => 'form-control',
                                'value' => $settings->mail,
                                'placeholder' => 'example@example.ex'
                            )
                        ); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Почта (уведомления):</label>
                    <div class="col-md-10 input-width-xxlarge">
                        <?= CHtml::activeTextField($settings, 'sendMail',
                            array(
                                'class' => 'form-control',
                                'value' => $settings->sendMail,
                                'placeholder' => 'example@example.ex'
                            )
                        ); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">VK (ссылка):</label>
                    <div class="col-md-10 input-width-xxlarge">
                        <?= CHtml::activeTextField($settings, 'vk',
                            array(
                                'class' => 'form-control',
                                'value' => $settings->vk,
                                'placeholder' => 'http://example.ru'
                            )
                        ); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Instagram (ссылка):</label>
                    <div class="col-md-10 input-width-xxlarge">
                        <?= CHtml::activeTextField($settings, 'inst',
                            array(
                                'class' => 'form-control',
                                'value' => $settings->inst,
                                'placeholder' => 'http://example.ru'
                            )
                        ); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Facebook (ссылка):</label>
                    <div class="col-md-10 input-width-xxlarge">
                        <?= CHtml::activeTextField($settings, 'facebook',
                            array(
                                'class' => 'form-control',
                                'value' => $settings->facebook,
                                'placeholder' => 'http://example.ru'
                            )
                        ); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Телефон:</label>
                    <div class="col-md-9 phoneItems">
                        <?php
                        $dataPhone = json_decode($settings->phones);
                        if (is_array($dataPhone) == true):
                            foreach ($dataPhone as $phones):
                                ?>
                                <input type="tel" class="form-control input-width-large" name="phone[]"
                                       value="<?= $phones ?>">
                            <? endforeach; else: ?>
                            <input type="tel" class="form-control input-width-large" name="phone[]" value=""
                                   placeholder="+7 (999) 999-99-99">
                        <? endif; ?>
                    </div>
                    <div class="col-md-1">
                        <a href="javascript:;" class="btn btn-sm ajax AddPhone"><i class="icol-add"></i></a>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Адреc:</label>
                    <div class="col-md-9 addressItems">
                        <?php
                        $dataAdress = json_decode($settings->adress);
                        if (!empty($dataAdress)):
                            foreach ($dataAdress->city as $cityKey => $city):
                                ?>
                                <input type="text" class="form-control input-width-large" name="adress[city][]"
                                       value="<?= $city ?>">
                                <input type="text" class="form-control input-width-large" name="adress[adress][]"
                                       value="<?= $dataAdress->adress[$cityKey] ?>">
                            <? endforeach; else: ?>
                            <input type="text" class="form-control input-width-large" name="adress[city][]"
                                   value="" placeholder = "Город">
                            <input type="text" class="form-control input-width-large" name="adress[adress][]"
                                   value="" placeholder="Местоположение">
                        <? endif; ?>
                    </div>
                    <div class="col-md-1">
                        <a href="javascript:;" class="btn btn-sm ajax addAdress"><i class="icol-add"></i></a>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                    </div>
                </div>
                <?= CHtml::endForm() ?>
            </div>
        </div>
    </div>
</div>



<!-- /Normal -->