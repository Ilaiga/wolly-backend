<?php
$this->pageTitle = 'Формы';
$this->breadcrumbs = [
    'Формы'
];
?>



<?foreach($forms as $form_key => $form_item): ?>
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4><i class="icon-reorder"></i> <?=strtolower($form_key)?></h4>
                <div class="toolbar no-padding">
                    <div class="btn-group">
                        <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                    </div>
                </div>
            </div>
            <div class="widget-content">
                <table class="table table-striped table-bordered table-hover table-checkable datatable">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Данные по форме</th>
                        <th><i class="icon-remove"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?foreach($form_item as $form):?>
                            <tr class="item">
                                <td><?=$form->id?></td>
                                <td>
                                    <?
                                    $array = json_decode($form->form_data);
                                    foreach($array as $jsonKey => $jsonItem):
                                        if($jsonKey == 'dataFormName') continue;?>
                                        <?=$form::translateData($jsonKey).': '.$jsonItem.'</br>'?>
                                    <?endforeach;?>
                                </td>
                                <td>
                                    <?= CHtml::ajaxLink(
                                        '<i class = "icon-remove"></i>',
                                        Yii::app()->createUrl('/admin/statistics/delete', [
                                            'type' => 1,
                                            'StatID' => $form->id
                                        ]),
                                        [
                                            'type' => 'POST',
                                            'success' => 'js:$(this).closest(".item").remove()'
                                        ]
                                    ); ?>
                                </td>
                            </tr>
                        <?endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?endforeach;?>