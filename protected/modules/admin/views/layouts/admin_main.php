<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <title>Административная панель 0.1</title>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.0/jquery.fancybox.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.4.0/jquery.fancybox.min.js"></script>
    <link rel="stylesheet" href = "/css/main.admin.css">
    <link rel="stylesheet" href = "/css/dropzone.css">
    <link rel="stylesheet" href="/css/font-awesome.css">
    <link rel="stylesheet" href="/css/custom.admin.css">

    <script type="text/javascript" src="/js/admin/main.admin.js"></script>
    <script type="text/javascript" src="/js/admin/bootstrap-colorpicker.js"></script>
    <script type="text/javascript" src="/js/admin/select2.js"></script>
    <script type="text/javascript" src="/js/admin/select2_locale_ru.js"></script>

    <script type="text/javascript" src="/js/admin/fileinput.js"></script>

    <script type="text/javascript" src="/js/admin/pickadate/picker.js"></script>
    <script type="text/javascript" src="/js/admin/pickadate/picker.date.js"></script>
    <script type="text/javascript" src="/js/admin/pickadate/picker.time.js"></script>




    <script>
        $(document).ready(function () {
            "use strict";
            App.init(); // Init layout and core plugins
            Plugins.init(); // Init all plugins
            FormComponents.init(); // Init all form-specific plugins

            $( ".datepicker" ).datepicker({
                defaultDate: +7,
                showOtherMonths:true,
                autoSize: true,
                appendText: '<span class="help-block">(yyyy-mm-dd)</span>',
                dateFormat: 'yy-mm-dd'
            });
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/3.8.4/dropzone.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.ajax.AddParams').on('click', function () {
                $('<div class="col-md-10 params-block" style = "display: flex; align-items: center">\n' +
                    '<input type = "text" class = "form-control" name = "new-params[]" value = "" placeholder="Название">\n' +
                    '<input type = "text" class = "form-control" name = "new-params_discount[]" placeholder="Скидка" value = "">\n' +
                    '<input type = "text" class = "form-control" name = "new-params_cost[]" placeholder="Стоимость" value = "">\n' +
                    'руб.\n' +
                    '</div>'
                ).insertAfter($('.params-block').last());
                return false;
            });
            $('.ajax.addDocs').on('click', function () {
                $('<div class = "input-width-xlarge" style = "display: flex; justify-content: center; flex-direction: column">\n' +
                    '<input type="text" class = "form-control"  name = "productDocs[Text][]" value = "" placeholder="Название документа">\n' +
                    '<input type="file" name = "productDocs[File][]" data-style="fileinput" style = "display: block">\n' +
                    '</div>').appendTo('.files-block');
                return false;
            });
            $('.ajax.RemoveParams').on('click', function () {
                $(this).parent('.params-block').remove();
            });
            $('.ajax.AddPhone').on('click', function () {
                $('<input type="tel" class="form-control input-width-large" name="phone[]" placeholder = "+7 (999) 999-99-99" value="">').appendTo('.phoneItems');
            });
            $('.ajax.addAdress').on('click', function () {
                $('<input type="text" class="form-control input-width-large" name="adress[city][]"\n' +
                    '                                   value="" placeholder = "Город">\n' +
                    '                            <input type="text" class="form-control input-width-large" name="adress[adress][]"\n' +
                    '                                   value="" placeholder="Местоположение">').appendTo('.addressItems');
            });
        });
    </script>
    <style>
        .phoneItems, .addressItems {
            display: flex;
            align-items: center;
            flex-wrap: wrap;
        }
        .form-control.image {
            width: 100px !important;
        }
    </style>
</head>
<body>
<header class="header navbar navbar-fixed-top" role="banner">
    <!-- Top Navigation Bar -->
    <div class="container">

        <!-- Only visible on smartphones, menu toggle -->
        <ul class="nav navbar-nav">
            <li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
        </ul>

        <!-- Logo -->
        <a class="navbar-brand" href="/admin">
            Панель управления
        </a>
        <!-- /logo -->

        <!-- Sidebar Toggler -->
        <a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Скрыть боковое меню">
            <i class="icon-reorder"></i>
        </a>
        <!-- /Sidebar Toggler -->

        <!-- Top Left Menu -->
        <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm">
            <li>
                <a href="/admin">
                    Главная
                </a>
            </li>
        </ul>
        <!-- /Top Left Menu -->

        <!-- Top Right Menu -->
        <ul class="nav navbar-nav navbar-right">

            <!-- User Login Dropdown -->
            <li class="dropdown user">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-male"></i>
                    <span class="username">Admin</span>
                    <i class="icon-caret-down small"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="login.html"><i class="icon-key"></i> Выход</a></li>
                </ul>
            </li>
            <!-- /user login dropdown -->
        </ul>
        <!-- /Top Right Menu -->
    </div>
    <!-- /top navigation bar -->
</header> <!-- /.header -->

<div id="container">
    <div id="sidebar" class="sidebar-fixed">
        <div id="sidebar-content">

            <!--=== Navigation ===-->
            <ul id="nav">
                <li>
                    <a href="/admin">
                        <i class="icon-dashboard"></i>
                        Рабочий экран
                    </a>
                </li>
                <li>
                    <a href="<?=Yii::app()->createUrl('/admin/products/') ?>">
                        <i class="icon-desktop"></i>
                        Товары
                    </a>
                </li>
                <li>
                    <a href="<?= Yii::app()->createUrl('/admin/projects/') ?>">
                        <i class="icon-desktop"></i>
                        Проекты
                    </a>
                </li>
                <li>
                    <a href="<?= Yii::app()->createUrl('/admin/pages/') ?>">
                        <i class="icon-folder-open-alt"></i>
                        Страницы
                    </a>
                </li>
                <li>
                    <a href="<?= Yii::app()->createUrl('/admin/promo/') ?>">
                        <i class="icon-folder-open-alt"></i>
                        Купоны
                    </a>
                </li>
                <li>
                    <a href="<?= Yii::app()->createUrl('/admin/labels/') ?>">
                        <i class="icon-folder-open-alt"></i>
                        Лейблы
                    </a>
                </li>
                <li>
                    <a href="<?= Yii::app()->createUrl('/admin/statistics/callback/') ?>">
                        <i class="icon-edit"></i>
                        Формы (Статистика)
                    </a>
                </li>
                <li>
                    <a href="<?= Yii::app()->createUrl('/admin/statistics/order/') ?>">
                        <i class="icon-table"></i>
                        Заказы (Статистика)
                    </a>
                </li>
                <li>
                    <a href="<?= Yii::app()->createUrl('/admin/setting/') ?>">
                        <i class="icon-table"></i>
                        Настройки сайта
                    </a>
                </li>
            </ul>

            <div class="sidebar-widget align-center">
                <div class="btn-group" data-toggle="buttons" id="theme-switcher">
                    <label class="btn active">
                        <input type="radio" name="theme-switcher" data-theme="bright"><i class="icon-sun"></i> Bright
                    </label>
                    <label class="btn">
                        <input type="radio" name="theme-switcher" data-theme="dark"><i class="icon-moon"></i> Dark
                    </label>
                </div>
            </div>

        </div>
        <div id="divider" class="resizeable"></div>
    </div>
    <!-- /Sidebar -->

    <div id="content">
        <div class="container">
            <!-- Breadcrumbs line -->
            <div class="crumbs">
                <?php if (isset($this->breadcrumbs)): ?>
                    <?=$this->widget('ABreadCrumbs', [
                            'links' => $this->breadcrumbs
                    ], true)?>
                <?php endif ?>
            </div>
            <!-- /Breadcrumbs line -->
            <div class="page-header">
                <div class="page-title">
                    <h3><?= CHtml::encode($this->pageTitle) ?></h3>
                </div>
            </div>
            <?= $content ?>
        </div> <!-- /.row -->

    </div>
</div>
</body>
</html>



