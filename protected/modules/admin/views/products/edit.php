<?php
$this->pageTitle = $product->title;
$this->breadcrumbs = [
    'Продукты' => Yii::app()->createUrl('/admin/products/'),
    $product->title . ' (' . $product->undertitle . ')'
];
?>
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4></h4>
            </div>
            <div class="widget-content">
                <?=CHtml::beginForm(Yii::app()->createUrl('/admin/products/SaveAll', ['ProductID' => $product->id]), 'POST', ['enctype' => 'multipart/form-data'])?>
                <div class="tabbable box-tabs">
                    <ul class="nav nav-tabs">
                        <li><a href="#box_tab4" data-toggle="tab">SEO</a></li>
                        <li><a href="#box_tab3" data-toggle="tab">Документы</a></li>
                        <li><a href="#box_tab2" data-toggle="tab">Изображения</a></li>
                        <li class="active"><a href="#box_tab1" data-toggle="tab">Описание</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="box_tab1">
                            <div class="form-horizontal row-border">
                                <? //= CHtml::beginForm(Yii::app()->createUrl('/admin/products/product', ['action' => 'edit', 'ProductID' => $product->id]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
                                <? //= CHtml::errorSummary($product) ?>

                                <div class="form-group">
                                    <label class="col-md-2 control-label"><b>Активность:</b> </label>
                                    <div class="col-md-10">
                                        <?= CHtml::activeCheckBox($product, 'active', [
                                            'checked' => $product->active ? ('checked') : ('')
                                        ]); ?>
                                        Да
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">URL:</label>
                                    <div class="col-md-10 input-width-xxlarge">
                                        <?= CHtml::activeTextField($product, 'url',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $product->url
                                            )
                                        ); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Сортировка (по убыванию): </label>
                                    <div class="col-md-10">
                                        <?= CHtml::activeNumberField($product, 'sort', [
                                            'value' => $product->sort,
                                            'class' => 'form-control input-width-small'
                                        ]); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">В наличии: </label>
                                    <div class="col-md-10">
                                        <?= CHtml::activeCheckBox($product, 'quantity', [
                                            'checked' => $product->quantity ? ('checked') : ('')
                                        ]); ?>
                                        Да
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Белый цвет: </label>
                                    <div class="col-md-10">
                                        <?= CHtml::activeCheckBox($product, 'inversion', [
                                            'checked' => $product->inversion ? ('checked') : ('')
                                        ]); ?>
                                        Да
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-2 control-label">Вид вывода товара: </label>
                                    <div class="col-md-10">
                                        <label class="radio">
                                            <?= CHtml::activeRadioButton($product, 'view',
                                                array(
                                                    'checked' => !$product->view ? ('checked') : (''),
                                                    'value' => 0,
                                                    'uncheckValue' => null
                                                )
                                            ) ?>
                                            Одиночный
                                        </label>
                                        <label class="radio">
                                            <?= CHtml::activeRadioButton($product, 'view',
                                                array(
                                                    'checked' => $product->view == 1 ? ('checked') : (''),
                                                    'value' => 1,
                                                    'uncheckValue' => null
                                                )
                                            ) ?>
                                            Двойной
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Категория: </label>
                                    <div class="col-md-10">
                                        <label class="radio">
                                            <?= CHtml::activeRadioButton($product, 'type',
                                                array(
                                                    'checked' => !$product->type ? ('checked') : (''),
                                                    'value' => 0,
                                                    'uncheckValue' => null
                                                )
                                            ) ?>
                                            Пленки
                                        </label>
                                        <label class="radio">
                                            <?= CHtml::activeRadioButton($product, 'type',
                                                array(
                                                    'checked' => $product->type == 1 ? ('checked') : (''),
                                                    'value' => 1,
                                                    'uncheckValue' => null
                                                )
                                            ) ?>
                                            Наборы
                                        </label>
                                        <label class="radio">
                                            <?= CHtml::activeRadioButton($product, 'type',
                                                array(
                                                    'checked' => $product->type == 2 ? ('checked') : (''),
                                                    'value' => 2,
                                                    'uncheckValue' => null
                                                )
                                            ) ?>
                                            Аксессуары
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Название товара:</label>
                                    <div class="col-md-10 input-width-xxlarge">
                                        <?= CHtml::activeTextField($product, 'title',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $product->title
                                            )
                                        ); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Подзаголовок товара:</label>
                                    <div class="col-md-10 input-width-xxlarge">
                                        <?= CHtml::activeTextField($product, 'undertitle',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $product->undertitle
                                            )
                                        ); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Описание товара:</label>
                                    <div class="col-md-10">
                                        <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                                            'model' => $product,
                                            'attribute' => 'description',
                                            'language' => 'ru',
                                        )); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Анонс: </label>
                                    <div class="col-md-10">
                                        <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                                            'model' => $product,
                                            'attribute' => 'previewText',
                                            'language' => 'ru',
                                        )); ?>
                                    </div>
                                </div>

                                <div class="form-group" style="display: flex; align-items: center">
                                    <label class="col-md-2 control-label">Стоимость товара:</label>
                                    <div class="col-md-10 input-width-large">
                                        <?= CHtml::activeNumberField($product, 'cost',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $product->cost
                                            )
                                        ); ?>
                                    </div>
                                    руб.
                                </div>
                                <div class="form-group" style="display: flex; align-items: center">
                                    <label class="col-md-2 control-label">Скидка:</label>
                                    <div class="col-md-10 input-width-large">
                                        <?= CHtml::activeNumberField($product, 'discount',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $product->discount
                                            )
                                        ); ?>
                                    </div>
                                    руб.
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Лейбл:</label>
                                    <div class="col-md-10">
                                        <select class="col-md-4" name="Products[labels_id]" data-role = "select2">
                                            <option value="0">Нет</option>
                                            <? foreach ($labels as $key => $item): ?>
                                                <option value="<?= $item->id ?>" <?= $product->labels_id == $item->id ? 'selected' : '' ?>><?= $item->LabelName ?></option>
                                            <? endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group " style="display: flex; align-items: center; flex-wrap: wrap">
                                    <label class="col-md-2 control-label">Параметры товара:</label>
                                    <div class="col-md-10">
                                        <? if (!empty($product->productsParams)) {
                                            foreach ($product->productsParams as $param): ?>
                                                <table style="font-weight: normal; width: 100%">
                                                    <tr>
                                                        <th>Название</th>
                                                        <th>Скидка (руб.)</th>
                                                        <th>Стоимость (руб.)</th>
                                                    </tr>
                                                </table>
                                                <div class="params-block"
                                                     style="display: flex; align-items: center">

                                                    <input type="text" class="form-control" name="params[]"
                                                           placeholder="NAME"
                                                           value="<?= $param['params_name'] ?>" style=" width: 33%;">
                                                    <input type="text" class="form-control" name="params_discount[]"
                                                           placeholder="DISC"
                                                           value="<?= $param['params_discount'] ?>" style="width: 33%;">
                                                    <input type="text" class="form-control" name="params_cost[]"
                                                           placeholder="COST"
                                                           value="<?= $param['params_cost'] ?>" style="width: 33%;">

                                                    <input type="hidden" name="params_id[]" value="<?= $param['id'] ?>">
                                                    руб.
                                                    <?= CHtml::ajaxLink(
                                                        '<i class="icol-cross"></i>',
                                                        Yii::app()->createUrl('/admin/default/ParamsDelete'),
                                                        array(
                                                            'type' => 'POST',// method
                                                            'data' => array(
                                                                'id' => $param['id'],
                                                            ),
                                                            'success' => 'js:$(this).parent(".params-block").remove()'
                                                        ),
                                                        array('class' => 'btn btn-sm')
                                                    );
                                                    ?>
                                                </div>
                                            <? endforeach; ?>
                                            <a href="javascript:;" class="btn btn-sm ajax AddParams"><i
                                                        class="icol-add"></i></a>

                                        <? } else { ?>

                                            <div class="params-block"
                                                 style="display: flex; align-items: center">
                                                <input type="text" class="form-control" name="new-params[]"
                                                       placeholder="NAME" value="">
                                                <input type="text" class="form-control" name="new-params_discount[]"
                                                       placeholder="DISC"
                                                       value="">
                                                <input type="text" class="form-control" name="new-params_cost[]"
                                                       placeholder="COST"
                                                       value=""
                                                       style="margin-right: 5px">
                                                руб.
                                            </div>
                                            <a href="javascript:;" class="btn btn-sm ajax AddParams"><i
                                                        class="icol-add"></i></a>
                                        <? } ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Рекомендуемые:</label>
                                    <div class="col-md-10">
                                        <select name="ProductForget[]" multiple="multiple" class="multiple">
                                            <? if (!empty($forget)): ?>
                                                <?php foreach ($products as $product_forget): ?>
                                                    <option value="<?= $product_forget->id ?>" <?= in_array($product_forget->id, $forget) == true ? ('selected') : ('') ?>><?= $product_forget->title?> <?=$product_forget->undertitle ? '('.$product_forget->undertitle.')' : ''?></option>
                                                <? endforeach; ?>
                                            <? else: ?>
                                                <?php foreach ($products as $product_forget): ?>
                                                    <option value="<?= $product_forget->id ?>"><?= $product_forget->title?> <?=$product_forget->undertitle ? '('.$product_forget->undertitle.')' : ''?></option>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="box_tab2">
                            <div class="form-horizontal row-border">
                                <div class="form-group">
                                    <div class="col-md-12" style="display: flex; flex-wrap: wrap">
                                        <? foreach ($product->productsImages as $key => $image): ?>
                                            <div class="productPreviewImageItem">
                                                <a data-fancybox="product-<?= $key ?>"
                                                   href="/uploads/shop/<?= $image['image_url'] ?>">
                                                    <img src="/uploads/shop/icon_<?= $image['image_url'] ?>">
                                                </a>
                                                <input type="hidden" name="image[imageID][<?= $key ?>]"
                                                       value="<?= $image['id'] ?>">
                                                <div class="productPreviewImage__content">
                                                    <div class="productPreviewImage__preview">
                                                        <input type="checkbox" id="image[imagePreview][<?= $key ?>]"
                                                               name="image[imagePreview][<?= $key ?>]"
                                                            <?= $image['preview'] ? 'checked="checked"' : '' ?>
                                                        >
                                                        На главной
                                                    </div>
                                                    <input type="text" name="image[imageParam][<?= $key ?>]"
                                                           placeholder="Название параметра"
                                                           value="<?= $image['params_name'] ?>">
                                                    <input type="text" name="image[imageName][<?= $key ?>]"
                                                           placeholder="Название изображения"
                                                           value="<?= $image['image_name'] ?>">
                                                    <?= CHtml::ajaxLink('Удалить',
                                                        Yii::app()->createUrl('/admin/products/DeleteImage'),
                                                        [
                                                            'type' => 'POST',
                                                            'data' => [
                                                                'imageID' => $image['id']
                                                            ],
                                                            'success' => 'js:$(this).closest(".productPreviewImageItem").remove()'
                                                        ],
                                                        [
                                                            'class' => 'removeLink'
                                                        ]);
                                                    ?>
                                                </div>
                                            </div>
                                        <? endforeach; ?>
                                        <input type="hidden" name="ProductID" value="<?= $product->id ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8">
                                        <div id="dropzone_profile_photo"></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane" id="box_tab3">
                            <div class="form-horizontal row-border">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Документы:</label>
                                    <div class="col-md-9 files-block">
                                        <? if (!empty($product->productsDocs)) {
                                            foreach ($product->productsDocs as $doc): ?>
                                                <div class="input-width-xlarge"
                                                     style="display: flex; justify-content: center; flex-direction: column;">
                                                    <div>
                                                        <input type="text"
                                                               style="width: calc(100% - 44px); display: inline-block;"
                                                               class="form-control" name="productDocs[Text][]"
                                                               value="<?= $doc->doc_type ?>">
                                                        <?= CHtml::ajaxLink(
                                                            '<i style = "display: inline-block" class="icol-cross"></i>',
                                                            Yii::app()->createUrl('/admin/products/DocsDelete'),
                                                            array(
                                                                'type' => 'POST',// method
                                                                'data' => array(
                                                                    'id' => $doc->id,
                                                                ),
                                                                'success' => 'js:$(this).parent(".input-width-xlarge").remove()'
                                                            ),
                                                            array('class' => 'btn btn-sm', 'style' => 'display: inline-block')
                                                        );
                                                        ?>
                                                    </div>
                                                    <input type="file" name="productDocs[File][]" data-style="fileinput"
                                                           style="display: block"
                                                           data-placeholder="<?= $doc->doc_url ?>"
                                                           data-buttontext="Перезалить...">
                                                </div>
                                            <? endforeach;
                                        } else { ?>
                                            <div class="input-width-xlarge"
                                                 style="display: flex; justify-content: center; flex-direction: column">
                                                <input type="text" class="form-control" name="productDocs[Text][]"
                                                       value=""
                                                       placeholder="Название документа">
                                                <input type="file" name="productDocs[File][]" data-style="fileinput"
                                                       style="display: block" data-buttontext="Выбрать...">
                                            </div>
                                        <? } ?>
                                    </div>
                                    <div class="col-md-1">
                                        <a href="javascript:;" class="btn btn-sm ajax addDocs"><i class="icol-add"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane" id="box_tab4">
                            <div class="form-horizontal row-border">
                                <? //= CHtml::beginForm(Yii::app()->createUrl('/admin/products/product', ['action' => 'seo', 'ProductID' => $product->id]), 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']); ?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">(SEO) Заголовок:</label>
                                    <div class="col-md-10 input-width-xxlarge">
                                        <?= CHtml::activeTextField($product, 'seo_title',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $product->seo_title
                                            )
                                        ); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">(SEO) Описание:</label>
                                    <div class="col-md-10">
                                        <?= CHtml::activeTextArea($product, 'seo_description',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $product->seo_description,
                                                'cols' => 5,
                                                'rows' => 3
                                            )
                                        ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                    </div>
                </div>
                <?=CHtml::endForm()?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function ($) {
        var myDropzone = new Dropzone('div#dropzone_profile_photo', {
            url: '<?=$this->createUrl('/admin/products/UploadImage')?>',
            previewsContainer: "#dropzone_profile_photo",
            paramName: "File", // имя переменной, используемой для передачи файлов
            parallelUploads: 1, //кол-во параллельных обращений к серверу
            acceptedFiles: 'image/*',
        });
        myDropzone.on('sending', function (file, xhr, formData) {
            var data = JSON.stringify({
                product_id: <?=$product->id?>,
            });
            formData.append("params", data);
        });
        myDropzone.on("addedfile", function (file) {
            // this is my confirm function, you can insert your
            if (!confirm("Do you want to upload the file?")) {
                this.removeFile(file);
                return false;
            }
        });
    });
</script>