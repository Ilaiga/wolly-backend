<?php
$this->pageTitle = $page->title;
$this->breadcrumbs = [
    'Страницы' => ['/admin/default/pages'],
    $this->pageTitle
];
?>

<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4><i class="icon-reorder"></i> <?= $page->title ?></h4>
            </div>
            <div class="widget-content">
                <?= CHtml::beginForm('', 'POST', ['class' => 'form-horizontal row-border']) ?>
                <?= CHtml::errorSummary($page) ?>

                <?if($content):?>

                <div class="form-group">
                    <label class="col-md-2 control-label">Заголовок:</label>
                    <div class="col-md-10 input-width-xxlarge">
                        <?= CHtml::activeTextField($page, 'title',
                            array(
                                'class' => 'form-control',
                                'value' => $page->title
                            )
                        ); ?>
                    </div>
                </div>

                <?endif;?>

                <div class="form-group">
                    <label class="col-md-2 control-label">(SEO) Заголовок:</label>
                    <div class="col-md-10 input-width-xxlarge">
                        <?= CHtml::activeTextField($page, 'seo_title',
                            array(
                                'class' => 'form-control',
                                'value' => $page->seo_title
                            )
                        ); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">(SEO) Описание:</label>
                    <div class="col-md-10 input-width-xxlarge">
                        <?= CHtml::activeTextField($page, 'seo_description',
                            array(
                                'class' => 'form-control',
                                'value' => $page->seo_description
                            )
                        ); ?>
                    </div>
                </div>

                <?if($content):?>

                <div class="form-group">
                    <label class="col-md-2 control-label">Ссылка на страницу (/pages/?):</label>
                    <div class="col-md-10 input-width-xxlarge">
                        <?= CHtml::activeTextField($page, 'url',
                            array(
                                'class' => 'form-control',
                                'value' => $page->url
                            )
                        ); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Контент:</label>
                    <div class="col-md-10">
                        <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                            'model' => $page,
                            'attribute' => 'content',
                            'language' => 'ru',
                        )); ?>
                    </div>
                </div>

                <?endif;?>

                <div class="form-group">
                    <div class="col-md-10">
                        <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                    </div>
                </div>
                <?= CHtml::endForm() ?>
            </div>
        </div>
    </div>
</div>

