<?php
$this->pageTitle = 'Страницы';
$this->breadcrumbs = [
    'Страницы'
];
?>

<div class="row" style = "margin-bottom: 25px;">
    <div class="col-md-12" style = "display: flex; justify-content: flex-end">
        <button class="btn btn-success" data-toggle="collapse" data-parent="#accordion" href="#promo">Добавить</button>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4><i class="icon-reorder"></i> Страницы</h4>
                <div class="toolbar no-padding">
                    <div class="btn-group">
                        <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                    </div>
                </div>
            </div>
            <div class="widget-content">
                <table class="table table-striped table-bordered table-hover table-checkable datatable">
                    <thead>
                    <tr>
                        <th>Заголовок</th>
                        <th>URL</th>
                        <th><i class="icon-edit"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    foreach ($pages as $page_key => $page_item):
                        ?>
                        <tr>
                            <td><?= $page_item->attributes['title'] ?></td>
                            <td><?= $page_item->attributes['url'] ?></td>
                            <td><a href = "<?= Yii::app()->createURL('admin/default/pages/', [
                                    'page' => $page_item->attributes['id']
                                ]) ?>">
                                    <i class="icon-edit"></i></a>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<div class="row collapse" id="promo">
    <div class="col-md-12">
        <div class="widget box">

            <div class="widget-content">
                <?= CHtml::beginForm('', 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
                <?= CHtml::errorSummary($FORM) ?>

                <div class="form-group">
                    <label class="col-md-2 control-label">Заголовок: </label>
                    <div class="col-md-10">
                        <?= CHtml::activeTextField($FORM, 'title', [
                            'value' => $FORM->title,
                            'class' => 'form-control input-width-xxlarge'
                        ]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Ссылка на страницу (/pages/?): </label>
                    <div class="col-md-10">
                        <?= CHtml::activeTextField($FORM, 'url', [
                            'value' => $FORM->url,
                            'class' => 'form-control input-width-xxlarge'
                        ]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Описание: </label>
                    <div class="col-md-10">
                        <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                            'model' => $FORM,
                            'attribute' => 'content',
                            'language' => 'ru',
                        )); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <?= CHtml::submitButton('Создать', array('class' => 'btn btn-success')); ?>
                    </div>
                </div>

                <?= CHtml::endForm() ?>
            </div>
        </div>
    </div>
</div>
