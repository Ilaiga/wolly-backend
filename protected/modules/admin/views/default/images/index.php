<?php
$this->pageTitle = 'Изображения';
$this->breadcrumbs = [
    'Изображения'
];
$productsTitle = $repeat = array();
//echo '<pre>'.print_r($products, true).'</pre>';
/*foreach($products as $product_key => $product_item) {
    echo $product_item->attributes['id'];
}*/
//die();
?>
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4><i class="icon-reorder"></i> Изображения для товаров</h4>
                <div class="toolbar no-padding">
                    <div class="btn-group">
                        <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                    </div>
                </div>
            </div>
            <div class="widget-content">
                <table class="table table-striped table-bordered table-hover table-checkable datatable">
                    <thead>
                    <tr>
                        <th>Товар</th>
                        <th>Изображение (URL)</th>
                        <th>Превью</th>

                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    foreach ($products as $product_key => $product_item):
                        $image_item = $images->findByAttributes(['products_id' => $product_item->attributes['id']]);


                        $productsTitle[$product_key]['title'] = $product_item->attributes['title'] . ' (' . $product_item->attributes['undertitle'] . ')';
                        $productsTitle[$product_key]['product_id'] = $product_item->attributes['id'];
                        ?>
                        <tr>
                            <td><?= $product_item->attributes['title'] . ' (' . $product_item->attributes['undertitle'] . ')' ?></td>
                            <td><?= $image_item ? $image_item->image_url : ('-') ?></td>
                            <td><?= $image_item ? $image_item->preview : ('-') ?></td>
                            <td>
                                <?= $image_item ? CHtml::ajaxLink(
                                    '<i class = "icon-remove"></i>',
                                    Yii::app()->createUrl('/admin/default/DeleteImages'),
                                    [
                                        'type' => 'POST',// method
                                        'data' => array(
                                            'imageId' => $image_item->id,
                                        ),
                                    ]
                                ) : ('-') ?>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="alert alert-danger fade in">
            <i class="icon-remove close" data-dismiss="alert"></i>
            <p><strong>ЗАГРУЗКА: </strong> Нужно указать данные, а потом грузить изображение</p>
        </div>
    </div>
    <div class="col-md-12">
        <div class="alert alert-warning fade in">
            <i class="icon-remove close" data-dismiss="alert"></i>
            <p><strong>ПАРАМЕТРЫ:</strong> Если у товара имеются параметры, то в поле нужно вбивать точно такой-же параметр</p>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-2">
            <label class=control-label">Выберите товар:</label>
            <select id="selectedProduct" class="form-control">
                <? foreach ($productsTitle as $productKey => $productItem): ?>
                    <option value="<?= $productItem['product_id'] ?>"><?= $productItem['title'] ?></option>
                <? endforeach; ?>
            </select>
            <div>
                Изображение карточки товара
                <input type="checkbox" id="product_preview" name="product_preview">
            </div>
            <div>
                Параметр
                <input type="text" id="product_params" name="product_params">
            </div>
        </div>
        <div class="col-md-10">
            <div id="dropzone_profile_photo"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function ($) {
        var myDropzone = new Dropzone('div#dropzone_profile_photo', {
            url: '<?=$this->createUrl('/admin/default/images')?>',
            previewsContainer: "#dropzone_profile_photo",
            paramName: "File", // имя переменной, используемой для передачи файлов
            maxFilesize: 5, // лимит размера файла в МБ
            parallelUploads: 1, //кол-во параллельных обращений к серверу
            acceptedFiles: 'image/*',
        });
        myDropzone.on('sending', function (file, xhr, formData) {
            var data = JSON.stringify({
                product_id: $('#selectedProduct').val(),
                product_preview: $('#product_preview').prop('checked'),
                product_params: $('#product_params').val()
            });
            formData.append("params", data);
        });
    });
</script>


