<?php
$this->pageTitle = $project->title;
$this->breadcrumbs = [
    'Проекты' => ['/admin/default/projects'],
    $this->pageTitle
];
?>

<? //= '<pre>' . print_r($project, true) . '</pre>'; ?>

<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4></h4>
            </div>
            <div class="widget-content">
                <div class="tabbable box-tabs">
                    <ul class="nav nav-tabs">
                        <li><a href="#box_tab3" data-toggle="tab">SEO</a></li>
                        <li><a href="#box_tab2" data-toggle="tab">Изображения</a></li>
                        <li class="active"><a href="#box_tab1" data-toggle="tab">Описание</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="box_tab1">
                            <?= CHtml::beginForm('', 'POST', ['class' => 'form-horizontal row-border']) ?>
                            <?= CHtml::errorSummary($project) ?>

                            <div class="form-group">
                                <label class="col-md-2 control-label">(SEO) Заголовок:</label>
                                <div class="col-md-10 input-width-xxlarge">
                                    <?= CHtml::activeTextField($project, 'seo_title',
                                        array(
                                            'class' => 'form-control',
                                            'value' => $project->seo_title
                                        )
                                    ); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">(SEO) Описание:</label>
                                <div class="col-md-10 input-width-xxlarge">
                                    <?= CHtml::activeTextField($project, 'seo_description',
                                        array(
                                            'class' => 'form-control',
                                            'value' => $project->seo_description
                                        )
                                    ); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Тип: </label>
                                <div class="col-md-10">
                                    <label class="radio">
                                        <?= CHtml::activeRadioButton($project, 'type',
                                            array(
                                                'checked' => !$project->type ? ('checked') : (''),
                                                'value' => 0,
                                                'uncheckValue' => null
                                            )
                                        ) ?>
                                        Проект
                                    </label>
                                    <label class="radio">
                                        <?= CHtml::activeRadioButton($project, 'type',
                                            array(
                                                'checked' => $project->type == 1 ? ('checked') : (''),
                                                'value' => 1,
                                                'uncheckValue' => null
                                            )
                                        ) ?>
                                        Новость
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Заголовок:</label>
                                <div class="col-md-10 input-width-xxlarge">
                                    <?= CHtml::activeTextField($project, 'title',
                                        array(
                                            'class' => 'form-control',
                                            'value' => $project->title
                                        )
                                    ); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Подзаголовок:</label>
                                <div class="col-md-10 input-width-xxlarge">
                                    <?= CHtml::activeTextField($project, 'undertitle',
                                        array(
                                            'class' => 'form-control',
                                            'value' => $project->undertitle
                                        )
                                    ); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Задача:</label>
                                <div class="col-md-10">
                                    <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                                        'model' => $project,
                                        'attribute' => 'task',
                                        'language' => 'ru',
                                    )); ?>
                                </div>
                            </div>
                            <!-- descritption -->
                            <div class="form-group">
                                <label class="col-md-2 control-label">Описание:</label>
                                <div class="col-md-10">
                                    <?= CHtml::activeTextArea($project, 'descritption', [
                                        'class' => 'form-control'
                                    ]); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Описание (в карточке товара):</label>
                                <div class="col-md-10">
                                    <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                                        'model' => $project,
                                        'attribute' => 'content',
                                        'language' => 'ru',
                                    )); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                                </div>
                            </div>
                            <?= CHtml::endForm() ?>
                        </div>
                        <div class="tab-pane" id="box_tab2">
                            <?= CHtml::beginForm('', 'POST', []); ?>
                            <div class="col-md-12" style="display: flex; flex-wrap;">
                                <? foreach ($project->projectImgs as $key => $image): ?>
                                    <div class="imageItem"
                                         style="margin-right: 10px; margin-bottom: 10px; width: 150px;">
                                        <a data-fancybox="product-<?= $image['id'] ?>"
                                           href="<?= $image['image_url'] ?>">
                                            <img src="<?= $image['image_url'] ?>" style="width: 100px; height: 100px"/>
                                        </a>
                                        <?= CHtml::ajaxLink(
                                            '<i class="icol-cross"></i>',
                                            Yii::app()->createUrl('/admin/default/DeleteImage'),
                                            [
                                                'type' => 'POST',
                                                'data' => [
                                                    'type' => 0,
                                                    'image_id' => $image['id'],
                                                ],
                                                'success' => 'js:$(this).closest(".imageItem").remove()'
                                            ],
                                            [
                                                'class' => 'btn btn-sm'
                                            ]
                                        ); ?>

                                    </div>
                                <? endforeach; ?>
                            </div>

                            <div class="col-md-8">
                                <div id="dropzone_profile_photo"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                                </div>
                            </div>

                            <?= CHtml::endForm() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function ($) {
        var myDropzone = new Dropzone('div#dropzone_profile_photo', {
            url: '<?=$this->createUrl('/admin/default/ProjectImage')?>',
            previewsContainer: "#dropzone_profile_photo",
            paramName: "File", // имя переменной, используемой для передачи файлов
            maxFilesize: 5, // лимит размера файла в МБ
            parallelUploads: 1, //кол-во параллельных обращений к серверу
            acceptedFiles: 'image/*',
        });
        myDropzone.on('sending', function (file, xhr, formData) {
            var data = JSON.stringify({
                project_id: <?=$project->id?>,
            });
            formData.append("params", data);
        });
        myDropzone.on("addedfile", function (file) {
            // this is my confirm function, you can insert your
            if (!confirm("Do you want to upload the file?")) {
                this.removeFile(file);
                return false;
            }
        });
    });
</script>
