<?php
$this->pageTitle = 'Проекты';
$this->breadcrumbs = [
    'Проекты'
];
?>

<div class="row" style = "margin-bottom: 25px;">
    <div class="col-md-12" style = "display: flex; justify-content: flex-end">
        <button class="btn btn-success" data-toggle="collapse" data-parent="#accordion" href="#promo">Добавить</button>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-content">
                <table class="table table-striped table-bordered table-hover table-checkable datatable">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>ТИП</th>
                        <th><i class="icon-edit"></i></th>
                        <th><i class="icon-remove"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    foreach ($projects as $project_key => $project_item):
                        ?>
                        <tr class = "projectItem">
                            <td><?= $project_item->attributes['title'] ?></td>
                            <td><?= $project_item->getType() ?></td>
                            <td><a href="<?= Yii::app()->createURL('admin/default/projects/', [
                                    'project' => $project_item->attributes['id']
                                ]) ?>"><i class="icon-edit"></i></a>
                            </td>
                            <td>
                                <?= CHtml::ajaxLink(
                                    '<i class="icon-remove"></i>',
                                    Yii::app()->createUrl('/admin/default/deleteProject'),
                                    [
                                        'type' => 'POST',
                                        'data' => ['projectId' => $project_item->attributes['id']],
                                        'success' => 'js:$(this).closest(".projectItem").remove()'
                                    ]
                                ); ?>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row collapse" id="promo">
    <div class="col-md-12">
        <div class="widget box">

            <div class="widget-content">
                <?= CHtml::beginForm('', 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
                <?= CHtml::errorSummary($FORM) ?>

                <div class="form-group">
                    <label class="col-md-2 control-label">Вид вывода товара: </label>
                    <div class="col-md-10">
                        <label class="radio">
                            <?= CHtml::activeRadioButton($FORM, 'type',
                                array(
                                    'checked' => !$FORM->type ? ('checked') : (''),
                                    'value' => 0,
                                    'uncheckValue' => null
                                )
                            ) ?>
                            Проект
                        </label>
                        <label class="radio">
                            <?= CHtml::activeRadioButton($FORM, 'type',
                                array(
                                    'checked' => $FORM->type == 1 ? ('checked') : (''),
                                    'value' => 1,
                                    'uncheckValue' => null
                                )
                            ) ?>
                            Новость
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-2 control-label">Заголовок: </label>
                    <div class="col-md-10">
                        <?= CHtml::activeTextField($FORM, 'title', [
                            'value' => $FORM->title,
                            'class' => 'form-control input-width-xxlarge'
                        ]); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Под заголовок (только проекты): </label>
                    <div class="col-md-10">
                        <?= CHtml::activeTextField($FORM, 'title', [
                            'value' => $FORM->undertitle,
                            'class' => 'form-control input-width-xxlarge'
                        ]); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Анонс: </label>
                    <div class="col-md-10">
                        <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                            'model' => $FORM,
                            'attribute' => 'descritption',
                            'language' => 'ru',
                        )); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Детальное описание: </label>
                    <div class="col-md-10">
                        <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                            'model' => $FORM,
                            'attribute' => 'content',
                            'language' => 'ru',
                        )); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Задачи: </label>
                    <div class="col-md-10">
                        <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                            'model' => $FORM,
                            'attribute' => 'task',
                            'language' => 'ru',
                        )); ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-10">
                        <?= CHtml::submitButton('Создать', array('class' => 'btn btn-success')); ?>
                    </div>
                </div>

                <?= CHtml::endForm() ?>
            </div>
        </div>
    </div>
</div>

