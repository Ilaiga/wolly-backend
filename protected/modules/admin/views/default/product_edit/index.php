<?php
$this->pageTitle = $product->title;
$this->breadcrumbs = [
    'Продукты' => array('/admin/default/product'),
    $product->title . ' (' . $product->undertitle . ')'
];
?>
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4></h4>
            </div>
            <div class="widget-content">
                <div class="tabbable box-tabs">
                    <ul class="nav nav-tabs">
                        <li><a href="#box_tab3" data-toggle="tab">SEO</a></li>
                        <li><a href="#box_tab2" data-toggle="tab">Изображения</a></li>
                        <li class="active"><a href="#box_tab1" data-toggle="tab">Описание</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="box_tab1">
                            <?= CHtml::beginForm('', 'POST', ['class' => 'form-horizontal row-border', 'enctype' => 'multipart/form-data']) ?>
                            <?= CHtml::errorSummary($product) ?>


                            <div class="form-group">
                                <label class="col-md-2 control-label">(SEO) Заголовок:</label>
                                <div class="col-md-10 input-width-xxlarge">
                                    <?= CHtml::activeTextField($product, 'seo_title',
                                        array(
                                            'class' => 'form-control',
                                            'value' => $product->seo_title
                                        )
                                    ); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">(SEO) Описание:</label>
                                <div class="col-md-10">
                                    <?= CHtml::activeTextArea($product, 'seo_description',
                                        array(
                                            'class' => 'form-control',
                                            'value' => $product->seo_description,
                                            'cols' => 5,
                                            'rows' => 3
                                        )
                                    ); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Сортировка (по убыванию): </label>
                                <div class="col-md-10">
                                    <?= CHtml::activeNumberField($product, 'sort', [
                                        'value' => $product->sort,
                                        'class' => 'form-control input-width-small'
                                    ]); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">В наличии: </label>
                                <div class="col-md-10">
                                    <?= CHtml::activeCheckBox($product, 'quantity', [
                                        'checked' => $product->quantity ? ('checked') : ('')
                                    ]); ?>
                                    Да
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Белый цвет: </label>
                                <div class="col-md-10">
                                    <?= CHtml::activeCheckBox($product, 'inversion', [
                                        'checked' => $product->inversion ? ('checked') : ('')
                                    ]); ?>
                                    Да
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-md-2 control-label">Вид вывода товара: </label>
                                <div class="col-md-10">
                                    <label class="radio">
                                        <?= CHtml::activeRadioButton($product, 'view',
                                            array(
                                                'checked' => !$product->view ? ('checked') : (''),
                                                'value' => 0,
                                                'uncheckValue' => null
                                            )
                                        ) ?>
                                        Одиночный
                                    </label>
                                    <label class="radio">
                                        <?= CHtml::activeRadioButton($product, 'view',
                                            array(
                                                'checked' => $product->view == 1 ? ('checked') : (''),
                                                'value' => 1,
                                                'uncheckValue' => null
                                            )
                                        ) ?>
                                        Двойной
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Категория: </label>
                                <div class="col-md-10">
                                    <label class="radio">
                                        <?= CHtml::activeRadioButton($product, 'type',
                                            array(
                                                'checked' => !$product->type ? ('checked') : (''),
                                                'value' => 0,
                                                'uncheckValue' => null
                                            )
                                        ) ?>
                                        Пленки
                                    </label>
                                    <label class="radio">
                                        <?= CHtml::activeRadioButton($product, 'type',
                                            array(
                                                'checked' => $product->type == 1 ? ('checked') : (''),
                                                'value' => 1,
                                                'uncheckValue' => null
                                            )
                                        ) ?>
                                        Наборы
                                    </label>
                                    <label class="radio">
                                        <?= CHtml::activeRadioButton($product, 'type',
                                            array(
                                                'checked' => $product->type == 2 ? ('checked') : (''),
                                                'value' => 2,
                                                'uncheckValue' => null
                                            )
                                        ) ?>
                                        Аксессуары
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Название товара:</label>
                                <div class="col-md-10 input-width-xxlarge">
                                    <?= CHtml::activeTextField($product, 'title',
                                        array(
                                            'class' => 'form-control',
                                            'value' => $product->title
                                        )
                                    ); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Подзаголовок товара:</label>
                                <div class="col-md-10 input-width-xxlarge">
                                    <?= CHtml::activeTextField($product, 'undertitle',
                                        array(
                                            'class' => 'form-control',
                                            'value' => $product->undertitle
                                        )
                                    ); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Описание товара:</label>
                                <div class="col-md-10">
                                    <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                                        'model' => $product,
                                        'attribute' => 'description',
                                        'language' => 'ru',
                                    )); ?>
                                </div>
                            </div>
                            <div class="form-group" style="display: flex; align-items: center">
                                <label class="col-md-2 control-label">Стоимость товара:</label>
                                <div class="col-md-10 input-width-large">
                                    <?= CHtml::activeNumberField($product, 'cost',
                                        array(
                                            'class' => 'form-control',
                                            'value' => $product->cost
                                        )
                                    ); ?>
                                </div>
                                руб.
                            </div>
                            <div class="form-group" style="display: flex; align-items: center">
                                <label class="col-md-2 control-label">Скидка:</label>
                                <div class="col-md-10 input-width-large">
                                    <?= CHtml::activeNumberField($product, 'discount',
                                        array(
                                            'class' => 'form-control',
                                            'value' => $product->discount
                                        )
                                    ); ?>
                                </div>
                                руб.
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Лейбл:</label>
                                <div class="col-md-10" style="margin-bottom: 25px;">
                                    <label class="radio">
                                        <?= CHtml::activeRadioButton($product, 'sale',
                                            array(
                                                'checked' => $product->sale == 1 ? ('checked') : (''),
                                                'value' => 1,
                                                'uncheckValue' => null
                                            )
                                        ) ?>
                                        Скидка
                                    </label>
                                    <label class="radio">
                                        <?= CHtml::activeRadioButton($product, 'sale',
                                            array(
                                                'checked' => $product->sale == 2 ? ('checked') : (''),
                                                'value' => 2,
                                                'uncheckValue' => null
                                            )
                                        ) ?>
                                        Хит
                                    </label>
                                    <label class="radio">
                                        <?= CHtml::activeRadioButton($product, 'sale',
                                            array(
                                                'checked' => $product->sale == 3 ? ('checked') : (''),
                                                'value' => 3,
                                                'uncheckValue' => null
                                            )
                                        ) ?>
                                        Новинка
                                    </label>
                                    <label class="radio">
                                        <?= CHtml::activeRadioButton($product, 'sale',
                                            array(
                                                'checked' => !$product->sale ? ('checked') : (''),
                                                'value' => 0,
                                                'uncheckValue' => null
                                            )
                                        ) ?>
                                        Нет
                                    </label>
                                </div>
                                <label class="col-md-2 control-label">Свой лейбл:</label>
                                <div class="col-md-10">
                                    <div class="input-width-large" style="display: block; margin-bottom: 10px">
                                        <?= CHtml::activeTextField($product, 'label',
                                            array(
                                                'class' => 'form-control ',
                                                'value' => $product->label,
                                                'placeholder' => 'Название скидки '
                                            )
                                        ); ?>
                                    </div>
                                    <div class="input-width-large">
                                        <?= CHtml::activeTextField($product, 'color_label',
                                            array(
                                                'class' => 'form-control ',
                                                'value' => $product->color_label,
                                                'placeholder' => 'HEX color '
                                            )
                                        ); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group " style="display: flex; align-items: center; flex-wrap: wrap">
                                <label class="col-md-2 control-label">Параметры товара:</label>
                                <div class="col-md-10">
                                    <? if (!empty($product->productsParams)) {
                                        foreach ($product->productsParams as $param): ?>
                                            <table style="font-weight: normal; width: 100%">
                                                <tr>
                                                    <th>Название</th>
                                                    <th>Скидка (руб.)</th>
                                                    <th>Стоимость (руб.)</th>
                                                </tr>
                                            </table>
                                            <div class="params-block"
                                                 style="display: flex; align-items: center">

                                                <input type="text" class="form-control" name="params[]"
                                                       placeholder="NAME"
                                                       value="<?= $param['params_name'] ?>" style=" width: 33%;">
                                                <input type="text" class="form-control" name="params_discount[]"
                                                       placeholder="DISC"
                                                       value="<?= $param['params_discount'] ?>" style="width: 33%;">
                                                <input type="text" class="form-control" name="params_cost[]"
                                                       placeholder="COST"
                                                       value="<?= $param['params_cost'] ?>" style="width: 33%;">

                                                <input type="hidden" name="params_id[]" value="<?= $param['id'] ?>">
                                                руб.
                                                <?= CHtml::ajaxLink(
                                                    '<i class="icol-cross"></i>',
                                                    Yii::app()->createUrl('/admin/default/ParamsDelete'),
                                                    array(
                                                        'type' => 'POST',// method
                                                        'data' => array(
                                                            'id' => $param['id'],
                                                        ),
                                                        'success' => 'js:$(this).parent(".params-block").remove()'
                                                    ),
                                                    array('class' => 'btn btn-sm')
                                                );
                                                ?>
                                            </div>
                                        <? endforeach; ?>
                                        <a href="javascript:;" class="btn btn-sm ajax AddParams"><i
                                                    class="icol-add"></i></a>

                                    <? } else { ?>

                                        <div class="params-block"
                                             style="display: flex; align-items: center">
                                            <input type="text" class="form-control" name="new-params[]"
                                                   placeholder="NAME" value="">
                                            <input type="text" class="form-control" name="new-params_discount[]"
                                                   placeholder="DISC"
                                                   value="">
                                            <input type="text" class="form-control" name="new-params_cost[]"
                                                   placeholder="COST"
                                                   value=""
                                                   style="margin-right: 5px">
                                            руб.
                                        </div>
                                        <a href="javascript:;" class="btn btn-sm ajax AddParams"><i
                                                    class="icol-add"></i></a>
                                    <? } ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Документы:</label>
                                <div class="col-md-9 files-block">
                                    <? if (!empty($product->productsDocs)) {
                                        foreach ($product->productsDocs as $doc): ?>
                                            <div class="input-width-xlarge"
                                                 style="display: flex; justify-content: center; flex-direction: column;">
                                                <div>
                                                    <input type="text"
                                                           style="width: calc(100% - 44px); display: inline-block;"
                                                           class="form-control" name="productDocs[Text][]"
                                                           value="<?= $doc->doc_type ?>">
                                                    <?= CHtml::ajaxLink(
                                                        '<i style = "display: inline-block" class="icol-cross"></i>',
                                                        Yii::app()->createUrl('/admin/default/DocsDelete'),
                                                        array(
                                                            'type' => 'POST',// method
                                                            'data' => array(
                                                                'id' => $doc->id,
                                                            ),
                                                            'success' => 'js:$(this).parent(".input-width-xlarge").remove()'
                                                        ),
                                                        array('class' => 'btn btn-sm', 'style' => 'display: inline-block')
                                                    );
                                                    ?>
                                                </div>
                                                <input type="file" name="productDocs[File][]" data-style="fileinput"
                                                       style="display: block">
                                            </div>
                                        <? endforeach;
                                    } else { ?>
                                        <div class="input-width-xlarge"
                                             style="display: flex; justify-content: center; flex-direction: column">
                                            <input type="text" class="form-control" name="productDocs[Text][]" value=""
                                                   placeholder="Название документа">
                                            <input type="file" name="productDocs[File][]" data-style="fileinput"
                                                   style="display: block">
                                        </div>
                                    <? } ?>
                                </div>
                                <div class="col-md-1">
                                    <a href="javascript:;" class="btn btn-sm ajax addDocs"><i class="icol-add"></i></a>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">Рекомендуемые:</label>
                                <div class="col-md-10">
                                    <select name="ProductForget[]" multiple="multiple" class="multiple">
                                        <? if (!empty($forget)): ?>
                                            <?php foreach ($products as $product_forget): ?>
                                                <option value="<?= $product_forget->id ?>" <?= in_array($product_forget->id, $forget) == true ? ('selected') : ('') ?>><?= $product_forget->title ?></option>
                                            <? endforeach; ?>
                                        <? else: ?>
                                            <?php foreach ($products as $product_forget): ?>
                                                <option value="<?= $product_forget->id ?>"><?= $product_forget->title ?></option>
                                            <? endforeach; ?>
                                        <? endif; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                                </div>
                            </div>
                            <?= CHtml::endForm() ?>
                        </div>
                        <div class="tab-pane" id="box_tab2">

                            <?= CHtml::beginForm('/admin/default/SaveImage', 'POST', []); ?>
                            <div class="col-md-12" style="display: flex; flex-wrap;">
                                <? foreach ($product->productsImages as $key => $image): ?>
                                    <div class="imageItem"
                                         style="margin-right: 10px; margin-bottom: 10px; width: 150px;">
                                        <a data-fancybox="product-<?= $image['id'] ?>"
                                           href="<?= $image['image_url'] ?>">
                                            <img src="<?= $image['image_url'] ?>" style="width: 100px; height: 100px"/>
                                        </a>

                                        <?= CHtml::activeTextField($image, 'image_name[]', [
                                            'class' => 'form-control',
                                            'placeholder' => 'название изображения',
                                            'value' => $image->image_name
                                        ]) ?>

                                        <?= CHtml::activeTextField($image, 'params_name[]', [
                                            'class' => 'form-control',
                                            'placeholder' => 'параметр',
                                            'value' => $image->params_name
                                        ]) ?>

                                        <?= CHtml::activeHiddenField($image, 'image_id[]', [
                                            'value' => $image->id
                                        ]) ?>

                                        <input type = "checkbox" name = "image_preview[<?=$key?>]" <?=$image->preview ? ('checked = "checked"') : ('')?>">


                                        <?= CHtml::ajaxLink(
                                            '<i class="icol-cross"></i>',
                                            Yii::app()->createUrl('/admin/default/DeleteImage'),
                                            [
                                                'type' => 'POST',
                                                'data' => [
                                                    'type' => 1,
                                                    'image_id' => $image['id'],
                                                ],
                                                'success' => 'js:$(this).closest(".imageItem").remove()'
                                            ],
                                            [
                                                'class' => 'btn btn-sm'
                                            ]
                                        ); ?>

                                    </div>
                                <? endforeach; ?>
                            </div>

                            <div class="col-md-8">
                                <div id="dropzone_profile_photo"></div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                                </div>
                            </div>

                            <?= CHtml::endForm() ?>
                        </div>
                        <div class="tab-pane" id="box_tab3">
                            <p>На данный момент настройки на главной</p>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(function ($) {
        var myDropzone = new Dropzone('div#dropzone_profile_photo', {
            url: '<?=$this->createUrl('/admin/default/ProductImage')?>',
            previewsContainer: "#dropzone_profile_photo",
            paramName: "File", // имя переменной, используемой для передачи файлов
            maxFilesize: 5, // лимит размера файла в МБ
            parallelUploads: 1, //кол-во параллельных обращений к серверу
            acceptedFiles: 'image/*',
        });
        myDropzone.on('sending', function (file, xhr, formData) {
            var data = JSON.stringify({
                product_id: <?=$product->id?>,
            });
            formData.append("params", data);
        });
        myDropzone.on("addedfile", function (file) {
            // this is my confirm function, you can insert your
            if (!confirm("Do you want to upload the file?")) {
                this.removeFile(file);
                return false;
            }
        });
    });
</script>