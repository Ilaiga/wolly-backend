<?php
$this->pageTitle = 'Товары';
$this->breadcrumbs = [
    'Товары'
];
?>

<div class="row">
    <div class="col-md-12">
        <div class="alert alert-info fade in">
            <i class="icon-remove close" data-dismiss="alert"></i>
            <p>Если товар <strong>не отображается</strong>, создайте для него изображения</p>
        </div>
    </div>
</div>

<? foreach ($products as $product_key => $product_item): ?>

    <div class="row">
        <div class="col-md-12">
            <div class="widget box">
                <div class="widget-header">
                    <h4><i class="icon-reorder"></i> <?= $product_key ?></h4>
                    <div class="toolbar no-padding">
                        <div class="btn-group">
                            <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                        </div>
                    </div>
                </div>
                <div class="widget-content">
                    <table class="table table-striped table-bordered table-hover table-checkable datatable">
                        <thead>
                        <tr>
                            <th>Название товара</th>
                            <th class="hidden-xs">Подзаголовок</th>
                            <th>SORT</th>
                            <th><i class="icon-edit"></i></th>
                            <th><i class="icon-remove"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($product_item as $item): ?>
                            <tr class = "productItem">
                                <td><?= $item->title ?></td>
                                <td class="hidden-xs"><?= $item->undertitle ?></td>
                                <td class="clickSort" data-product="<?= $item->id ?>">
                                    <span><?= $item->sort ?></span>

                                </td>
                                <td><a href="<?=
                                    Yii::app()->createUrl('/admin/default/product_edit', [
                                        'id' => $item->id
                                    ]); ?>">
                                        <i class="icon-edit"></i></a>
                                </td>
                                <td>
                                    <?= CHtml::ajaxLink(
                                        '<i class = "icon-remove"></i>',
                                        Yii::app()->createUrl('/admin/default/removeProduct'),
                                        [
                                            'type' => 'POST',
                                            'data' => [
                                                'productId' => $item->id
                                            ],
                                            'success' => 'js:$(this).closest(".productItem").remove()'
                                        ]
                                    ); ?>
                                </td>
                            </tr>
                        <? endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<? endforeach; ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('.clickSort').on('dblclick', function () {
            if ($(this).hasClass('active')) return true;
            let content = $(this).find('span');
            let data = content.html();
            content.remove();
            $(this).append('<input class = "sortInput form-control input-width-small" type = "text" value = "' + data + '">').addClass('active');
            $(this).find('.sortInput').focus();
            return false;
        });
    });
    $(document).on('blur', '.sortInput', function () {
        let data = $(this).val();
        let product_id = $(this).closest('.clickSort').data('product');
        if (data)
            $.ajax({
                url: '',
                type: 'POST',
                data: {
                    productId: product_id,
                    productSort: data
                },
                success: function (request) {

                }
            });
        $(this).closest('.clickSort').append('<span>' + data + '</span>').removeClass('active');
        $(this).remove();
    });
</script>
<!-- /Normal -->
