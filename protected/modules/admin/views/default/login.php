<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <title>Административная панель 0.1</title>
    <link href= "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type = "text/css">
    <script>
        $(document).ready(function(){
            "use strict";
            App.init(); // Init layout and core plugins
            Plugins.init(); // Init all plugins
            FormComponents.init(); // Init all form-specific plugins
        });
    </script>
</head>
<body class = "login">
<div class="box">
    <div class="content">
        <!-- Login Formular -->
        <?=CHtml::beginForm('','POST',array('class' => 'form-vertical login-form', 'id'=>'login-form')); ?>
            <!-- Title -->
        <h3 class="form-title">Авторизация</h3>

        <!-- Error Message -->
        <div class="alert fade in alert-danger" style="display: none;">
            <i class="icon-remove close" data-dismiss="alert"></i>
            Enter any username and password.
        </div>

        <!-- Input Fields -->
        <div class="form-group">
            <div class="input-icon">
                <i class="icon-user"></i>
                <?= CHtml::activeTextField($form, 'username',
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Логин',
                        'autofocus'=> 'autofocus'
                    )
                ); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="input-icon">
                <i class="icon-lock"></i>

                <?= CHtml::activePasswordField($form, 'password',
                    array(
                        'class' => 'form-control',
                        'placeholder' => 'Пароль'
                    )
                ); ?>
            </div>
        </div>
        <!-- /Input Fields -->

        <!-- Form Actions -->
        <div class="form-actions">
            <?= CHtml::submitButton("Авторизация", array('class' => 'submit btn btn-primary pull-right')); ?>
        </div>

        <?= CHtml::endForm() ?>
        <!-- /Login Formular -->

    </div> <!-- /.content -->

</div>
</body>
</html>
<!-- /Login Box -->