<?php
$this->pageTitle = 'Формы';
$this->breadcrumbs = [
    'Формы'
];
?>



<?foreach($forms as $form_key => $form_item): ?>
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4><i class="icon-reorder"></i> <?=strtolower($form_key)?></h4>
                <div class="toolbar no-padding">
                    <div class="btn-group">
                        <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                    </div>
                </div>
            </div>
            <div class="widget-content">
                <table class="table table-striped table-bordered table-hover table-checkable datatable">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Данные по форме</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?foreach($form_item as $form):?>
                            <tr>
                                <td><?=$form->id?></td>
                                <td>
                                    <?
                                    $array = json_decode($form->form_data);
                                    foreach($array as $jsonKey => $jsonItem):
                                        if($jsonKey == 'dataFormName') continue;?>
                                        <?=$form::translateData($jsonKey).': '.$jsonItem.'</br>'?>
                                    <?endforeach;?>
                                </td>
                            </tr>
                        <?endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?endforeach;?>