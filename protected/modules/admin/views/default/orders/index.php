<?php
$this->pageTitle = 'Заказы';
$this->breadcrumbs = [
    'Заказы'
];

//echo '<pre>'.print_r($orders, true).'</pre>';
//die();
?>


<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4><i class="icon-reorder"></i> Ваши заказы</h4>
                <div class="toolbar no-padding">
                    <div class="btn-group">
                        <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                    </div>
                </div>
            </div>
            <div class="widget-content">
                <table class="table table-striped table-bordered table-hover table-checkable datatable">
                    <thead>
                    <tr>
                        <th>Номер заказа</th>
                        <th>ФИО</th>
                        <th>Номер телефона</th>
                        <th>Почта</th>
                        <th>Адрес доставки</th>
                        <th>CDEK (Город)</th>
                        <th>CDEK (Адрес)</th>
                        <th>Тип оплаты</th>
                        <th>Сумма</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <? foreach ($orders as $order):
                        $cdek = (array)json_decode($order->order_cdek);
                        ?>
                        <tr>
                            <td><?= $order->id ?></td>
                            <td><?= $order->fio ?></td>
                            <td><?= $order->phone ?></td>
                            <td><?= $order->email ?></td>
                            <td><?= $order->adress != '-' ? $order->adress : ('самовывоз') ?></td>
                            <td><?= count($cdek) ? $cdek['Город']->value : '-' ?></td>
                            <td><?= count($cdek) ? $cdek['Адрес']->value : '-' ?></td>
                            <td><?= $order->getTypeOrder() ?></td>
                            <td><?= $order->totalcost ?></td>

                            <td><a data-toggle="modal" href="#anotherI<?= $order->id ?>"
                                   class=" icon-plus-sign-alt"></a></td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<? foreach ($orders as $order): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="modal fade" id="anotherI<?= $order->id ?>">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Заказ номер <?= $order->id ?></h4>
                        </div>
                        <div class="modal-body">
                            <table class="table table-striped table-bordered table-hover table-checkable datatable">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Количество</th>
                                    <th>Общая стоимость</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? if (!empty($order->product_data)) foreach (json_decode($order->product_data) as $data): ?>
                                    <tr>
                                        <td><?= $data[0].'('. $data[3]. ')'?></td>
                                        <td><?= $data[1] ?></td>
                                        <td><?= $data[2] ?></td>
                                    </tr>
                                <? endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? endforeach; ?>
