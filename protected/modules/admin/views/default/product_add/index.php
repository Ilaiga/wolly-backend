<?php
    $this->pageTitle = 'Новый товар';
    $this->breadcrumbs = ['Новый товар'];

    ?>

<div class="alert alert-danger fade in">
    <i class="icon-remove close" data-dismiss="alert"></i>
    <p>Название товара нужно указывать обязательно</p>
</div>
<div class="alert alert-warning fade in">
    <i class="icon-remove close" data-dismiss="alert"></i>
    <p>Если какого-то параметра не нашли используйте вкладку <strong>Редактирование товара</strong></p>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4><i class="icon-reorder"></i> <?=$product->title?></h4>
            </div>
            <div class="widget-content">
                <?=CHtml::beginForm('', 'POST', ['class' => 'form-horizontal row-border'])?>
                <?=CHtml::errorSummary($product)?>

                <div class="form-group">
                    <label class="col-md-2 control-label">В наличии: </label>
                    <div class="col-md-10">
                        <?= CHtml::activeCheckBox($product, 'quantity', [
                            'checked' => $product->quantity ? ('checked') : ('')
                        ]); ?>
                        Да
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Вид вывода товара: </label>
                    <div class="col-md-10">
                        <label class="radio">
                            <?=CHtml::activeRadioButton($product, 'view',
                                array(
                                    'checked' => !$product->type ? ('checked') : (''),
                                    'value' => 0,
                                    'uncheckValue' => null
                                )
                            )?>
                            Одиночный
                        </label>
                        <label class="radio">
                            <?=CHtml::activeRadioButton($product, 'view',
                                array(
                                    'checked' => $product->type == 1 ? ('checked') : (''),
                                    'value' => 1,
                                    'uncheckValue' => null
                                )
                            )?>
                            Двойной
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Категория: </label>
                    <div class="col-md-10">
                        <label class="radio">
                            <?=CHtml::activeRadioButton($product, 'type',
                                array(
                                    'checked' => !$product->type ? ('checked') : (''),
                                    'value' => 0,
                                    'uncheckValue' => null
                                )
                            )?>
                            Пленки
                        </label>
                        <label class="radio">
                            <?=CHtml::activeRadioButton($product, 'type',
                                array(
                                    'checked' => $product->type == 1 ? ('checked') : (''),
                                    'value' => 1,
                                    'uncheckValue' => null
                                )
                            )?>
                            Наборы
                        </label>
                        <label class="radio">
                            <?=CHtml::activeRadioButton($product, 'type',
                                array(
                                    'checked' => $product->type == 2 ? ('checked') : (''),
                                    'value' => 2,
                                    'uncheckValue' => null
                                )
                            )?>
                            Аксессуары
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Название товара:</label>
                    <div class="col-md-10 input-width-xxlarge">
                        <?=CHtml::activeTextField($product, 'title',
                            array(
                                'class' => 'form-control',
                                'value' => $product->title
                            )
                        );?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Подзаголовок товара:</label>
                    <div class="col-md-10 input-width-xxlarge">
                        <?=CHtml::activeTextField($product, 'undertitle',
                            array(
                                'class' => 'form-control',
                                'value' => $product->undertitle
                            )
                        );?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Описание товара:</label>
                    <div class="col-md-10 input-width-xxlarge">
                        <?=CHtml::activeTextArea($product, 'description',
                            array(
                                'class' => 'form-control wysiwyg',
                                'value' => $product->description
                            )
                        );?>
                    </div>
                </div>

                <div class="form-group" style = "display: flex; align-items: center">
                    <label class="col-md-2 control-label">Стоимость товара:</label>
                    <div class="col-md-10 input-width-large">
                        <?=CHtml::activeNumberField($product, 'cost',
                            array(
                                'class' => 'form-control',
                                'value' => $product->cost
                            )
                        );?>
                    </div>
                    руб.
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Тип скидки:</label>
                    <div class="col-md-10">
                        <label class="radio">
                            <?=CHtml::activeRadioButton($product, 'sale',
                                array(
                                    'checked' => $product->sale == 1 ? ('checked') : (''),
                                    'value' => 1,
                                    'uncheckValue' => null
                                )
                            )?>
                            Скидка
                        </label>
                        <label class="radio">
                            <?=CHtml::activeRadioButton($product, 'sale',
                                array(
                                    'checked' => $product->sale == 2 ? ('checked') : (''),
                                    'value' => 2,
                                    'uncheckValue' => null
                                )
                            )?>
                            Хит
                        </label>
                        <label class="radio">
                            <?=CHtml::activeRadioButton($product, 'sale',
                                array(
                                    'checked' => $product->sale == 3 ? ('checked') : (''),
                                    'value' => 3,
                                    'uncheckValue' => null
                                )
                            )?>
                            Новинка
                        </label>
                        <label class="radio">
                            <?=CHtml::activeRadioButton($product, 'sale',
                                array(
                                    'checked' => !$product->sale ? ('checked') : (''),
                                    'value' => 0,
                                    'uncheckValue' => null
                                )
                            )?>
                            Нет
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Свой тип скидки:</label>
                    <div class="col-md-10">
                        <div class="input-width-large" style="display: block; margin-bottom: 10px">
                            <?= CHtml::activeTextField($product, 'label',
                                array(
                                    'class' => 'form-control ',
                                    'value' => $product->label,
                                    'placeholder' => 'Название скидки '
                                )
                            ); ?>
                        </div>
                        <div class="input-width-large">
                            <?= CHtml::activeTextField($product, 'color_label',
                                array(
                                    'class' => 'form-control ',
                                    'value' => $product->color_label,
                                    'placeholder' => 'HEX color '
                                )
                            ); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group " style = "display: flex; align-items: center; flex-wrap: wrap">
                    <label class="col-md-2 control-label">Параметры товара:</label>
                    <div class="col-md-10 input-width-large params-block" style = "display: flex; align-items: center">
                        <input type = "text" class = "form-control" name = "new-params[]" value = "">
                        <input type = "text" class = "form-control" name = "new-params_cost[]" value = "" style = "margin-right: 5px">
                        руб.
                    </div>
                    <a href = "javascript:;" class="btn btn-sm ajax AddParams"><i class="icol-add"></i></a>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                    </div>
                </div>
                <?=CHtml::endForm()?>
            </div>
        </div>
    </div>
</div>