<?php
$this->pageTitle = 'Лейбл';
$this->breadcrumbs = [
    'Лейблы' => array('/admin/labels/'),
    'Лейбл'
];
?>


<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-content">
                <?= CHtml::beginForm('', 'POST', ['class' => 'form-horizontal row-border']); ?>
                <?= CHtml::errorSummary($label)?>
                <div class="form-group">
                    <label class="col-md-2">Активна: </label>
                    <div class="col-md-10">
                        <?= CHtml::activeCheckBox($label, 'Active', [
                            'checked' => $label->Active ? ('checked') : ('')
                        ]); ?>
                        Да
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">Название</label>
                    <div class="col-md-10">
                        <?= CHtml::activeTextField($label, 'LabelName', ['class' => 'form-control input-width-large', 'value' => $label->LabelName]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">Цвет. (Example: FFFFFF)</label>
                    <div class="col-md-10">
                        <?= CHtml::activeTextField($label, 'LabelColor', [
                            'class' => 'form-control bs-colorpicker',
                            'value' => $label->LabelColor,
                            'data-color-format' => 'hex'
                        ]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                    </div>
                </div>
                <?= CHtml::endForm() ?>
            </div>
        </div>
    </div>
</div>
