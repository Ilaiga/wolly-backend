<?php
$this->pageTitle = 'Лейблы';
$this->breadcrumbs = [
    'Лейблы'
];
?>
<div class="row" style = "margin-bottom: 25px;">
    <div class="col-md-12" style = "display: flex; justify-content: flex-end">
        <button class="btn btn-success" data-toggle="collapse" data-parent="#accordion" href="#promo">Добавить</button>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4><i class="icon-reorder"></i> Промо</h4>
                <div class="toolbar no-padding">
                    <div class="btn-group">
                        <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                    </div>
                </div>
            </div>
            <div class="widget-content">
                <table class="table table-striped table-bordered table-hover table-checkable datatable">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Цвет (#)</th>
                        <th>Активность</th>
                        <th><i class="icon-edit"></i></th>
                        <th><i class="icon-remove"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    foreach ($label as $key => $item):
                        ?>
                        <tr class = "item">
                            <td><?= $item->attributes['LabelName'] ?></td>
                            <td><?= $item->attributes['LabelColor'] ?></td>
                            <td><?= $item->attributes['Active'] ? ('<i class = "icon-ok"></i>') : ('<i class = "icon-remove"></i>') ?></td>
                            <td>
                                <a href="<?= Yii::app()->createUrl('/admin/labels/edit', ['id' => $item->attributes['id']]) ?>">
                                    <i class="icon-edit"></i>
                                </a>
                            </td>
                            <td>
                                <?= CHtml::ajaxLink(
                                    '<i class = "icon-remove"></i>',
                                    Yii::app()->createUrl('/admin/labels/delete', [
                                        'id' => $item->attributes['id']
                                    ]),
                                    [
                                        'type' => 'POST',
                                        'success' => 'js:$(this).closest(".item").remove()'
                                    ]
                                ); ?>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<div class="row collapse" id="promo">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-content">
                <?= CHtml::beginForm('', 'POST', ['class' => 'form-horizontal row-border']); ?>
                <?= CHtml::errorSummary($labelForm); ?>
                <div class="form-group">
                    <label class="col-md-2">Название</label>
                    <div class="col-md-10">
                        <?= CHtml::activeTextField($labelForm, 'LabelName', ['class' => 'form-control input-width-large']) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">Цвет. (Example: FFFFFF)</label>
                    <div class="col-md-10">

                        <?= CHtml::activeTextField($labelForm, 'LabelColor', [
                            'class' => 'form-control bs-colorpicker',
                            'value' => '#8fff00',
                            'data-color-format' => 'hex'
                        ]); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                    </div>
                </div>
                <?= CHtml::endForm() ?>
            </div>
        </div>
    </div>
</div>

