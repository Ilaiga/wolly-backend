<?php
$this->pageTitle = 'Скидка (' . $promo->title . ')';
$this->breadcrumbs = [
    'Промо' => array('/admin/promo/'),
    $this->pageTitle
];
?>


<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-content">
                <?= CHtml::beginForm('', 'POST', ['class' => 'form-horizontal row-border']); ?>
                <?= CHtml::errorSummary($promo)?>
                <div class="form-group">
                    <label class="col-md-2">Активна: </label>
                    <div class="col-md-10">
                        <?= CHtml::activeCheckBox($promo, 'status', [
                            'checked' => $promo->status ? ('checked') : ('')
                        ]); ?>
                        Да
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2">Название промокода</label>
                    <div class="col-md-10">
                        <?= CHtml::activeTextField($promo, 'title', ['class' => 'form-control input-width-large', 'value' => $promo->title]) ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2">Скидка</label>
                    <div class="col-md-10" style="display: flex">
                        <?= CHtml::activeTextField($promo, 'value', ['style' => 'margin-right: 10px','class' => 'form-control input-width-large', 'value' => $promo->value]) ?>
                        <select class="form-control input-width-small" name="Promo[valueType]">
                            <option value="1" <?=$promo->valueType == 1 ? ('selected') : ('')?>>руб.</option>
                            <option value="2" <?=$promo->valueType == 2 ? ('selected') : ('')?>>%</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2">Сумма корзины не менее (0 = отключено):</label>
                    <div class="col-md-10">
                        <?= CHtml::activeNumberField($promo, 'cartSum', ['class' => 'form-control input-width-large', 'value' => $promo->cartSum]) ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                    </div>
                </div>
                <?= CHtml::endForm() ?>
            </div>
        </div>
    </div>
</div>
