<?php
$this->pageTitle = 'Промо коды';
$this->breadcrumbs = [
    'Промо коды'
];
?>
<div class="row" style = "margin-bottom: 25px;">
    <div class="col-md-12" style = "display: flex; justify-content: flex-end">
        <button class="btn btn-success" data-toggle="collapse" data-parent="#accordion" href="#promo">Добавить</button>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4><i class="icon-reorder"></i> Промо</h4>
                <div class="toolbar no-padding">
                    <div class="btn-group">
                        <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                    </div>
                </div>
            </div>
            <div class="widget-content">
                <table class="table table-striped table-bordered table-hover table-checkable datatable">
                    <thead>
                    <tr>
                        <th>Значение</th>
                        <th>Скидка (руб.)</th>
                        <th>Активна</th>
                        <th><i class="icon-edit"></i></th>
                        <th><i class="icon-remove"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?
                    foreach ($promoAll as $key => $item):
                        ?>
                        <tr class="item">
                            <td><?= $item->attributes['title'] ?></td>
                            <td>-<?= $item->attributes['value'] ?></td>
                            <td><?= $item->attributes['status'] ? ('<i class = "icon-ok"></i>') : ('<i class = "icon-remove"></i>') ?></td>
                            <td>
                                <a href="<?= Yii::app()->createUrl('/admin/promo/edit', ['id' => $item->attributes['id']]) ?>">
                                    <i class="icon-edit"></i>
                                </a>
                            </td>
                            <td>
                                <?= CHtml::ajaxLink(
                                    '<i class = "icon-remove"></i>',
                                    Yii::app()->createUrl('/admin/promo/delete', [
                                        'id' => $item->attributes['id'],
                                    ]),
                                    [
                                        'type' => 'POST',
                                        'success' => 'js:$(this).closest(".item").remove()'
                                    ]
                                ); ?>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

<div class="row collapse" id="promo">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-content">
                <?= CHtml::beginForm('', 'POST', ['class' => 'form-horizontal row-border']); ?>
                <?= CHtml::errorSummary($promoForm); ?>
                <div class="form-group">
                    <label class="col-md-2">Название промокода</label>
                    <div class="col-md-10">
                        <?= CHtml::activeTextField($promoForm, 'title', ['class' => 'form-control input-width-large']) ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2">Скидка</label>
                    <div class="col-md-10" style="display: flex">
                        <?= CHtml::activeTextField($promoForm, 'value', ['style' => 'margin-right: 10px', 'class' => 'form-control input-width-large']) ?>
                        <select class="form-control input-width-small" name="Promo[valueType]">
                            <option value="1">руб.</option>
                            <option value="2">%</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-10">
                        <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                    </div>
                </div>
                <?= CHtml::endForm() ?>
            </div>
        </div>
    </div>
</div>

