<?php
$this->pageTitle = $page->title;
$this->breadcrumbs = [
    'Страницы' => ['/admin/pages'],
    $this->pageTitle
];
?>

<div class="row">
    <div class="col-md-12">
        <div class="widget box">
            <div class="widget-header">
                <h4><i class="icon-reorder"></i> <?= $page->title ?></h4>
            </div>
            <div class="widget-content">
                <div class="tabbable box-tabs">
                    <ul class="nav nav-tabs">
                        <? if ($content): ?>
                            <li><a href="#box_tab2" data-toggle="tab">SEO</a></li>
                            <li class="active"><a href="#box_tab1" data-toggle="tab">Основная часть</a></li>
                        <? elseif($page->url == 'cart'):?>
                            <li class="active"><a href="#box_tab2" data-toggle="tab">SEO</a></li>
                            <li><a href="#box_tab1" data-toggle="tab">Основная часть</a></li>
                        <? else: ?>
                            <li class="active"><a href="#box_tab2" data-toggle="tab">SEO</a></li>
                        <? endif ?>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane <?=$content ? 'active' : ''?>" id="box_tab1">
                            <?= CHtml::beginForm('', 'POST', ['class' => 'form-horizontal row-border']) ?>
                            <?= CHtml::errorSummary($page) ?>

                            <? if ($content): ?>

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Заголовок:</label>
                                    <div class="col-md-10 input-width-xxlarge">
                                        <?= CHtml::activeTextField($page, 'title',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $page->title
                                            )
                                        ); ?>
                                    </div>
                                </div>

                            <? endif; ?>

                            <? if ($content): ?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Ссылка на страницу (/pages/?):</label>
                                    <div class="col-md-10 input-width-xxlarge">
                                        <?= CHtml::activeTextField($page, 'url',
                                            array(
                                                'class' => 'form-control',
                                                'value' => $page->url
                                            )
                                        ); ?>
                                    </div>
                                </div>
                                <?if($page->url != 'faq'):?>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Контент:</label>
                                        <div class="col-md-10">
                                            <?php $this->widget('application.extensions.ckeditor.CKEditor', array(
                                                'model' => $page,
                                                'attribute' => 'content',
                                                'language' => 'ru',
                                            )); ?>
                                        </div>
                                    </div>
                                <?else:?>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Контент:</label>
                                        <div class="col-md-10">
                                            <?= CHtml::activeTextArea($page, 'content',
                                                array(
                                                    'class' => 'form-control',
                                                    'value' => $page->content
                                                )
                                            ); ?>
                                        </div>
                                    </div>
                                <?endif;?>
                            <? endif; ?>

                            <? if (!empty($showCart)): ?>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Рекомендуемые:</label>
                                    <div class="col-md-10">
                                        <select name="ProductForgetCart[]" multiple="multiple" class="multiple">
                                            <? if (!empty($cartProducts)): ?>
                                                <?php foreach ($products as $product_forget): ?>
                                                    <option value="<?= $product_forget->id ?>" <?= in_array($product_forget->id, $cartProducts) == true ? ('selected') : ('') ?>><?= $product_forget->title ?></option>
                                                <? endforeach; ?>
                                            <? else: ?>
                                                <?php foreach ($products as $product_forget): ?>
                                                    <option value="<?= $product_forget->id ?>"><?= $product_forget->title ?></option>
                                                <? endforeach; ?>
                                            <? endif; ?>
                                        </select>
                                    </div>
                                </div>
                            <? endif; ?>

                            <div class="form-group">
                                <div class="col-md-10">
                                    <?= CHtml::submitButton('Применить', array('class' => 'btn btn-success')); ?>
                                </div>
                            </div>

                            <?= CHtml::endForm() ?>
                        </div>
                        <div class="tab-pane <?=!$content ? 'active' : ''?>" id="box_tab2">
                            <?= CHtml::beginForm(Yii::app()->createURL('admin/pages/SEO', ['page' => $page->id]), 'POST', ['class' => 'form-horizontal row-border']) ?>
                            <?= CHtml::errorSummary($page) ?>
                            <div class="form-group">
                                <label class="col-md-2 control-label">(SEO) Заголовок:</label>
                                <div class="col-md-10 input-width-xxlarge">
                                    <?= CHtml::activeTextField($page, 'seo_title',
                                        array(
                                            'class' => 'form-control',
                                            'value' => $page->seo_title
                                        )
                                    ); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">(SEO) Описание:</label>
                                <div class="col-md-10 input-width-xxlarge">
                                    <?= CHtml::activeTextField($page, 'seo_description',
                                        array(
                                            'class' => 'form-control',
                                            'value' => $page->seo_description
                                        )
                                    ); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <?= CHtml::submitButton('Создать', array('class' => 'btn btn-success')); ?>
                                </div>
                            </div>
                            <?= CHtml::endForm()?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

