<?php

/**
 * This is the model class for table "User".
 *
 * The followings are the available columns in table 'products':
 * @property integer $id
 * @property string $password
 * @property string $username
 * @property integer $allow
 * @property string $lastLoginTime
 */


class User extends CActiveRecord
{
    public function tableName() {
        return 'Users';
    }
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}