<?php
    $data = (new Settings())->find();
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/main.css">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon-16x16.png">
    <title><?= CHtml::encode(!$this->seo_title ? $this->pageTitle : $this->seo_title); ?></title>

</head>
<body>

<div class="page-wrapper">
    <div class="page-buffer">
        <div class="js-navbar navbar">
            <div class="container">
                <div class="navbar__inner">
                    <div class="navbar__toggle">
                        <button class="js-navbar-toggle navbar-toggle" aria-expanded="false" data-toggle="collapse"
                                data-target="#navbarMenu">
                           <span class="navbar-toggle__inner">
                              <span class="navbar-toggle__sep"></span>
                              <span class="navbar-toggle__sep"></span>
                              <span class="navbar-toggle__sep"></span>
                           </span>
                        </button>
                    </div>
                    <div id="navbarMenu" class="navbar__menu-wr collapse">
                        <div class="container">
                            <div class="navbar__menu">
                                <div class="navbar__menu-logo">
                                    <div class="navbar-logo">
                                        <img src="assets/images/logo_white.svg" alt="webstorm" class="navbar-logo__img">
                                    </div>
                                </div>
                                <div class="navbar__menu-close">
                                    <button data-target="#navbarMenu" aria-expanded="false" data-toggle="collapse"
                                            class="js-navbar-toggle navbar__close"></button>
                                </div>
                            </div>
                            <div class="navbar__nav">
                                <ul class="navbar-nav">
                                    <li
                                            class="navbar-nav__item-wr  ">
                                        <div class="navbar-nav__item">
                                            <a data-page="/"
                                               href="/pages/about/"
                                               class="js-navbar-nav-link navbar-nav__link">О нашей пленке</a>
                                        </div>
                                    </li>

                                    <li
                                            class="navbar-nav__item-wr  ">
                                        <div class="navbar-nav__item">
                                            <a data-page="/"
                                               href="/pages/faq/"
                                               class="js-navbar-nav-link navbar-nav__link">Помощь</a>
                                        </div>
                                    </li>

                                    <li
                                            class="navbar-nav__item-wr  ">
                                        <div class="navbar-nav__item">
                                            <a data-page="/"
                                               href="/pages/projects/"
                                               class="js-navbar-nav-link navbar-nav__link">Проекты</a>
                                        </div>
                                    </li>

                                    <li
                                            class="navbar-nav__item-wr  ">
                                        <div class="navbar-nav__item">
                                            <a data-page="/"
                                               href="<?= Yii::app()->createUrl('/catalog/') ?>"
                                               class="js-navbar-nav-link navbar-nav__link">Каталог</a>
                                        </div>
                                    </li>

                                    <li
                                            class="navbar-nav__item-wr  ">
                                        <div class="navbar-nav__item">
                                            <a data-page="/"
                                               href="/contacts/"
                                               class="js-navbar-nav-link navbar-nav__link">Контакты</a>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="navbar__logo">
                        <div class="navbar-logo">
                            <img src="assets/images/logo.svg" alt="webstorm" class="navbar-logo__img">
                        </div>
                    </div>
                    <a href="tel:<?=$data->attributes['header_phone']?>" class="navbar__call">
                        <span class="navbar__call-icon"></span>
                    </a>
                    <div class="navbar__content">
                        <div class="navbar__content-inner">
                            <div class="navbar__phone-wr center-m">
                                <a href="tel:<?=$data->attributes['header_phone']?>"
                                   class="navbar__phone">
                                    <?=$data->attributes['header_phone']?>
                                </a>
                            </div>
                            <div class="navbar__request-wr center-m">
                                <button data-target="#modalRequest"
                                        class="js-modal-toggle btn _inverse navbar__request">
                                        <span class="btn__text">
                                            Заказать звонок
                                        </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="svg-ghost"></div>
        <header class="header">
            <div class="header__content _main">
                <div class="container">
                    <div class="header__inner">
                        <h1 class="header__title">
                            Пленка —<br>
                            всего лишь<br>
                            поверхность...
                        </h1>
                        <p class="header__desc visible-xs visible-sm">
                            Вы сами создаете историю своего<br>бизнеса,
                            мы просто всегда под рукой.
                        </p>
                        <p class="header__desc hidden-xs hidden-sm">
                            Вы сами создаете историю своего бизнеса,<br>
                            мы просто всегда под рукой.
                        </p>
                    </div>
                </div>
            </div>
            <div class="header__content _fill visible-xs">
                <div class="container">
                    <div class="header__phone-wr center-m">
                        <a href="tel:+78002222245" class="header__phone">8 (800) 222-22-45</a>
                    </div>
                    <div class="header__request-wr center-m">
                        <button data-target="#modalRequest" class="js-modal-toggle btn _inverse header__request">
                        <span class="btn__text">
                        Заказать звонок
                     </span>
                        </button>
                    </div>
                </div>
            </div>
        </header>

        <?= $content; ?>

        <div id="root"></div>
        <div class="page-buffer__end"></div>
    </div>
</div>


<div class="page-footer">


    <div class="footer">
        <div class="container">
            <div class="footer__inner">
                <div class="footer__row _content">
                    <div class="footer__col _request">
                        <div class="footer__block">
                            <div class="footer__block-header">
                                <h5 class="footer__title">Обратная связь</h5>
                                <p class="footer__desc">
                                    Оставьте заявку и мы вам перезвоним
                                </p>
                            </div>

                            <?php
                            $FORM = new FormOrder();
                            ?>

                            <div class="footer__block-content">
                                <div class="footer__form">
                                    <?= CHtml::beginForm('', 'POST', ['class' => 'js-form-footer form']) ?>
                                    <div class="form__group flex-row">

                                        <label class="form-input ">
                                            <?= CHtml::activeTextField($FORM, 'dataName', [
                                                'class' => 'form-input__field',
                                                'placeholder' => 'Ваше имя'
                                            ]); ?>
                                        </label>


                                        <label class="form-input ">
                                            <?= CHtml::activeTextField($FORM, 'dataPhone', [
                                                'class' => 'js-form-phone form-input__field',
                                            ]); ?>
                                        </label>

                                    </div>
                                    <div class="form__group">

                                        <label class="form-input ">
                                            <?= CHtml::activeTextField($FORM, 'dataEmail', [
                                                'class' => 'form-input__field',
                                                'placeholder' => 'E-mail'
                                            ]); ?>
                                        </label>

                                    </div>
                                    <div class="form__group">
                                        <label class="form-input _textarea ">
                                            <?= CHtml::activeTextArea($FORM, 'dataMessage', [
                                                'class' => 'form-input__field',
                                                'placeholder' => 'Сообщение'
                                            ]); ?>
                                        </label>
                                    </div>
                                    <?= CHTML::activeHiddenField($FORM, 'dataFormName', [
                                        'value' => 'Обратная связь'
                                    ]); ?>
                                    <div class="form__submit-wr">
                                        <button type="submit" class="btn _inverse form__submit">
                                            <span class="btn__text">Отправить</span>
                                        </button>
                                        <? /*CHtml::ajaxSubmitButton('Заказать', '', [
                                            'type' => 'POST',
                                            'success' =>
                                                'js:$("#modalSuccess").fadeIn(500);',
                                        ], [
                                            'type' => 'submit',
                                            'class' => 'form__submit btn _inverse'
                                        ]);*/ ?>
                                    </div>
                                    <?= CHtml::endForm() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer__col _contacts">
                        <div class="footer__block">
                            <div class="footer__block-header">
                                <h5 class="footer__title">
                                    Наши контакты
                                </h5>
                                <p class="footer__desc">
                                    Мы будем рады видеть вас по адресу
                                </p>
                            </div>
                            <div class="footer__block-content">
                                <div class="footer__address">
                                    <div class="footer-address">
                                        <?php
                                        $dataAdress = json_decode($data->adress);

                                        if (!empty($dataAdress))
                                            foreach ($dataAdress->city as $cityKey => $city):
                                                if(empty($city)) continue;
                                            ?>
                                            <div class="footer-address__item">
                                                <span class="footer-address__title">
                                                  <span class="footer-address__title-text">
                                                     <?=$city?>
                                                  </span>
                                                </span>
                                                <span class="footer-address__text"><?=$dataAdress->adress[$cityKey]?></span>
                                            </div>
                                        <?endforeach;?>
                                        <!--
                                        <div class="footer-address__item">
                                           <span class="footer-address__title">
                                              <span class="footer-address__title-text">
                                                 Москва
                                              </span>
                                           </span>
                                            <span class="footer-address__text">
                                              пр. Серебрякова, д. 2, стр. 1
                                           </span>
                                        </div>
                                        <div class="footer-address__item">
                                            <span class="footer-address__title">
                                              <span class="footer-address__title-text">
                                                 Новосибирск
                                              </span>
                                           </span>
                                            <span class="footer-address__text">
                                              ул. Октябрьская, д. 34, цок. этаж
                                           </span>
                                        </div>
                                        -->
                                    </div>
                                </div>
                                <div class="footer__contacts">
                                    <div class="footer-contacts">
                                        <div class="footer-contacts__col">
                                            <div class="footer-contacts__phones">

                                                <?php
                                                $dataPhone = json_decode($data->attributes['phones']);
                                                if (is_array($dataPhone) == true)
                                                    foreach ($dataPhone as $phones):
                                                        if(empty($phones)) continue;
                                                        ?>
                                                        <div class="footer-contacts__phones-item">
                                                            <a href="tel:<?=$phones?>" class="footer-contacts__phone">
                                                                <!--8 (495) 740-65-22-->
                                                                <?=$phones?>
                                                            </a>
                                                        </div>
                                                    <? endforeach; ?>
                                            </div>
                                            <div class="footer-contacts__help-wr">
                                              <span class="footer-contacts__help">
                                                 <span class="footer-contacts__help-text">
                                                    Бесплатно по РФ
                                                 </span>
                                              </span>
                                            </div>
                                        </div>
                                        <div class="footer-contacts__divider"></div>
                                        <div class="footer-contacts__col">
                                            <div class="footer-contacts__email-wr">
                                                <a href="mailto:<?= $data->attributes['mail'] ?>"
                                                   class="footer-contacts__email"><?=$data->attributes['mail']?></a>
                                            </div>
                                            <div class="footer-contacts__socials-wr">
                                                <ul class="footer-contacts__socials">
                                                    <li class="footer-contacts__socials-item">
                                                        <a href="<?= $data->attributes['vk'] ?>"
                                                           class="footer-contacts__social _vk">
                                                            <span class="footer-contacts__social-icon"></span>
                                                        </a>
                                                    </li>
                                                    <li class="footer-contacts__socials-item">
                                                        <a href="<?= $data->attributes['inst'] ?>"
                                                           class="footer-contacts__social _inst">
                                                            <span class="footer-contacts__social-icon"></span>
                                                        </a>
                                                    </li>
                                                    <li class="footer-contacts__socials-item">
                                                        <a href="<?= $data->attributes['facebook'] ?>"
                                                           class="footer-contacts__social _fb">
                                                            <span class="footer-contacts__social-icon"></span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer__download-wr">
                                    <a href="#" class="footer__download">
                                        <span class="footer__download-icon"></span>
                                        <span class="footer__download-text">
                           Скачать презентацию
                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer__separate"></div>
                <div class="footer__row _info">
                    <div class="footer__copyright">
                        &#169; <?= date("Y"); ?> Wolly. Все права защищены.
                    </div>
                    <div class="footer__design mt-20 mt-0-sm g-va">
                        <span class="footer__design-text">Разработка сайта</span>
                        <span class="footer__design-separate">&#8212;</span>
                        <a href="//constructivesolutions.ru" target="_blank" class="footer__design-link">Конструктивные
                            решения</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>


<div class="js-page-anchor page-anchor"></div>


<div class="page-helper"></div>

<div class="hidden">
    <div id="modalRequest" class="modal ">
        <div class="modal__inner">
            <div class="modal__header">

                <h3 class="modal__title">Бесплатные образцы</h3>

            </div>
            <div class="modal__content">

                <p class="modal__desc">
                    в Москве и Санкт-Петербурге
                </p>
                <div class="modal__form">
                    <form class="js-request-form form _modal">
                        <div class="form__group flex-row">

                            <label class="form-input ">
                                <input class=" form-input__field" placeholder="Ваше имя" type="text"  name="name" placeholder="Ваше имя">
                            </label>


                            <label class="form-input ">
                                <input class="js-form-phone form-input__field" placeholder="" type="text"  name="phone" placeholder="">
                            </label>

                        </div>
                        <div class="form__group">

                            <label class="form-input ">
                                <input class=" form-input__field" placeholder="Адрес доставки" type="text"  name="address" placeholder="Адрес доставки">
                            </label>

                        </div>
                        <div class="form__submit-wr">
                            <button type="submit" class="form__submit btn _inverse">
                                <span class="btn__text">Заказать</span>
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="hidden">
    <div id="modalSuccess" class="modal ">
        <div class="modal__inner">
            <div class="modal__header">
                <h3 class="modal__title">Спасибо</h3>
            </div>
            <div class="modal__content">
                <p class="modal__desc">
                    В ближайшее время мы с вами свяжемся.
                </p>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/main.js"></script>
</body>
</html>
