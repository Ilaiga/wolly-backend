<html lang=ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/bundle.css">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <title><?= CHtml::encode($this->pageTitle); ?></title>
    <script id="ISDEKscript" type="text/javascript" src="https://www.cdek.ru/website/edostavka/template/js/widjet.js"></script>

</head>
<body>
<main class="l-wrapper">
    <div class="l-wrapper-inner">

        <header class="header">
            <div class="container">
                <div class="header__wrap">
                    <button type="button" class="btn_menu-open">
                        <span></span>
                    </button>
                    <div class="header__logo">
                        <a href="/">
                            <svg class="icon icon-logo" width="1em" height="1em">
                                <use xlink:href="<?= Yii::app()->request->baseUrl; ?>/assets/images/sprite1.svg#cicon-logo"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="header__main" style = "flex-grow: unset; width: auto; margin-left: auto;">
                        <div class="header__main-rt">
                            <div class="header__callback">
                                <div class="header__callback-num">
                                    <a href="tel:88002222245">8 (800) 222-22-45</a>
                                </div>
                                <div class="header__callback-btn">
                                    <a href="#" class="btn btn_sm btn_white-bord btn_fw800 btn_cback js-modal-toggle">
                                        Заказать звонок
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <div class="mobile_menu">
            <button type="button" class="btn_menu-close">
                <span></span>
            </button>
            <div class="mobile_menu__nav"></div>
            <div class="mobile_menu__callback"></div>
        </div>


        <div class="c-main-title">
            <div class="container">
                <div class="c-breadcrumbs ">
                    <?php if(isset($this->breadcrumbs)):?>
                        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                            'links'=>$this->breadcrumbs,
                            'htmlOptions' => array(
                                'class'=>'h-reset-list breadcrumbs__list',
                            ),
                            'activeLinkTemplate'=>'<li><a href="{url}">{label}</a></li>',
                            'inactiveLinkTemplate'=>'<li><span>{label}</span></li>',
                            'tagName'=>'ul',
                            'separator' => '',

                        )); ?><!-- breadcrumbs -->
                    <?php endif?>


                </div>

                <div class="main-title__heading">
                    <h1 class = "h1"><?=CHtml::encode($this->pageTitle);?></h1>
                    <!-- <h1 class="h1">Каталог товаров</h1> -->
                </div>
            </div>
        </div>

        <?= $content ?>
    </div>

    <div class="footer">
        <div class="container">
            <div class="footer__privacy">
                <div class="footer__privacy-rights">
                    <p class="footer__privacy-text">&copy; <?= date("Y"); ?> Wolly. Все права защищены.</p>
                </div>
            </div>

        </div>
    </div>
</main>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/bundle.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/common.js"></script>
</body>
</html>