<?php /* @var $this Controller */ ?>
<?php
$data = (new Settings())->find();
?>
<!doctype html>
<html lang=ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="<?= Yii::app()->request->baseUrl; ?>/css/bundle.css">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon-16x16.png">
    <title>
        <?= isset($this->seo_title) && !empty($this->seo_title) ? CHtml::encode($this->seo_title) : CHtml::encode($this->pageTitle); ?>
    </title>
</head>
<body>
<main class="l-wrapper">
    <div class="l-wrapper-inner">
        <header class="header">
            <div class="container">
                <div class="header__wrap">
                    <button type="button" class="btn_menu-open">
                        <span></span>

                    </button>
                    <div class="header__logo">
                        <a href="/">
                            <svg class="icon icon-logo" width="1em" height="1em">
                                <use xlink:href="<?= Yii::app()->baseUrl ?>/assets/images/sprite1.svg#cicon-logo"></use>
                            </svg>
                        </a>
                    </div>
                    <div class="header__main">
                        <nav class="header__nav">
                            <?php $this->widget('zii.widgets.CMenu', array(
                                'items' => array(

                                    array(
                                        'label' => 'Каталог',
                                        'url' => array('/catalog/')
                                    ),
                                    array(
                                        'label' => 'Проекты',
                                        'url' => array('/projects/')
                                    ),
                                    array(
                                        'label' => 'О нас',
                                        'url' => array('/pages/about/')
                                    ),
                                    array(
                                        'label' => 'Помощь',
                                        'url' => array('pages/faq/')
                                    ),
                                    array(
                                        'label' => 'Контакты',
                                        'url' => array('/contacts/')
                                    ),
                                    array(
                                        'label' => 'Доставка и оплата',
                                        'url' => array('/pages/delivery/')
                                    ),
                                   
                                ),
                                'htmlOptions' => array(
                                    'class' => 'h-reset-list header__nav-list'
                                ),
                            )); ?>
                        </nav>
                        <div class="header__main-rt">
                            <div class="header__callback">
                                <div class="header__callback-num">
                                    <a href="tel:88002222245">8 (800) 222-22-45</a>
                                </div>
                                <div class="header__callback-btn">
                                    <a href="#"
                                       data-target="#modalRequest"
                                       class="btn btn_sm btn_white-bord btn_fw800 btn_cback js-modal-toggle">
                                        Заказать звонок
                                    </a>
                                </div>
                            </div>
                            <a href="<?= Yii::app()->createUrl('cart/index') ?>" class="header__basket">
                                <div class="header__basket-icon">
                                    <svg class="icon icon-basket" width="1em" height="1em">
                                        <use xlink:href="<?= Yii::app()->baseUrl ?>/assets/images/sprite1.svg#cicon-basket"></use>
                                    </svg>
                                </div>
                                <div class="header__basket-value"><span
                                            id="update_basket"><?= Yii::app()->shoppingCart->getCost() ?></span> <span>&#8381;</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>


        <div class="mobile_menu">
            <button type="button" class="btn_menu-close">
                <span></span>
            </button>
            <div class="mobile_menu__nav"></div>
            <div class="mobile_menu__callback"></div>
        </div>

        <?= $content ?>

    </div>


    <div class="footer">
        <div class="container">
            <div class="footer__wrap">
                <div class="footer__top">
                    <div class="footer__top-col footer__top-address">
                        <?php
                            $dataAdress = json_decode($data->adress);
                            if (!empty($dataAdress))
                                foreach ($dataAdress->city as $cityKey => $city):
                                    if(empty($city)) continue;
                        ?>
                                    <div class="footer__top-address-item">
                                        <div class="footer__top-address-item-city  footer__top-address-item-city--moscow">
                                            <span class="word-flag"><?= $city ?></span>
                                        </div>
                                        <div class="footer__top-address-item-descr"><?= $dataAdress->adress[$cityKey] ?></div>
                                    </div>
                        <?endforeach;?>



                    </div>
                    <div class="footer__top-col footer__top-numbers">
                        <?php
                        $dataPhone = json_decode($data->attributes['phones']);
                        if (is_array($dataPhone) == true)
                        foreach ($dataPhone as $phones):
                        if(empty($phones)) continue;
                        ?>
                            <a href="tel:<?=$phones?>" class="footer__top-numbers-item"><?=$phones?></a>
                        <?endforeach;?>
                        <div class="footer__top-numbers-free">
                            <span class="word-flag">бесплатно по РФ</span>
                        </div>
                    </div>
                    <div class="footer__top-col footer__top-socials">
                        <a href="mailto:<?= $data->attributes['mail'] ?>" class="footer__top-socials-mail"><?= $data->attributes['mail'] ?></a>
                        <div class="footer__top-socials-items">
                            <a href="<?= $data->attributes['vk'] ?>" class="footer__top-socials-item">
                                <svg class="icon icon-vk" width="1em" height="1em">
                                    <use xlink:href="<?= Yii::app()->baseUrl ?>/assets/images/sprite1.svg#cicon-vk"></use>
                                </svg>
                            </a>
                            <a href="<?= $data->attributes['inst'] ?>" class="footer__top-socials-item">
                                <svg class="icon icon-instagram" width="1em" height="1em">
                                    <use xlink:href="<?= Yii::app()->baseUrl ?>/assets/images/sprite1.svg#cicon-instagram"></use>
                                </svg>
                            </a>
                            <a href="<?= $data->attributes['facebook'] ?>" class="footer__top-socials-item">
                                <svg class="icon icon-facebook" width="1em" height="1em">
                                    <use xlink:href="<?= Yii::app()->baseUrl ?>/assets/images/sprite1.svg#cicon-facebook"></use>
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="footer__bot-col footer__bot-download">
                        <a href="#" class="footer__bot-download-btn">
                            <svg class="icon icon-document" width="1em" height="1em">
                                <use xlink:href="<?= Yii::app()->baseUrl ?>/assets/images/sprite1.svg#cicon-document"></use>
                            </svg>
                            <span>Скачать презентацию</span>
                        </a>
                    </div>
                    <!--<div class="footer__top-col footer__top-about">ОБЩЕСТВО С ОГРАНИЧЕННОЙ<br>ОТВЕТСТВЕННОСТЬЮ «Wolly»<br>присвоен ИНН 9729215515, КПП 772901001,<br>ОГРН 5177746394601</div>!-->
                </div>
                <div class="footer__privacy">
                    <div class="footer__privacy-rights">
                        <p class="footer__privacy-text">&copy; <?= date("Y"); ?> Wolly. Все права защищены.</p>
                        <div class="footer__bot-col footer__bot-policy">Используя сайт, вы принимаете условия <a
                                    href="#">Публичной оферты</a>,<br><a href="#">Соглашения на обработку персональных
                                данных</a> и <a href="#">Политики в отношении персональных данных</a></div>
                    </div>
                    <div class="footer__privacy-development">
                        <div class="footer__bot-col footer__bot-payment">
                            <div class="footer__bot-payment-item">
                                <img src="<?= Yii::app()->baseUrl ?>/assets/images/footer/visa.png" alt="">
                            </div>
                            <div class="footer__bot-payment-item">
                                <img src="<?= Yii::app()->baseUrl ?>/assets/images/footer/mastercard.png" alt="">
                            </div>
                        </div>
                        <div class="footer__privacy-development-descr">Дизайн и разработка сайта</div>
                        <div class="footer__privacy-development-company">
                            <a href="#" class="footer__privacy-development-company-item"><img
                                        src="<?= Yii::app()->baseUrl ?>/assets/images/footer/company.png" alt=""></a>
                            <a href="#" class="footer__privacy-development-company-item"><img
                                        src="<?= Yii::app()->baseUrl ?>/assets/images/footer/company-sm.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</main>

<div class="hidden">
    <div id="modalRequest" class="modal ">
        <div class="modal__inner">
            <div class="modal__header">

                <h3 class="modal__title">Бесплатные образцы</h3>

            </div>
            <div class="modal__content">

                <p class="modal__desc">
                    в Москве и Санкт-Петербурге
                </p>
                <div class="modal__form">
                    <form class="js-request-form form _modal">
                        <div class="form__group flex-row">

                            <label class="form-input ">
                                <input class=" form-input__field" placeholder="Ваше имя" type="text" name="name"
                                       placeholder="Ваше имя">
                            </label>


                            <label class="form-input ">
                                <input class="js-form-phone form-input__field" placeholder="" type="text" name="phone"
                                       placeholder="">
                            </label>

                        </div>
                        <div class="form__group">

                            <label class="form-input ">
                                <input class=" form-input__field" placeholder="Адрес доставки" type="text"
                                       name="address" placeholder="Адрес доставки">
                            </label>

                        </div>
                        <div class="form__submit-wr">
                            <button type="submit" class="form__submit btn _inverse">
                                <span class="btn__text">Заказать</span>
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="hidden">
    <div id="modalSuccess" class="modal ">
        <div class="modal__inner">
            <div class="modal__header">
                <h3 class="modal__title">Спасибо</h3>
            </div>
            <div class="modal__content">
                <p class="modal__desc">
                    В ближайшее время мы с вами свяжемся.
                </p>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/bundle.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/common.js"></script>

</body>
</html>
