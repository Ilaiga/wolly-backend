<?php

if(!empty($product->seo_description))
    Yii::app()->clientScript->registerMetaTag($product->seo_description, 'description');
if(!empty($product->seo_title))
    $this->seo_title = $product->seo_title;


$this->pageTitle = $product->title; //$product->attribute->title;
$this->breadcrumbs =
    array(
        'Каталог товаров' => array('catalog/index'),
        $product->title,
    );
    $array_sale = array(
        '1' => array(
            'Скидка' => 'atlantis',
        ),
        '2' => array(
            'Хит' => 'brinkpink',
        ),
        '3' => array(
            'Новинка' => 'turquoiseblue',
        )
    );
?>

<div class="c-kt">
    <div class="kt__banner">
        <div class="kt__banner-ttl_xs">
            <div class="container"></div>
        </div>
        <div class="kt__banner-bg">
            <div class="kt__banner-bg-inner js-kt-banner-slider">
                <?php foreach ($product->productsImages as $image): ?>
                    <div class="kt__banner-bg-item">
                        <div class="h-object-fit">
                            <img src="<?= $image['image_url'] ?>" alt="">
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
        <div class="kt__banner-wrap">
            <div class="container">
                <div class="kt__banner-breadcrumbs">
                    <div class="c-breadcrumbs c-breadcrumbs_blue">
                        <?php if (isset($this->breadcrumbs)): ?>
                            <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                                'links' => $this->breadcrumbs,
                                'htmlOptions' => array(
                                    'class' => 'h-reset-list breadcrumbs__list',
                                ),
                                'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
                                'inactiveLinkTemplate' => '<li><span>{label}</span></li>',
                                'tagName' => 'ul',
                                'separator' => '',
                            )); ?>
                        <?php endif ?>
                    </div>
                </div>
                <div class="kt__banner-inner">
                    <div class="kt__banner-lt">
                        <div class="kt__banner-ttl">
                            <h1 class="h1"><?= $product->title ?></h1>
                        </div>
                        <div class="js-kt-carousel-banner kt-carousel-banner kt__banner-images">
                            <?php foreach ($product->productsImages as $image): ?>
                                <div class="kt__banner-bg-item">
                                    <div class="h-object-fit">
                                        <img src="<?= $image['image_url'] ?>" alt="">
                                    </div>
                                </div>
                                <div class="kt-carousel-banner__slide">
                                    <div class="kt-carousel-banner__slide-inner">
                                        <a href="<?= $image['image_url'] ?>" class="kt__banner-images-item"
                                           data-fancybox="gallery-1">
                                            <div class="h-object-fit">
                                                <img src="<?= $image['image_url'] ?>" alt="">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                    <div class="kt__banner-rt">
                        <div class="kt__banner-choise">
                            <div class="kt__banner-choise-ttl">Сколько пленки вам нужно?</div>
                            <a href="#" class="kt__banner-choise-help">Помогите мне посчитать</a>
                            <div class="kt__banner-choise-main">
                                <?
                                if (!empty($product->productsParams)): foreach ($product->productsParams as $param):
                                    $product->param_id = $param['id'];
                                    $product->param_name = $param['params_name'];
                                    $positions = Yii::app()->shoppingCart->itemAt($product->getId());
                                    ?>
                                    <div class="kt__banner-choise-main-item"
                                         data-product="<?= $product->id ?>"
                                         data-param_name="<?= $product->param_name ?>"
                                         data-param_cost="<?= $param['params_cost'] ?>"
                                    >
                                        <div class="kt__banner-choise-main-item-lt">
                                            <div class="kt__banner-choise-main-item-name"><?= $param['params_name'] ?></div>
                                            <div class="kt__banner-choise-main-item-counter">
                                                <div class="counter-wrap">
                                                    <div class="counter-minus"></div>
                                                    <div class="counter-number-wrap">
                                                        <input type="number"
                                                               value="<?= !empty($positions) ? $positions->getQuantity() : 0 ?>"
                                                               class="counter-number">
                                                    </div>
                                                    <div class="counter-plus"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt__banner-choise-main-item-price">
                                            <div class="basket_price"><span
                                                        class="basket_price-singleVal"><?= !empty($positions) ? $positions->getSumPrice() : 0 ?></span>
                                                <span>&#8381;</span></div>
                                        </div>
                                    </div>
                                <? endforeach;
                                else:
                                    $positions = Yii::app()->shoppingCart->itemAt($product->getId());
                                    ?>
                                    <div class="kt__banner-choise-main-item"
                                         data-product="<?= $product->id ?>"
                                         data-param_name=""
                                         data-param_cost="<?= $product->getPrice() ?>"
                                    >
                                        <div class="kt__banner-choise-main-item-lt">
                                            <div class="kt__banner-choise-main-item-name"><?= $product->title ?></div>
                                            <div class="kt__banner-choise-main-item-counter">
                                                <div class="counter-wrap">
                                                    <div class="counter-minus"></div>
                                                    <div class="counter-number-wrap">
                                                        <input type="number"
                                                               value="<?= !empty($positions) ? $positions->getQuantity() : 0 ?>"
                                                               class="counter-number">
                                                    </div>
                                                    <div class="counter-plus"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt__banner-choise-main-item-price">
                                            <div class="basket_price"><span
                                                        class="basket_price-singleVal"><?= !empty($positions) ? $positions->getSumPrice() : 0 ?></span>
                                                <span>&#8381;</span></div>
                                        </div>
                                    </div>
                                <? endif; ?>
                            </div>
                            <div class="basket__table-total-main-top kt__banner-choise-total">
                                <div class="basket__table-total-main-top-descr">Итого</div>
                                <div class="basket__table-total-main-top-price">
                                    <div class="basket_price">1500 <span>&#8381;</span></div>
                                </div>
                            </div>
                            <div class="kt__banner-choise-delivery">
                                <a href="<?= Yii::app()->createUrl('delivery') ?>" target="_blank"
                                   class="kt__banner-choise-delivery-link">Доставка и оплата</a>
                            </div>
                            <div class="kt__banner-choise-btn">
                                <a href="#" class="btn btn_blue btn_bold ajax AddSingleProduct">Добавить в корзину</a>
                            </div>
                        </div>
                        <?= CHtml::beginForm('', 'POST', ['class' => 'kt__banner-calculator minimized']) ?>
                        <div class="kt__banner-calculator-top">
                            <div class="kt__banner-choise-ttl">Калькулятор расчета колличества пленки</div>
                            <button type="button" class="kt__banner-calculator-top-close">
                                <span>Закрыть</span>
                                <span class="kt__banner-calculator-top-close-icon">
                                        <svg class="icon icon-delete" width="1em" height="1em">
                                            <use xlink:href="/assets/images/sprite1.svg#cicon-delete"></use>
                                        </svg>
                                    </span>
                            </button>
                        </div>
                        <div class="kt__banner-calculator-info">
                            <p>Если вы сомневаетесь, отправьте свои данные и наш менеджер свяжется с
                                вами</p>
                            <div class="kt__banner-calculator-info-sizes">

                                <div class="form-group">
                                    <div class="input-wrap">
                                        <label class="input-label ">
                                            <span class="input-title ">Ширина</span>
                                            <?=CHtml::activeNumberField($form, 'dataWidth', ['class' => 'form-control_h42 form-control']);?>
                                        </label>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="input-wrap">
                                        <label class="input-label ">
                                            <span class="input-title ">Длина</span>
                                            <?=CHtml::activeNumberField($form, 'dataHeight', ['class' => 'form-control_h42 form-control'])?>
                                        </label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="kt__banner-calculator-inputs">

                            <div class="form-group">
                                <div class="input-wrap">
                                    <label class="input-label ">
                                        <span class="input-title ">Имя</span>
                                        <?=CHtml::activeTextField($form, 'dataName', ['class' => 'form-control', 'placeholder' => 'Имя'])?>
                                    </label>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="input-wrap">
                                    <label class="input-label ">
                                        <span class="input-title  input-title_required ">Телефон</span>
                                        <?=CHtml::activeTelField($form, 'dataPhone', ['class' => 'form-control', 'placeholder' => '+7 (000) 000-00-00'])?>
                                    </label>
                                </div>
                            </div>
                            <?=CHtml::activeHiddenField($form, 'dataFormName', ['value' => 'Калькулятор расчета колличества пленки'])?>

                        </div>
                        <ul class="h-reset-list delivery__main-item-notes">
                            <li>Поля, обязательные для заполнения</li>
                        </ul>
                        <div class="kt__banner-calculator-btn">
                            <?= CHtml::ajaxSubmitButton('Отправить', Yii::app()->createUrl('/catalog/Calculate'), [
                                'type' => 'POST',
                            ], ['class' => 'btn btn_blue btn_bold']) ?>
                        </div>
                        <?= CHtml::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="kt__descr">
        <div class="container">
            <div class="kt__descr-wrap">
                <div class="kt__descr-ttl">
                    <h2 class="h2">Описание</h2>
                </div>
                <div class="kt__descr-main">
                    <div class="kt__descr-txt">
                        <p>
                            <?= !empty($product->description) ? $product->description : 'Описание не заполнено' ?>
                            <a href="#" class="kt__descr-txt-readmore">Читать дальше</a>
                        </p>
                    </div>
                    <div class="kt__descr-links">

                        <?if(!empty($product->productsDocs)) foreach ($product->productsDocs as $doc): ?>
                            <div class="kt__descr-links-item">
                                <a target="_blank" href="<?= Yii::app()->baseUrl ?>/uploads/attach_ProductFile/<?=$doc->doc_url?>" class="document-link">
                                    <div class="document-link-icon">
                                        <svg class="icon icon-document" width="1em" height="1em">
                                            <use xlink:href="<?= Yii::app()->baseUrl ?>/assets/images/sprite1.svg#cicon-document"></use>
                                        </svg>
                                    </div>
                                    <span class="document-link-name"><?=$doc->doc_type?></span>
                                </a>
                            </div>
                        <?endforeach;?>

                    </div>
                </div>
            </div>
        </div>
    </div>




    <? if (!empty($product->productsForgets)): ?>
        <div class="c-buy">
            <div class="container">
                <div class="buy__ttl">
                    <h2 class="h2">Не забудьте купить</h2>
                </div>
                <div class="js-carousel-buy buy__items carousel-buy">
                    <? foreach ($product->productsForgets as $forget):
                        $data = $product->getForgetProduct($forget->product_id);
                        if(empty($data)) continue;
                        ?>
                        <div class="carousel-buy__slide">

                            <div class="buy__item">
                                <div class="h-object-fit buy__item-img">
                                    <?
                                    $active = false;
                                    foreach ($data->productsImagesPreview as $preview_image):?>
                                        <img src="<?= $preview_image['image_url'] ?>"
                                             class="<?= !$active ? ('active') : ('') ?>"
                                             data-params_name="<?= $preview_image['params_name'] ?>" alt="">
                                        <?
                                        $active = true;
                                    endforeach;
                                    ?>
                                </div>
                                <div class="buy__item-main">
                                    <? if ($data->sale) foreach ($array_sale[$data->sale] as $arraySale_key => $arraySale_item): ?>
                                        <span class="catalog-item__flag catalog-item__flag_<?= $arraySale_item ?>"><?= $arraySale_key ?></span>
                                    <? endforeach; ?>
                                    <div class="catalog-item__ttl buy__item-main-ttl">
                                        <div class="catalog-item__ttl-top"><?= $data->undertitle ?></div>
                                        <div class="catalog-item__ttl-bot"><?= $data->title ?></div>
                                    </div>
                                    <? if (!empty($data->productsParams)): ?>
                                        <div class="catalog-item__radiogroup  catalog-item__radiogroup--dontforget">
                                            <? $active = false;
                                            foreach ($data->productsParams as $data_params):

                                                ?>
                                                <div class="form-group"
                                                >
                                                    <label class="radio-label_catalog <?= !$active ? ('active') : ('') ?>"
                                                           data-params_name="<?= $data_params['params_name'] ?>"
                                                           data-product_id="<?= $data->id ?>"
                                                           data-params_cost="<?= $data_params['params_cost'] ?>"
                                                    >
                                                        <input type="radio" name="size"
                                                               value="<?= $data_params['params_name'] ?>"
                                                               class="hidden-input">
                                                        <span class="check-value"><?= $data_params['params_name'] ?></span>
                                                    </label>
                                                </div>
                                                <?
                                                $active = true;
                                            endforeach;
                                            ?>
                                        </div>
                                    <? else: ?>
                                        <div class="catalog-item__radiogroup  catalog-item__radiogroup--dontforget">
                                            <div class="form-group">
                                                <label class="radio-label_catalog active"
                                                       data-params_name=""
                                                       data-product_id="<?= $data->id ?>"
                                                       data-params_cost="<?= $data->getPrice() ?>
                                                "></label>
                                            </div>
                                        </div>
                                    <? endif; ?>
                                    <div class="buy__item-main-counter">
                                        <div class="buy__item-main-counter-wrap">
                                            <div class="counter-wrap">
                                                <div class="counter-minus"></div>
                                                <div class="counter-number-wrap">
                                                    <input type="number" value="1" class="counter-number">
                                                </div>
                                                <div class="counter-plus"></div>
                                            </div>
                                        </div>
                                        <div class="buy__item-main-counter-price">
                                            <? if (!empty($data->productsParams)) {
                                                $paramCount = false;
                                                foreach ($data->productsParams as $param): ?>
                                                    <div class="basket_price <?= !$paramCount ? ('active') : ('') ?>"
                                                         data-params_name="<?= $param['params_name'] ?>"
                                                    >
                                                        <?= $param['params_cost'] ?>
                                                        <span>&#8381;</span></div>
                                                    <?
                                                    $paramCount = true;
                                                endforeach;
                                            } else {
                                                ?>
                                                <div class="basket_price active"><?= $product->price ?>
                                                    <span>&#8381;</span></div>
                                            <? } ?>
                                        </div>
                                    </div>
                                    <div class="buy__item-main-btn ajaxButton cBuy">
                                        <a href="#" class="btn btn_blue btn_bold">Добавить</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    <? endif ?>
</div>

