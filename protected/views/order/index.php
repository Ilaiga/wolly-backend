<?php

$this->pageTitle = 'Оформление заказа';
$this->breadcrumbs = array(
    'Оформление заказа',
);

?>

<div class="c-order">
    <div class="container">
        <?=CHtml::beginForm('','POST',array('class' => 'order__wrap', 'enctype' => 'multipart/form-data')); ?>

            <div class="order__data">
                <div class="order__data-item">
                    <div class="order__data-item-ttl">
                        <h2 class="h2">Личные данные</h2>
                    </div>
                    <?=CHtml::errorSummary($form)?>
                    <div class="order__data-item-main">
                        <div class="order__data-item-inputs">

                            <div class="form-group">
                                <div class="input-wrap">
                                    <label class="input-label ">
                                        <span class="input-title  input-title_required ">ФИО</span>
                                        <?= CHtml::activeTextField($form, 'fio',
                                            array(
                                                    'class' => 'form-control',
                                                    'placeholder' => 'ФИО'
                                            )
                                        ); ?>
                                    </label>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="input-wrap">
                                    <label class="input-label ">
                                        <span class="input-title  input-title_required ">Телефон</span>
                                        <?= CHtml::activeTelField($form, 'phone',
                                            array(
                                                'class' => 'form-control',
                                                'placeholder' => '+7 (000) 000-00-00',
                                                'type' => 'tel'
                                            )
                                        ); ?>
                                    </label>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="input-wrap">
                                    <label class="input-label ">
                                        <span class="input-title  input-title_required ">E-mail</span>
                                        <?= CHtml::activeEmailField($form, 'email',
                                            array(
                                                'class' => 'form-control',
                                                'placeholder' => 'wolly@mail.ru',
                                            )
                                        ); ?>
                                    </label>
                                </div>
                            </div>

                        </div>
                        <ul class="h-reset-list delivery__main-item-notes">
                            <li>Поля, обязательные для заполнения</li>
                        </ul>

                    </div>
                </div>
                <div class="order__data-item order__data-item_delivery">
                    <div class="order__data-item-ttl">
                        <h2 class="h2">Доставка</h2>
                    </div>
                    <div class="order__data-item-main">
                        <div class="order__data-item-radiobtns">

                            <label class="radio-label ">
                                <?=CHtml::activeRadioButton($form, 'delivery',
                                    array(
                                        'class' => 'hidden-input',
                                        'checked' => 'checked',
                                        'data-val' => 'delivery'
                                    )
                                )?>
                                <span class="check-icon"></span>
                                <span class="check-value">Доставка</span>
                            </label>

                            <label class="radio-label ">
                                <?=CHtml::activeRadioButton($form, 'delivery',
                                    array(
                                        'class' => 'hidden-input',
                                        'data-val' => 'pickup'
                                    )
                                )?>
                                <span class="check-icon"></span>
                                <span class="check-value">Самовывоз</span>
                                <span class="check-descr">(для Москвы и Московской области)</span>
                            </label>
                        </div>
                    </div>
                    <div class="order__data-item_delivery-info">
                        <div class="order__data-item_delivery-info-default">
                            <div class="order__data-item_delivery-info-inputs">

                                <div class="form-group">
                                    <div class="input-wrap">
                                        <label class="input-label ">
                                            <span class="input-title  input-title_required ">Адрес доставки</span>
                                            <?= CHtml::activeTextField($form, 'adress',
                                                array(
                                                    'class' => 'form-control',
                                                    'placeholder' => 'г. Москва, Московская обл., ул. Лазурная, д. 6, кв 51',
                                                )
                                            ); ?>
                                        </label>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="input-wrap">
                                        <label class="input-label ">
                                            <span class="input-title ">Комментарий</span>

                                            <?= CHtml::activeTextArea($form, 'comment',
                                                array(
                                                    'class' => 'form-control',
                                                    'placeholder' => ''
                                                )
                                            ); ?>


                                        </label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="order__data-item_delivery-info-descr">
                            <p>Покупка от 5000р — <b>бесплатно по РФ</b>; <br> Покупка до 5000р — 300р. по РФ <br>Доставка
                                осуществляется на следующий рабочий день.</p>
                        </div>
                        <div class="order__data-item_delivery-info-pickup minimized">
                            <p>Стоимость — <b>бесплатно</b>. <br>Осуществляется с нашего склада по адресу: <br>г.
                                Москва, метро Ботаничекий сад, проезд Серебрякова, д. 2, стр. 1. <br>График работы:
                                пн-пт 10:00 - 20:00, сб-вс 10:00 - 15:00.</p>
                            <ul class="h-reset-list delivery__main-item-notes">
                                <li>Просьба перед приездом предупредить наших менеджеров, чтобы они смогли подать заявку
                                    на комплектацию.
                                </li>
                            </ul>
                            <div id="forpvz" style="height:600px;"></div>
                        </div>
                    </div>
                </div>
                <div class="order__data-item order__data-item_payment">
                    <div class="order__data-item-ttl">
                        <h2 class="h2">Оплата</h2>
                    </div>
                    <div class="js-order-payment-tabs order__data-item-main">
                        <div class="order__data-item-radiobtns">

                            <label class="radio-label ">
                                <?=CHtml::activeRadioButton($form, 'order_type',
                                    array(
                                        'class' => 'hidden-input js-order-payment-tab',
                                        'data-val' => 'cash',
                                        'checked' => 'checked',
                                        'value' => 1,
                                        'uncheckValue' => null
                                    )
                                )?>
                                <span class="check-icon"></span>
                                <span class="check-value">Наличными</span>
                            </label>
                            <label class="radio-label ">
                                <?=CHtml::activeRadioButton($form, 'order_type',
                                    array(
                                        'class' => 'hidden-input js-order-payment-tab',
                                        'data-val' => 'online',
                                        'value' => 2,
                                        'uncheckValue' => null
                                    )
                                )?>
                                <span class="check-icon"></span>
                                <span class="check-value">Оплата онлайн</span>
                            </label>
                            <label class="radio-label ">
                                <?=CHtml::activeRadioButton($form, 'order_type',
                                    array(
                                        'class' => 'hidden-input js-order-payment-tab',
                                        'data-val' => 'bill',
                                        'value' => 3,
                                        'uncheckValue' => null
                                    )
                                )?>
                                <span class="check-icon"></span>
                                <span class="check-value">Оплата по счету</span>
                                <span class="check-descr">(для юридических лиц)</span>
                            </label>
                        </div>
                        <div class="order__data-item_payment-info">
                            <div class="js-order-payment-content order__data-item_payment-content _show"
                                 data-target="cash">
                                <p>Оплатить покупки можно наличными при получении заказа.</p>
                            </div>
                            <div class="js-order-payment-content order__data-item_payment-content" data-target="online">
                                <p>При выборе онлайн оплаты онлайн менеджер свяжется с Вами для проверки заказа</p>
                            </div>
                            <div class="js-order-payment-content order__data-item_payment-content" data-target="bill">
                                <div class="order__data-item-file mt-0">
                                    <label class="order__data-item-file-label">

                                        <?=CHtml::activeFileField($form, 'order_file', []);?>

                                        <span class="order__data-item-file-btn">

                                    <span class="order__data-item-file-btn-icon">
                                        <svg class="icon icon-clip" width="1em" height="1em">
                                            <use xlink:href="assets/images/sprite1.svg#cicon-clip"></use>
                                        </svg>
                                    </span>
                                    <span class="order__data-item-file-btn-txt">Прикрепить файл с реквизитами организации</span>
                                </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="order__main">
                <div class="order__main-top">
                    <div class="order__main-top-ttl">
                        <h2 class="h2">Ваш заказ</h2>
                    </div>
                    <a href="<?= Yii::app()->createUrl('cart/index') ?>" class="order__main-top-link">редактировать
                        заказ</a>
                </div>


                <div class="order__main-items">

                    <?
                    $positions = Yii::app()->shoppingCart->getPositions();
                    foreach ($positions as $position):
                        $product = $position->getPropertyProduct();
                        ?>
                        <div class="order__main-item">
                            <div class="order__main-item-img h-object-fit">

                                <? if (!empty($product->productsImagesPreview)): ?>
                                    <img src="<?= $product->productsImagesPreview[0]['image_url'] ?>" alt="">
                                <? else: ?>
                                    <img src=assets/images/basket/item-img-1.png alt="">
                                <? endif; ?>

                            </div>
                            <div class="order__main-item-info">
                                <div class="order__main-item-info-top">
                                    <div class="catalog-item__ttl-top"><?= $product->undertitle ?></div>
                                    <div class="catalog-item__ttl-bot"><?= $product->title ?></div>
                                </div>
                                <div class="order__main-item-info-bot">

                                    <div class="order__main-item-info-bot-num"><?= $position->getQuantity() ?> шт</div>
                                    <div class="order__main-item-info-bot-price"><?= $position->getSumPrice() ?> <span>&#8381;</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>


                <div class="order__main-sum">
                    <div class="basket__table-total-main-top  basket__table-total-main-top--order">
                        <div class="basket__table-total-main-top-descr  basket__table-total-main-top-descr--delivery">
                            Доставка
                        </div>
                        <div class="basket__table-total-main-top-price  basket__table-total-main-top-price--delivery">
                                <div class="basket_price  basket_price--delivery">
                                    <?php
                                    echo Yii::app()->shoppingCart->getCost() < 5000 ?
                                        300 . '<span>&#8381;</span>' : ('Бесплатно');
                                    ?>
                                </div>
                        </div>
                    </div>
                    <?if(!empty($promoCode)):?>
                        <div class="basket__table-total-main-top  basket__table-total-main-top--order">
                            <div class="basket__table-total-main-top-descr basket__table-total-main-top-descr--delivery">Промокод:</div>
                            <div class="basket__table-total-main-top-price basket__table-total-main-top-descr--delivery">
                                <div class="basket_price basket_price--delivery"><?='-'. $promoCode?> <span><?=$promoType == 2 ? '%': '&#8381;'?></span></div>
                            </div>
                        </div>
                    <?endif;?>

                    <div class="basket__table-total-main-top  basket__table-total-main-top--order">
                        <div class="basket__table-total-main-top-descr">Итого:</div>
                        <div class="basket__table-total-main-top-price">
                            <div class="basket_price"><?php
                                echo Yii::app()->shoppingCart->getCost() < 5000 ?
                                    Yii::app()->shoppingCart->getCost() + 300 : Yii::app()->shoppingCart->getCost();
                                ?> <span>&#8381;</span></div>
                        </div>
                    </div>
                </div>
                <div class="order__main-btn">
                    <?= CHtml::submitButton('Оформить заказ', array('class' => 'btn btn_blue btn_bold')); ?>
                </div>
            </div>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>
<script type="text/javascript">
    var widjet = new ISDEKWidjet({
        showWarns: true,
        showErrors: true,
        showLogs: true,
        hideMessages: false,
        path: 'https://www.cdek.ru/website/edostavka/template/scripts/',
        servicepath: 'wolly.local:8888/site/widget/service.php', //ссылка на файл service.php на вашем сайте
        templatepath: 'wolly.local:8888/widget/scripts/template.php',
        choose: true,
        popup: true,
        country: 'Россия',
        defaultCity: 'Москва',
        cityFrom: 'Москва',
        link: null,
        hidedress: true,
        hidecash: true,
        hidedelt: false,
        onReady: onReady,
        onChoose: onChoose,
        onChooseProfile: onChooseProfile,
        onCalculate: onCalculate
    });

    function onReady() {
        alert('Виджет загружен');
    }

    function onChoose(wat) {
        alert(
            'Выбран пункт выдачи заказа ' + wat.id + "\n" +
            'цена ' + wat.price + "\n" +
            'срок ' + wat.term + " дн.\n" +
            'город ' + wat.cityName + ', код города ' + wat.city
        );
    }

    function onChooseProfile(wat) {
        alert(
            'Выбрана доставка курьером в город ' + wat.cityName + ', код города ' + wat.city + "\n" +
            'цена ' + wat.price + "\n" +
            'срок ' + wat.term + ' дн.'
        );
    }

    function onCalculate(wat) {
        alert('Расчет стоимости доставки произведен');
    }
</script>
