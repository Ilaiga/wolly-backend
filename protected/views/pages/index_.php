<?php

    $this->pageTitle = $page->title;
    $this->breadcrumbs = [
        $page->title
    ];
    if(!empty($page->seo_description))
        Yii::app()->clientScript->registerMetaTag($page->seo_description, 'description');
?>
<div class="c-contacts">
    <div class="container">
        <?=$page->content?>
    </div>
</div>



