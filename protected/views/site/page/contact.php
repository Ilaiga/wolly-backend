<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle = 'Контакты';
$this->breadcrumbs = array(
	'Контакты',
);
?>
<div class="c-contacts">
    <div class="container">
        <div class="contacts__ttl">
            <h2 class="h2">Где нас найти?</h2>
        </div>
        <div class="js-contacts-offices contacts__offices">
            <div class="contacts__offices-descr">Будем рады видеть Вас в одном из наших офисов:</div>
            <ul class="h-reset-list contacts__offices-items">
                <li class="contacts__offices-item">
                    <a data-lat="55.846012" data-long="37.621173"
                       class="js-contacts-toggle contacts__offices-link active" href="#moscow"><span
                                class="word-flag">Москва</span></a>
                </li>
                <li class="contacts__offices-item">
                    <a data-lat="55.034784" data-long="82.918672"
                       class="js-contacts-toggle contacts__offices-link" href="#novosibirsk"><span
                                class="word-flag">Новосибирск</span></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="contacts__tabs">
        <div class="container">
            <div class="contacts__main">
                <div class="contacts__main-item">
                    <div class="contacts__main-item-icon">
                        <svg class="icon icon-phone" width="1em" height="1em">
                            <use xlink:href="/assets/images/sprite1.svg#cicon-phone"></use>
                        </svg>
                    </div>
                    <div data-contact="moscow" class="js-contacts-target contacts__main-item-descr">
                        <a href="tel:84957406522">8 (495) 740-65-22</a>
                        <br>
                        <a href="tel:88002222245">8 (800) 222-22-45</a>
                    </div>
                    <div data-contact="novosibirsk" class="js-contacts-target contacts__main-item-descr hidden">
                        <a href="tel:84957406522">8 (495) 210-46-90</a>
                        <br>
                        <a href="tel:88002222245">8 (800) 222-22-45</a>
                    </div>
                </div>
                <div class="contacts__main-item">
                    <div class="contacts__main-item-icon">
                        <svg class="icon icon-address" width="1em" height="1em">
                            <use xlink:href="/assets/images/sprite1.svg#cicon-address"></use>
                        </svg>
                    </div>
                    <div data-contact="moscow" class="js-contacts-target contacts__main-item-descr">пр.
                        Серебрякова, д. 2, стр. 1А
                    </div>
                    <div data-contact="novosibirsk" class="js-contacts-target contacts__main-item-descr hidden">
                        ул. Красный проспект, д. 39, оф. 404
                    </div>
                </div>
                <div class="contacts__main-item">
                    <div class="contacts__main-item-icon">
                        <svg class="icon icon-message" width="1em" height="1em">
                            <use xlink:href="/assets/images/sprite1.svg#cicon-message"></use>
                        </svg>
                    </div>
                    <div class="contacts__main-item-descr">
                        <a href="mailto:hello@wolly.ru">hello@wolly.ru</a>
                    </div>


                </div>
            </div>
        </div>

        <div class="contacts__map">
            <div id="contactsMap" class="map"></div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBg1nomn9LTkLBNRJZXGP7Qb6yq6BeViSs"></script>
