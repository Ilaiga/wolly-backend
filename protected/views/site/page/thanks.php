<?php
$this->pageTitle='Спасибо, за оформление заказа';
$this->breadcrumbs=array(
    'Оформление заказа',
);
?>


<div class="c-about">
    <div class="container">
        <h2 class="h2">Успех!</h2>
        <div class="about__cont">
            <p class="fs-21">
                <strong><?=$orderData->fio?></strong>, спасибо за заказ!<br>
                Данные по вашему заказу
            </p>
            <ul>
                <?foreach (json_decode($orderData->product_data) as $data):?>
                    <li>Название: <?=$data[0]?> Количество: <?=$data[1]?> Общая стоимость: <?=$data[2]?></li>
                <?endforeach;?>
            </ul>
            <p class="fs-21"><?=$orderType?></p>
        </div>
    </div>
</div>

