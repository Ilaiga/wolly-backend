<?php
$this->pageTitle = 'Помощь';
$this->breadcrumbs=array(
    'Помощь',
);

?>
<div class="c-faq">
    <div class="container">
        <div class="js-faq-wrap faq__wrap">
            <nav class="js-faq-nav faq__nav">
                <ul class="h-reset-list faq__nav-list">
                    <li>
                        <a href="#faq__help">Видео инструкции</a>
                    </li>
                    <li>
                        <a href="#faq__instruction">Инструкции</a>
                    </li>
                    <li>
                        <a href="#faq__sertificates">Сертификаты</a>
                    </li>
                    <li>
                        <a href="#faq__faq">FAQ</a>
                    </li>
                </ul>
            </nav>
            <div class="faq__main">
                <div id="faq__help" class="faq__main-item faq__main-item_help">
                    <div class="faq__main-item-ttl">
                        <h2 class="h2">Видео инструкции</h2>
                    </div>
                    <div class="faq__main-item-cont">
                        <div class="faq__main-item_help-item">
                            <div class="faq__main-item_help-ttl">Пленка не подвержена износу</div>
                            <div class="faq__main-item-descr">Подробная видео-инструкция о том как наклеивать
                                пленку.
                            </div>
                            <div class="faq__main-item_help-video">
                                <iframe src="https://www.youtube.com/embed/EOnSh3QlpbQ" frameborder="0"
                                        allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="faq__main-item_help-item">
                            <div class="faq__main-item_help-ttl">Пленка не подвержена износу</div>
                            <div class="faq__main-item-descr">Подробная видео-инструкция о том как наклеивать
                                пленку.
                            </div>
                            <div class="faq__main-item_help-video">
                                <iframe src="https://www.youtube.com/embed/EOnSh3QlpbQ" frameborder="0"
                                        allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="faq__instruction"
                     class="faq__main-item faq__main-item_bord faq__main-item_instruction">
                    <div class="faq__main-item-ttl">
                        <h2 class="h2">Инструкции</h2>
                    </div>
                    <div class="faq__main-item-cont">
                        <div class="faq__main-item-descr">Все товары, размещенные на сайте, есть на нашем складе
                            и готовы к
                            отгрузке. <br>Имея два филиала в Москве и Новосибирске мы готовы обеспечить
                            кратчайшие <br>сроки
                            доставки по всей территории России.
                        </div>
                    </div>
                    <div class="faq__main-item_instruction-items">
                        <a href="#" class="document-link">
                            <div class="document-link-icon">
                                <svg class="icon icon-document" width="1em" height="1em">
                                    <use xlink:href="assets/images/sprite1.svg#cicon-document"></use>
                                </svg>
                            </div>
                            <span class="document-link-name">Инструкция по наклейке</span>
                        </a>
                        <a href="#" class="document-link">
                            <div class="document-link-icon">
                                <svg class="icon icon-document" width="1em" height="1em">
                                    <use xlink:href="assets/images/sprite1.svg#cicon-document"></use>
                                </svg>
                            </div>
                            <span class="document-link-name">Инструкция по эксплуатации и уходу</span>
                        </a>
                        <a href="#" class="document-link">
                            <div class="document-link-icon">
                                <svg class="icon icon-document" width="1em" height="1em">
                                    <use xlink:href="assets/images/sprite1.svg#cicon-document"></use>
                                </svg>
                            </div>
                            <span class="document-link-name">Рекомендауии по подготовке поверхности</span>
                        </a>
                    </div>
                </div>
                <div id="faq__sertificates"
                     class="faq__main-item faq__main-item_bord faq__main-item_sertificates">
                    <div class="faq__main-item-ttl">
                        <h2 class="h2">Сертификаты</h2>
                    </div>
                    <div class="faq__main-item-cont">
                        <div class="faq__main-item-descr">Все товары, размещенные на сайте, есть на нашем складе
                            и готовы к
                            отгрузке. <br>Имея два филиала в Москве и Новосибирске мы готовы обеспечить
                            кратчайшие <br>сроки
                            доставки по всей территории России.
                        </div>
                    </div>
                    <div class="faq__main-item_instruction-items">
                        <a href="#" class="document-link">
                            <div class="document-link-icon">
                                <svg class="icon icon-document" width="1em" height="1em">
                                    <use xlink:href="assets/images/sprite1.svg#cicon-document"></use>
                                </svg>
                            </div>
                            <span class="document-link-name">Инструкция по наклейке</span>
                        </a>
                        <a href="#" class="document-link">
                            <div class="document-link-icon">
                                <svg class="icon icon-document" width="1em" height="1em">
                                    <use xlink:href="assets/images/sprite1.svg#cicon-document"></use>
                                </svg>
                            </div>
                            <span class="document-link-name">инструкция по эксплуатации и уходу</span>
                        </a>
                        <a href="#" class="document-link">
                            <div class="document-link-icon">
                                <svg class="icon icon-document" width="1em" height="1em">
                                    <use xlink:href="assets/images/sprite1.svg#cicon-document"></use>
                                </svg>
                            </div>
                            <span class="document-link-name">рекомендауии по подготовке поверхности</span>
                        </a>
                    </div>
                </div>
                <div id="faq__faq" class="faq__main-item faq__main-item_faq">
                    <div class="faq__main-item-ttl">
                        <h2 class="h2">FAQ</h2>
                    </div>
                    <div class="faq__main-item-cont">
                        <div class="faq__main-item_faq-collapse" role="tablist">
                            <div class="faq__main-item_faq-collapse-item">
                                <div class="faq__main-item_faq-collapse-item-btn-wrap" role="tab">
                                    <a href="#collapse-1" class="faq__main-item_faq-collapse-item-btn collapsed"
                                       role="button"
                                       data-toggle="collapse" aria-controls="collapse-1">
                                        <span class="faq__main-item_faq-collapse-item-btn-name">Инсталляция пленки</span>
                                        <span class="faq__main-item_faq-collapse-item-btn-icon"></span>
                                    </a>
                                </div>
                                <div id="collapse-1" class="faq__main-item_faq-collapse-item-cont collapse"
                                     role="tabpanel">
                                    <div class="faq__main-item_faq-collapse-item-cont-inner">
                                        <div class="faq__main-item_faq-collapse-item-cont-card">
                                            <div class="faq__main-item_faq-collapse-item-cont-card-ttl">Как
                                                подготовить
                                                поверхность?
                                            </div>
                                            <div class="faq__main-item_faq-collapse-item-cont-card-descr">Для
                                                нанесения покрытия
                                                на неровную поверхность мы рекомендуем, по возможности,
                                                зашкурить ее до гладкого
                                                состояния. Пожалуйста, удостоверьтесь в том, что поверхность
                                                хорошо высушена и не
                                                была подвержена влаге (как минимум на протяжении 1 месяца).
                                            </div>
                                        </div>
                                        <div class="faq__main-item_faq-collapse-item-cont-card">
                                            <div class="faq__main-item_faq-collapse-item-cont-card-ttl">Сколько
                                                необходимо людей
                                                для инсталляции?
                                            </div>
                                            <div class="faq__main-item_faq-collapse-item-cont-card-descr">Для
                                                любого размера
                                                более 1 метра в длину рекомендуется 2 человека. 3 человека может
                                                потребоваться
                                                для инсталляции больших объемов.
                                            </div>
                                        </div>
                                        <div class="faq__main-item_faq-collapse-item-cont-card">
                                            <div class="faq__main-item_faq-collapse-item-cont-card-ttl">Могу ли
                                                я приклеить
                                                Wolly вне помещения?
                                            </div>
                                            <div class="faq__main-item_faq-collapse-item-cont-card-descr">Да,
                                                однако воздействие
                                                окружающей среды может привести к снижению производительности,
                                                сцепления
                                                (клейкости) и долговечности покрытия.
                                            </div>
                                        </div>
                                        <div class="faq__main-item_faq-collapse-item-cont-card">
                                            <div class="faq__main-item_faq-collapse-item-cont-card-ttl">Могу ли
                                                я приклеить
                                                Wolly на дерево?
                                            </div>
                                            <div class="faq__main-item_faq-collapse-item-cont-card-descr">Да, но
                                                обратите
                                                внимание, что если древесина не обработанная, то пленка не
                                                прилипнет вообще.
                                            </div>
                                        </div>
                                        <div class="faq__main-item_faq-collapse-item-cont-card">
                                            <div class="faq__main-item_faq-collapse-item-cont-card-ttl">Могу ли
                                                я приклеить
                                                Wolly на стекло?
                                            </div>
                                            <div class="faq__main-item_faq-collapse-item-cont-card-descr">Да,
                                                если сперва слегка
                                                сбрызнуть и обработать поверхность средством для мытья стекол.
                                            </div>
                                        </div>
                                        <div class="faq__main-item_faq-collapse-item-cont-card">
                                            <div class="faq__main-item_faq-collapse-item-cont-card-ttl">Могу ли
                                                я приклеить
                                                Wolly на металл?
                                            </div>
                                            <div class="faq__main-item_faq-collapse-item-cont-card-descr">Да,
                                                при условии, что
                                                он ровный. На пленку можно будет прикреплять магниты.
                                            </div>
                                        </div>
                                        <div class="faq__main-item_faq-collapse-item-cont-card">
                                            <div class="faq__main-item_faq-collapse-item-cont-card-ttl">Как
                                                избавиться от
                                                пузырьков воздуха?
                                            </div>
                                            <div class="faq__main-item_faq-collapse-item-cont-card-descr">Прежде
                                                всего
                                                подготовьте поверхность, очистите и, если необходимо, зашкурите.
                                                Если при
                                                установке образовались пузырьки, воспользуйтесь ракелем или
                                                тряпочкой из
                                                микрофибры, чтобы вывести воздух к краям, за пределы покрытия
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="faq__main-item_faq-collapse-item">
                                <div class="faq__main-item_faq-collapse-item-btn-wrap" role="tab">
                                    <a href="#collapse-2" class="faq__main-item_faq-collapse-item-btn collapsed"
                                       role="button"
                                       data-toggle="collapse" aria-controls="collapse-2">
                                        <span class="faq__main-item_faq-collapse-item-btn-name">Снятие пленки</span>
                                        <span class="faq__main-item_faq-collapse-item-btn-icon"></span>
                                    </a>
                                </div>
                                <div id="collapse-2" class="faq__main-item_faq-collapse-item-cont collapse"
                                     role="tabpanel">
                                    <div class="faq__main-item_faq-collapse-item-cont-inner">
                                        <div class="faq__main-item_faq-collapse-item-cont-card">
                                            <div class="faq__main-item_faq-collapse-item-cont-card-ttl">Могу ли
                                                я отклеить
                                                Wolly?
                                            </div>
                                            <div class="faq__main-item_faq-collapse-item-cont-card-descr">
                                                Убедитесь, что все
                                                сделанные вами рисунки (записи) высохли. Затем, с помощью фена
                                                немного нагрейте
                                                Wolly. Этот прием облегчит процесс снятия пленки. Во избежание
                                                разрывов и
                                                повреждений, снимайте пленку очень плавно и медленно, без резких
                                                движений.
                                            </div>
                                        </div>
                                        <div class="faq__main-item_faq-collapse-item-cont-card">
                                            <div class="faq__main-item_faq-collapse-item-cont-card-ttl">Вызовет
                                                ли снятие пленки
                                                повреждение поверхности?
                                            </div>
                                            <div class="faq__main-item_faq-collapse-item-cont-card-descr">
                                                Поскольку каждый
                                                случай установки индивидуален, мы рекомендуем предварительно
                                                проверить образец
                                                пленки на малозаметном участке выбранной вами поверхности перед
                                                окончательной
                                                установкой пленки. Важно учесть, что слишком специфичная
                                                поверхность может
                                                непредсказуемо реагировать на пленку, с риском порчи при ее
                                                снятии.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="faq__main-item_faq-collapse-item">
                                <div class="faq__main-item_faq-collapse-item-btn-wrap" role="tab">
                                    <a href="#collapse-3" class="faq__main-item_faq-collapse-item-btn collapsed"
                                       role="button"
                                       data-toggle="collapse" aria-controls="collapse-3">
                                        <span class="faq__main-item_faq-collapse-item-btn-name">Рисовать и стирать</span>
                                        <span class="faq__main-item_faq-collapse-item-btn-icon"></span>
                                    </a>
                                </div>
                                <div id="collapse-3" class="faq__main-item_faq-collapse-item-cont collapse"
                                     role="tabpanel">
                                    <div class="faq__main-item_faq-collapse-item-cont-inner">
                                        <div class="faq__main-item_faq-collapse-item-cont-card">
                                            <div class="faq__main-item_faq-collapse-item-cont-card-ttl">Могу ли
                                                я отклеить
                                                Wolly?
                                            </div>
                                            <div class="faq__main-item_faq-collapse-item-cont-card-descr">
                                                Убедитесь, что все
                                                сделанные вами рисунки (записи) высохли. Затем, с помощью фена
                                                немного нагрейте
                                                Wolly. Этот прием облегчит процесс снятия пленки. Во избежание
                                                разрывов и
                                                повреждений, снимайте пленку очень плавно и медленно, без резких
                                                движений.
                                            </div>
                                        </div>
                                        <div class="faq__main-item_faq-collapse-item-cont-card">
                                            <div class="faq__main-item_faq-collapse-item-cont-card-ttl">Вызовет
                                                ли снятие пленки
                                                повреждение поверхности?
                                            </div>
                                            <div class="faq__main-item_faq-collapse-item-cont-card-descr">
                                                Поскольку каждый
                                                случай установки индивидуален, мы рекомендуем предварительно
                                                проверить образец
                                                пленки на малозаметном участке выбранной вами поверхности перед
                                                окончательной
                                                установкой пленки. Важно учесть, что слишком специфичная
                                                поверхность может
                                                непредсказуемо реагировать на пленку, с риском порчи при ее
                                                снятии.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="faq__main-item_faq-collapse-item">
                                <div class="faq__main-item_faq-collapse-item-btn-wrap" role="tab">
                                    <a href="#collapse-4" class="faq__main-item_faq-collapse-item-btn collapsed"
                                       role="button"
                                       data-toggle="collapse" aria-controls="collapse-4">
                                        <span class="faq__main-item_faq-collapse-item-btn-name">Основное</span>
                                        <span class="faq__main-item_faq-collapse-item-btn-icon"></span>
                                    </a>
                                </div>
                                <div id="collapse-4" class="faq__main-item_faq-collapse-item-cont collapse"
                                     role="tabpanel">
                                    <div class="faq__main-item_faq-collapse-item-cont-inner">
                                        <div class="faq__main-item_faq-collapse-item-cont-card">
                                            <div class="faq__main-item_faq-collapse-item-cont-card-ttl">Могу ли
                                                я отклеить
                                                Wolly?
                                            </div>
                                            <div class="faq__main-item_faq-collapse-item-cont-card-descr">
                                                Убедитесь, что все
                                                сделанные вами рисунки (записи) высохли. Затем, с помощью фена
                                                немного нагрейте
                                                Wolly. Этот прием облегчит процесс снятия пленки. Во избежание
                                                разрывов и
                                                повреждений, снимайте пленку очень плавно и медленно, без резких
                                                движений.
                                            </div>
                                        </div>
                                        <div class="faq__main-item_faq-collapse-item-cont-card">
                                            <div class="faq__main-item_faq-collapse-item-cont-card-ttl">Вызовет
                                                ли снятие пленки
                                                повреждение поверхности?
                                            </div>
                                            <div class="faq__main-item_faq-collapse-item-cont-card-descr">
                                                Поскольку каждый
                                                случай установки индивидуален, мы рекомендуем предварительно
                                                проверить образец
                                                пленки на малозаметном участке выбранной вами поверхности перед
                                                окончательной
                                                установкой пленки. Важно учесть, что слишком специфичная
                                                поверхность может
                                                непредсказуемо реагировать на пленку, с риском порчи при ее
                                                снятии.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>