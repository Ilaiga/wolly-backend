<?php
$this->pageTitle = $project->title;
$this->breadcrumbs =
    array(
        'Проекты' => array('projects/index'),
        $project->title,
    );
if(!empty($project->seo_description))
    Yii::app()->clientScript->registerMetaTag($project->seo_description, 'description');
?>

<div class="c-project">
    <div class="container">
        <div class="project__wrap">

            <? if ($project->type == 0): ?>
                <div class="project__main">
                    <div class="project__main-slider">

                        <? foreach ($project->projectImgs as $img): ?>
                            <div class="project__main-slider-item-wrap">
                                <a href="<?= $img['image_url'] ?>" data-fancybox="project"
                                   class="project__main-slider-item h-object-fit">
                                    <img src="<?= $img['image_url'] ?>" alt="">
                                </a>
                            </div>
                        <? endforeach; ?>
                    </div>
                    <div class="project__main-slider_nav">
                        <div class="project__main-slider_nav-inner">
                            <? foreach ($project->projectImgs as $img): ?>
                                <div class="project__main-slider_nav-item-wrap">
                                    <div class="project__main-slider_nav-item h-object-fit">
                                        <img src="<?= $img['image_url'] ?>" alt="">
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>

                </div>
                <div class="project__cont">
                    <img src="<?= Yii::app()->request->baseUrl; ?>/assets/images/yandex.png" alt="">
                    <div class="project__cont-items">
                        <div class="project__cont-item">
                            <div class="project__cont-item-ttl">Задача</div>
                            <div class="project__cont-item-descr"><?= $project->task ?>
                            </div>
                        </div>
                        <div class="project__cont-item">
                            <div class="project__cont-item-ttl">Описание</div>
                            <div class="project__cont-item-descr project__cont-item-descr_fs18">
                                <?= $project->content ?>
                            </div>
                        </div>
                        <div class="project__cont-item">
                            <div class="project__cont-item-ttl">Использовано</div>
                            <div class="project__cont-item-descr">
                                <div class="project__cont-item-descr-links">
                                    <a href="#" class="project__cont-item-descr-link">МАРКЕРНАЯ ПЛЕНКА WOLLY БЕЛАЯ
                                        ГЛЯНЦЕВАЯ 122
                                        см — 50 м</a>
                                    <a href="#" class="project__cont-item-descr-link">МАРКЕРНАЯ ПЛЕНКА WOLLY БЕЛАЯ
                                        ГЛЯНЦЕВАЯ 152
                                        см — 10 м</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <? else: ?>
                <?=$project->content?>
            <? endif; ?>
        </div>
    </div>
</div>
