<?php
$this->pageTitle = $news->title;
$this->breadcrumbs =
    array(
        'Проекты' => array('projects/index'),
        $news->title,
    );
if(!empty($news->seo_description))
    Yii::app()->clientScript->registerMetaTag($news->seo_description, 'description');
?>

<div class="c-project">
    <div class="container">
        <div class="project__wrap">
            <div class="project__cont">
                <div class="project__cont-items">
                    <div class="project__cont-item">
                        <div class="project__cont-item-descr project__cont-item-descr_fs18">
                            <?= $news->content ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
