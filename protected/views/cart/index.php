<?php
$this->pageTitle = 'Корзина';
$this->breadcrumbs = array(
    'Корзина',
);

?>
<div class="c-basket">
    <div class="container">
        <?= CHtml::beginForm('', 'POST', ['class' => 'basket__wrap']) ?>
        <div class="basket__download">
            <a target="_blank" href="<?= Yii::app()->createUrl('/cart/PDF') ?>" class="basket__download-btn">
                    <span class="basket__download-btn-icon">
                        <svg class="icon icon-document" width="1em" height="1em">
                            <use xlink:href="/assets/images/sprite1.svg#cicon-document"></use>
                        </svg>
                    </span>
                <span class="basket__download-btn-descr">Скачать PDF</span>
            </a>
        </div>
        <div class="basket__table-wrap">
            <table class="basket__table">
                <tbody>
                <?
                foreach ($positions as $position) {
                    $product = $position->getPropertyProduct();
                    ?>
                    <tr class="basket__table-item"
                        data-product="<?= $product->id ?>"
                        data-param_name="<?= !empty($product->param_name) ? $product->param_name : ('') ?>">
                        <td class="basket__table-item-img">
                            <a href="<?= $product->getProductUrl() ?>">
                                <? if (!empty($product->productsImagesPreview)): ?>
                                    <img src="<?= $product->productsImagesPreview[0]['image_url'] ?>" alt="">
                                <? else: ?>
                                    <img src=assets/images/basket/item-img-1.png alt="">
                                <? endif; ?>
                            </a>
                        </td>
                        <td class="basket__table-item-name">
                            <div class="basket__table-item-name-category"><?= $product->undertitle ?></div>
                            <div class="basket__table-item-name-main"><?= $product->title ?>
                                (<?= $product->param_name ?>)
                            </div>
                        </td>
                        <td class="basket__table-item-singleprice">
                            <div class="basket__table-item-singleprice-top">Цена за 1 шт</div>
                            <div class="basket_price basket__table-item-singleprice-main"><?= $product->price ?>
                                <span>&#8381;</span></div>
                        </td>
                        <td class="basket__table-item-counter">
                            <div class="counter-wrap">
                                <div class="counter-minus"></div>
                                <div class="counter-number-wrap">
                                    <input type="number" value="<?= $position->getQuantity(); ?>"
                                           class="counter-number">
                                </div>
                                <div class="counter-plus"></div>
                            </div>
                        </td>
                        <td class="basket__table-item-price">
                            <div class="basket_price"><?= $position->getSumPrice() ?> <span>&#8381;</span></div>
                            <div class="basket__table-item-price-single"><?= $product->price ?> <span>&#8381;</span>
                                за 1 шт
                            </div>
                        </td>

                        <td class="basket__table-item-delete">
                            <?= CHtml::ajaxLink(
                                '<svg class="icon icon-delete" width="1em" height="1em">
                                        <use xlink:href="/assets/images/sprite1.svg#cicon-delete"></use>
                                    </svg>',
                                Yii::app()->createUrl('/cart/DeleteProduct'),
                                array(
                                    'type' => 'POST',// method
                                    'data' => array(
                                        'product_id' => 'js:$(this).closest(".basket__table-item").data("product")',
                                        'param_name' => 'js:$(this).closest(".basket__table-item").data("param_name")',
                                    ),
                                    'update' => '#update_basket, #updMainPrice',
                                ),
                                array('class' => 'basket__table-item-delete-btn')
                            );
                            ?>
                        </td>
                    </tr>
                    <?php

                } ?>
                </tbody>
            </table>
        </div>

        <div class="basket__table-total">
            <div class="basket__table-total-code">
                <div class="basket__table-total-code-descr">У вас есть промокод?</div>
                <div class="basket__table-total-code-input">
                    <div class="form-group">

                        <div class="input-wrap">
                            <input type="text" name="promocode" class="form-control">
                        </div>

                        <span class = "basket__table-total-code--status basket__table-total-code__status--error"></span>
                    </div>
                </div>

                <div class = "basket__table-total-code-input">
                    <button class = "ajax basket__table-total-code--ok"></button>
                </div>

            </div>
            <div class="basket__table-total-main">
                <div class="basket__table-total-main-top">
                    <div class="basket__table-total-main-top-descr">Итого</div>
                </div>
                <div class="basket__table-total-main-top-price">
                    <div class="basket_price">
                        <span id="updMainPrice"><?= Yii::app()->shoppingCart->getCost()?></span> <span>&#8381;</span>
                    </div>
                </div>
            </div>
            <div class="basket__table-total-btn">
                <?//= CHtml::submitButton('Продолжить', ['class' => 'btn btn_blue btn_bold']) ?>
                <a href="<?= Yii::app()->createUrl('order/index') ?>" type="sumbit" class="btn btn_blue btn_bold">Продолжить</a>
            </div>
        </div>
        <?= CHtml::endForm() ?>
    </div>
</div>

<?if (!empty($cartProducts)):?>
<div class="c-buy">
    <div class="container">
        <div class="buy__ttl">
            <h2 class="h2">Не забудьте купить</h2>
        </div>
        <div class="js-carousel-buy buy__items carousel-buy">
            <?
                foreach ($cartProducts as $forget):
                    $data = $product->getForgetProduct($forget->products_id);
                ?>
                <div class="carousel-buy__slide">

                    <div class="buy__item">
                        <div class="h-object-fit buy__item-img">
                            <?
                            $active = false;
                            foreach ($data->productsImagesPreview as $preview_image):?>
                                <img src="<?= $preview_image['image_url'] ?>"
                                     class="<?= !$active ? ('active') : ('') ?>"
                                     data-params_name="<?= $preview_image['params_name'] ?>" alt="">
                                <?
                                $active = true;
                            endforeach;
                            ?>
                        </div>
                        <div class="buy__item-main">

                            <div class="catalog-item__ttl buy__item-main-ttl">
                                <div class="catalog-item__ttl-top"><?= $data->undertitle ?></div>
                                <div class="catalog-item__ttl-bot"><?= $data->title ?></div>
                            </div>
                            <? if (!empty($data->productsParams)): ?>
                                <div class="catalog-item__radiogroup  catalog-item__radiogroup--dontforget">
                                    <? $active = false;
                                    foreach ($data->productsParams as $data_params):

                                        ?>
                                        <div class="form-group"
                                        >
                                            <label class="radio-label_catalog <?= !$active ? ('active') : ('') ?>"
                                                   data-params_name="<?= $data_params['params_name'] ?>"
                                                   data-product_id="<?= $data->id ?>"
                                                   data-params_cost="<?= $data_params['params_cost'] ?>"
                                            >
                                                <input type="radio" name="size"
                                                       value="<?= $data_params['params_name'] ?>"
                                                       class="hidden-input">
                                                <span class="check-value"><?= $data_params['params_name'] ?></span>
                                            </label>
                                        </div>
                                        <?
                                        $active = true;
                                    endforeach;
                                    ?>
                                </div>
                            <? else: ?>
                                <div class="catalog-item__radiogroup  catalog-item__radiogroup--dontforget">
                                    <div class="form-group">
                                        <label class="radio-label_catalog active"
                                               data-params_name=""
                                               data-product_id="<?= $data->id ?>"
                                               data-params_cost="<?= $data->getPrice() ?>
                                                "></label>
                                    </div>
                                </div>
                            <? endif; ?>
                            <div class="buy__item-main-counter">
                                <div class="buy__item-main-counter-wrap">
                                    <div class="counter-wrap">
                                        <div class="counter-minus"></div>
                                        <div class="counter-number-wrap">
                                            <input type="number" value="1" class="counter-number">
                                        </div>
                                        <div class="counter-plus"></div>
                                    </div>
                                </div>
                                <div class="buy__item-main-counter-price">
                                    <? if (!empty($data->productsParams)) {
                                        $paramCount = false;
                                        foreach ($data->productsParams as $param): ?>
                                            <div class="basket_price <?= !$paramCount ? ('active') : ('') ?>"
                                                 data-params_name="<?= $param['params_name'] ?>"
                                            >
                                                <?= $param['params_cost'] ?>
                                                <span>&#8381;</span></div>
                                            <?
                                            $paramCount = true;
                                        endforeach;
                                    } else {
                                        ?>
                                        <div class="basket_price active"><?= $product->price ?>
                                            <span>&#8381;</span></div>
                                    <? } ?>
                                </div>
                            </div>
                            <div class="buy__item-main-btn ajaxButton cBuy">
                                <a href="#" class="btn btn_blue btn_bold">Добавить</a>
                            </div>
                        </div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>
<?endif;?>