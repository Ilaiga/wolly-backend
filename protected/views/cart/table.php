<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=cp1251" />
    <style>
        .basket__table {
            border-spacing: 10px 40px;
            margin: 0 auto;
            width: 100%;
            text-align: center;
            margin-left: 100px;
            font-size: 16px;
        }
        h2 {
            text-align: center;
            margin-bottom: 50px;
            font-size: 20px;
        }
        .basket__table th {
            width: 150px;
        }
    </style>
</head>
<body>


<h2>Ваша корзина:</h2>

<table class="basket__table">
    <thead>
    <tr>
        <th>
            Название товара
        </th>
        <th>
            Стоимость
        </th>
        <th>
            Количество
        </th>
        <th>
            Итого
        </th>
    </tr>
    </thead>
    <tbody>
    <?
    foreach ($positions as $position) {
        $product = $position->getPropertyProduct();
        ?>
        <tr>
            <td>
                <?= $product->title ?> (<?= $product->param_name ?>)
            </td>
            <td>
                <?= $product->price ?> руб.
            </td>
            <td>
                <?= $position->getQuantity(); ?>
            </td>
            <td>
                <?= $position->getSumPrice() ?> руб.
            </td>
        </tr>
        <?php
    } ?>
    </tbody>
</table>
</body>
</html>
